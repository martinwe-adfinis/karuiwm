#ifndef _KWM_STATUSBAR_H
#define _KWM_STATUSBAR_H

#include <karuiwm/rectangle.h>
#include <karuiwm/size.h>
#include <stdbool.h>
#include <X11/Xlib.h>

struct statusbar {
	struct rectangle dim;
	struct xwindow *xwindow;
	Pixmap pm;
	char buffer[256];
	struct monitor *monitor;
	struct workspace *workspace;
	bool focused;
	struct {
		struct bus_session *session;
		struct bus_subscription *sub_ws_rename;
		struct bus_subscription *sub_ws_layout;
	} bus;
};

#include <karuiwm/monitor.h>

struct statusbar *statusbar_alloc(struct monitor *mon);
void statusbar_free(struct statusbar *bar);

void statusbar_redraw(struct statusbar *bar);
int statusbar_set_focus(struct statusbar *bar, bool focus);

#endif /* ndef _KWM_STATUSBAR_H */
