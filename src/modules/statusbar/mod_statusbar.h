#ifndef _KWM_MOD_STATUSBAR_H
#define _KWM_MOD_STATUSBAR_H

#include <stdint.h>

struct statusbar_config {
	struct {
		uint32_t colour_fg;
		uint32_t colour_bg;
	} focused;
	struct {
		uint32_t colour_fg;
		uint32_t colour_bg;
	} unfocused;
	struct xfont *xfont;
};

#include <karuiwm/module.h>

int announce(struct module_data *data);

extern struct statusbar_config statusbar;

#endif /* ndef _KWM_STATUSBAR_H */
