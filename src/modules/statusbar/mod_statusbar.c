#include <modules/statusbar/mod_statusbar.h>
#include <modules/statusbar/statusbar.h>
#include <karuiwm/bus.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/registry.h>
#include <karuiwm/seat.h>
#include <karuiwm/utils.h>
#include <karuiwm/xfont.h>
#include <karuiwm/xserver.h>
#include <karui/list.h>
#include <karui/log.h>

struct _baraction {
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;
};

static int _init(struct module_data *data);
static int _deinit(struct module_data *data);
static int _action_set_colour_focused(struct list **results, struct list const *args);
static int _action_set_colour_unfocused(struct list **results, struct list const *args);
static int _action_set_font(struct list **results, struct list const *args);

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static struct statusbar *_find_statusbar_for_monitor(struct monitor *mon);

struct statusbar_config statusbar;

static uint32_t const _DEFAULT_FOCUSED_FG = 0xAFD700;
static uint32_t const _DEFAULT_FOCUSED_BG = 0x222222;
static uint32_t const _DEFAULT_UNFOCUSED_FG = 0x888888;
static uint32_t const _DEFAULT_UNFOCUSED_BG = 0x222222;
static char const *const _DEFAULT_FONT =
	"-misc-fixed-medium-r-semicondensed--13-100-100-100-c-60-iso8859-1";

#define _(...) (enum value_type const[]) { __VA_ARGS__ }
static struct _baraction const _baractions[] = {
	{ "statusbar_set_colour_focused", _action_set_colour_focused,
	  2, _(VALUE_COLOUR, VALUE_COLOUR) },
	{ "statusbar_set_colour_unfocused", _action_set_colour_unfocused,
	  2, _(VALUE_COLOUR, VALUE_COLOUR) },
	{ "statusbar_set_font", _action_set_font,
	  1, _(VALUE_STRING) },
};
#undef _

static struct bus_session *_busses;
static struct list *_statusbars;
static struct registry_session *_regses;

int
announce(struct module_data *data)
{
	data->init = _init;
	data->deinit = _deinit;
	return 0;
}

static int
_init(struct module_data *data)
{
	struct monitor *mon;
	struct statusbar *bar;
	int unsigned i;
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;

	(void) data;

	statusbar.focused.colour_fg = _DEFAULT_FOCUSED_FG;
	statusbar.focused.colour_bg = _DEFAULT_FOCUSED_BG;
	statusbar.unfocused.colour_fg = _DEFAULT_UNFOCUSED_FG;
	statusbar.unfocused.colour_bg = _DEFAULT_UNFOCUSED_BG;

	statusbar.xfont = xfont_alloc(xserver.display, karuiwm.locale,
	                              _DEFAULT_FONT);
	if (!statusbar.xfont) {
		log_set("Could not load X font set '%s': %s",
		        _DEFAULT_FONT, log_str());
		goto error_font;
	}

	_statusbars = list_alloc();
	if (!_statusbars) {
		log_set("Could not create list of statusbars: %s", log_str());
		goto error_list;
	}
	DEBUG("Detected %zu monitors in state, creating %zu statusbars",
	      seat.monitors->size, seat.monitors->size);
	LIST_FOR (seat.monitors, i, mon) {
		bar = statusbar_alloc(mon);
		if (!bar) {
			log_set("Could not create statusbar for monitor %u: %s",
			        i, log_str());
			goto error_scan_monitors;
		}
		if (statusbar_set_focus(bar, seat.selmon == mon) < 0)
			ERROR("Could not set focus on statusbar for monitor %u [%s]: %s",
			      i, seat.selmon == mon ? "focused" : "unfocused",
			      log_str());
		list_append(_statusbars, bar);
	}

	_regses = registry_connect("statusbar");
	if (!_regses) {
		log_set("Could not open registry: %s", log_str());
		goto error_regopen;
	}

	for (i = 0; i < LENGTH(_baractions); ++i) {
		name = _baractions[i].name;
		func = _baractions[i].func;
		nargs = _baractions[i].nargs;
		arg_types = _baractions[i].arg_types;

		if (registry_create_action(_regses,
		                           name, func, nargs, arg_types) < 0)
		{
			log_set("Could not register action '%s': %s",
			        name, log_str());
			goto error_register_action;
		}
	}

	/* monitor events */
	_busses = bus_connect("statusbar", _bus_event, NULL);
	if (!_busses) {
		log_set("Could not register on event bus: %s", log_str());
		goto error_bus_register;
	}
	if (!bus_subscription_alloc(_busses, bus.monitor_attach,
	                            BUS_SUBSCRIPTION_DATA_MASK_ALL))
	{
		log_set("Could not join event bus channel '%s': %s",
		        bus.monitor_attach->name, log_str());
		goto error_bus_channel;
	}
	if (!bus_subscription_alloc(_busses, bus.monitor_detach,
	                            BUS_SUBSCRIPTION_DATA_MASK_ALL))
	{
		log_set("Could not join event bus channel '%s': %s",
		        bus.monitor_detach->name, log_str());
		goto error_bus_channel;
	}

	return 0;

 error_bus_channel:
	log_push();
	bus_disconnect(_busses);
	log_pop();
 error_bus_register:
	/* registered actions will be cleaned up with session closure */
 error_register_action:
	log_push();
	registry_disconnect(_regses);
	log_pop();
 error_regopen:
 error_scan_monitors:
	log_push();
	list_clear(_statusbars, (list_delfunc) statusbar_free);
	list_free(_statusbars);
	log_pop();
 error_list:
	log_push();
	xfont_free(statusbar.xfont);
	log_pop();
 error_font:
	return -1;
}

static int
_deinit(struct module_data *data)
{
	(void) data;

	bus_disconnect(_busses);
	xfont_free(statusbar.xfont);
	statusbar.xfont = NULL;
	registry_disconnect(_regses);
	_regses = NULL;
	list_clear(_statusbars, (list_delfunc) statusbar_free);
	list_free(_statusbars);
	_statusbars = NULL;

	return 0;
}

static int
_action_set_colour_focused(struct list **results, struct list const *args)
{
	int unsigned i;
	struct statusbar *bar;
	(void) results;

	statusbar.focused.colour_fg = ((struct value *) args->elements[0])->data.col;
	statusbar.focused.colour_bg = ((struct value *) args->elements[1])->data.col;

	LIST_FOR (_statusbars, i, bar)
		statusbar_redraw(bar);
	return 0;
}

static int
_action_set_colour_unfocused(struct list **results, struct list const *args)
{
	int unsigned i;
	struct statusbar *bar;
	(void) results;

	statusbar.unfocused.colour_fg = ((struct value *) args->elements[0])->data.col;
	statusbar.unfocused.colour_bg = ((struct value *) args->elements[1])->data.col;

	LIST_FOR (_statusbars, i, bar)
		statusbar_redraw(bar);
	return 0;
}

static int
_action_set_font(struct list **results, struct list const *args)
{
	char const *fontname;
	struct xfont *xf_new;
	(void) results;

	/* create new font */
	fontname = ((struct value *) args->elements[0])->data.str;
	xf_new = xfont_alloc(xserver.display, karuiwm.locale, fontname);
	if (!xf_new) {
		log_set("Could not load new X font for '%s': %s",
		        fontname, log_str());
		return -1;
	}

	/* replace old font */
	xfont_free(statusbar.xfont);
	statusbar.xfont = xf_new;
	return 0;
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct statusbar *bar;
	struct monitor *mon;

	(void) ctx;

	if (ch == bus.monitor_attach) {
		DEBUG("Bus event '%s', creating and attaching new statusbar",
		      ch->name);
		mon = data;
		bar = statusbar_alloc(mon);
		if (!bar) {
			ERROR("Could not create new statusbar for attached monitor: %s",
			      log_str());
			return;
		}
		list_append(_statusbars, bar);
	} else if (ch == bus.monitor_detach) {
		mon = data;
		DEBUG("Bus event '%s', detaching statusbar", ch->name);
		bar = _find_statusbar_for_monitor(mon);
		if (!bar) {
			WARN("Bus event '%s' for monitor not tracked by statusbar, ignoring",
			     ch->name);
			return;
		}
		list_remove(_statusbars, bar, NULL);
		statusbar_free(bar);
	} else {
		WARN("Statusbar received bus event on unexpected channel '%s'", ch->name);
	}
}

static struct statusbar *
_find_statusbar_for_monitor(struct monitor *mon)
{
	int unsigned i;
	struct statusbar *bar;

	LIST_FOR (_statusbars, i, bar) {
		if (bar->monitor == mon)
			return bar;
	}

	return NULL;
}
