#ifndef _KARUI_LOG_H
#define _KARUI_LOG_H

#include <stdio.h>

#define _LOG_STR2(I) #I
#define LOG_STR(I) _LOG_STR2(I)
#define LOG_FILE() __FILE__":"LOG_STR(__LINE__)

#define LOG_SET_AND_FLUSH(LVL, ...) do { \
	log_set(__VA_ARGS__); \
	log_flush(LVL); \
} while (0)

#define FATAL(...) do { \
	LOG_SET_AND_FLUSH(LOG_FATAL, __VA_ARGS__); \
	log_abort(); \
} while (0)
#define CRITICAL(...) LOG_SET_AND_FLUSH(LOG_CRITICAL, __VA_ARGS__)
#define ERROR(...)    LOG_SET_AND_FLUSH(LOG_ERROR,    __VA_ARGS__)
#define WARN(...)     LOG_SET_AND_FLUSH(LOG_WARNING,  __VA_ARGS__)
#define INFO(...)     LOG_SET_AND_FLUSH(LOG_INFO,     __VA_ARGS__)
#define VERBOSE(...)  LOG_SET_AND_FLUSH(LOG_VERBOSE,  __VA_ARGS__)
#ifdef DEBUG
# undef DEBUG
#endif
#define DEBUG(...)    LOG_SET_AND_FLUSH(LOG_DEBUG, "("LOG_FILE()") "__VA_ARGS__)

#define FATAL_BUG(...) FATAL("\033[1;31m[BUG@"LOG_FILE()"]\033[0m "__VA_ARGS__)
#define BUG(...)    CRITICAL("\033[1;31m[BUG@"LOG_FILE()"]\033[0m "__VA_ARGS__)

enum log_level {
	LOG_SILENT,
	LOG_FATAL,
	LOG_CRITICAL,
	LOG_ERROR,
	LOG_WARNING,
	LOG_INFO,
	LOG_VERBOSE,
	LOG_DEBUG,
	LOG_DISCARD,
};

int log_init(enum log_level log_level, char const *appname);
void log_deinit(void);

void log_abort(void) __attribute__((noreturn));
void log_flush(enum log_level log_level);
void log_pop(void);
void log_push(void);
void log_set(char const *format, ...) __attribute__((format(printf, 1, 2)));
void log_set_level(enum log_level log_level);
char const *log_str(void);

#endif /* ndef _KARUI_LOG_H */
