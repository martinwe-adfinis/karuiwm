#ifndef _KARUI_RUNTIMEDIR_H
#define _KARUI_RUNTIMEDIR_H

int runtimedir_init(char const *appname);
void runtimedir_deinit(void);

char const *runtimedir_get(void);

#endif /* _KARUI_RUNTIMEDIR_H */
