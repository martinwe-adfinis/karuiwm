#include <karui/log.h>
#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define _DIE(...) do { _err(__VA_ARGS__); abort(); } while (0)
#define _ERR(...) _err(__VA_ARGS__)

static void _err(char const *format, ...) __attribute__((format(printf, 1, 2)));
static char *_format(char const *format, va_list ap);
static void _print(enum log_level log_level, char const *msg);
static void _set(char const *format, va_list ap);

static enum log_level _log_level;
static char *_appname;
static struct {
	char **entries;
	size_t size;
} _stack;

int
log_init(enum log_level log_level, char const *appname)
{
	size_t slen;

	_stack.size = 0;
	_stack.entries = NULL;
	log_push(); /* initial entry */

	log_set_level(log_level);

	if (appname) {
		slen = strlen(appname);
		_appname = malloc(slen + 1);
		if (!_appname)
			_DIE("Could not create buffer of size %zu+1 for application name: %s",
			     slen, strerror(errno));
		(void) strcpy(_appname, appname);
	} else {
		_appname = NULL;
	}

	return 0;
}

void
log_deinit(void)
{
	/* no leftovers on the stack */
	assert(_stack.size == 1);
	if (_stack.entries[0])
		free(_stack.entries[0]);

	/* clean up */
	free(_stack.entries);
	_stack.entries = NULL;
	_stack.size = 0;

	if (_appname) {
		free(_appname);
		_appname = NULL;
	}
}

void
log_abort(void)
{
	abort();
}

void
log_flush(enum log_level log_level)
{
	/* there should be exactly one message on the stack */
	// TODO: let *every* function return an error
	//assert(_stack.size == 1);
	//assert(_stack.entries[0] != NULL);

	/* print if log level permits */
	if (log_level <= _log_level)
		_print(log_level, _stack.entries[_stack.size - 1]);
}

void
log_pop(void)
{
	void *ptr;

	assert(_stack.entries);
	assert(_stack.size > 1);

	if (_stack.entries[_stack.size - 1])
		free(_stack.entries[_stack.size - 1]);

	--_stack.size;
	ptr = realloc(_stack.entries, _stack.size * sizeof(char *));
	if (!ptr)
		_ERR("Could not shrink log stack to %zu entries (%zu bytes): %s",
		     _stack.size, _stack.size * sizeof(char *),
		     strerror(errno));
	else
		_stack.entries = ptr;
}

void
log_push(void)
{
	++_stack.size;
	_stack.entries = realloc(_stack.entries, _stack.size * sizeof(char *));
	if (!_stack.entries)
		_DIE("Could not grow log stack to %zu bytes: %s",
		     _stack.size * sizeof(char *), strerror(errno));
	_stack.entries[_stack.size - 1] = NULL;
}

void
log_set(char const *format, ...)
{
	va_list ap;

	va_start(ap, format);
	_set(format, ap);
	va_end(ap);
}

void
log_set_level(enum log_level log_level)
{
	_log_level = log_level;
}

char const *
log_str(void)
{
	assert(_stack.size >= 1);
	return _stack.entries[_stack.size - 1];
}

static void
_err(char const *format, ...)
{
	va_list ap;

	va_start(ap, format);
	(void) vfprintf(stderr, format, ap);
	va_end(ap);

	(void) fprintf(stderr, "\n");
	(void) fflush(stderr);
}

static char *
_format(char const *format, va_list ap)
{
	va_list aq;
	int len;
	size_t slen;
	char *str;

	/* determine length */
	va_copy(aq, ap);
	len = vsnprintf(NULL, 0, format, aq);
	va_end(aq);
	if (len < 0)
		_DIE("Could not determine log stack entry string length: vsnprintf returned %d",
		     len);
	slen = (size_t) len;

	/* allocate */
	str = malloc(slen + 1);
	if (!str)
		_DIE("Could not allocate %zu bytes for log stack entry string: %s",
		     slen + 1, strerror(errno));

	/* copy */
	va_copy(aq, ap);
	len = vsnprintf(str, slen + 1, format, aq);
	va_end(aq);
	if (len < 0 || (size_t) len != slen)
		_DIE("Could not copy log stack entry string: vsnprintf returned %d != %zu",
		     len, slen);

	return str;
}

static void
_print(enum log_level log_level, char const *msg)
{
	time_t rawtime;
	struct tm *date;
	FILE *f = NULL;
	char const *prefix;

	/* output */
	switch (log_level) {
	case LOG_SILENT:
		_ERR("BUG: Attempt to print message at log level LOG_SILENT");
		return;
	case LOG_FATAL:
	case LOG_CRITICAL:
	case LOG_ERROR:
	case LOG_WARNING:
		f = stderr;
		break;
	case LOG_INFO:
	case LOG_VERBOSE:
	case LOG_DEBUG:
		f = stdout;
		break;
	case LOG_DISCARD:
		f = NULL;
		break;
	}

	if (!f)
		/* nowhere to print out */
		return;

	/* timestamp */
	time(&rawtime);
	date = localtime(&rawtime);
	(void) fprintf(f, "[%04d-%02d-%02d,%02d:%02d:%02d] %s: ",
	               date->tm_year+1900, date->tm_mon, date->tm_mday,
	               date->tm_hour, date->tm_min, date->tm_sec,
	               _appname);

	/* prefix */
	switch (log_level) {
	case LOG_FATAL:    prefix = "\033[1;31mFATAL\033[0m";    break;
	case LOG_CRITICAL: prefix = "\033[1;31mCRITICAL\033[0m"; break;
	case LOG_ERROR:    prefix = "\033[31mERROR\033[0m";      break;
	case LOG_WARNING:  prefix = "\033[33mWARNING\033[0m";    break;
	case LOG_INFO:     prefix = NULL;                        break;
	case LOG_VERBOSE:  prefix = NULL;                        break;
	case LOG_DEBUG:    prefix = "\033[34mDBG\033[0m";        break;
	default:
		BUG("Unexpected log_level");
		return;
	}
	if (prefix)
		(void) fprintf(f, "%s ", prefix);

	/* message */
	fprintf(f, "%s\n", msg);
	fflush(f);
}

static void
_set(char const *format, va_list ap)
{
	char *entry;

	if (format) {
		entry = _format(format, ap);
		assert(entry);
	} else {
		entry = NULL;
	}

	assert(_stack.size >= 1);
	assert(_stack.entries);

	if (_stack.entries[_stack.size - 1])
		free(_stack.entries[_stack.size - 1]);
	_stack.entries[_stack.size - 1] = entry;
}
