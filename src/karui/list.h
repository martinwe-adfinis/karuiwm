#ifndef _KARUI_LIST_H
#define _KARUI_LIST_H

#include <stdbool.h>
#include <stddef.h>

#define LIST_FOR(L,I,E) \
        for (I = 0; I < L->size; ++I) \
        for (E = L->elements[I]; E != NULL; E = NULL)

typedef int (*list_cmpfunc)(void const *e1, void const *e2);
typedef void (*list_delfunc)(void *e);
typedef bool (*list_eqfunc)(void const *le, void const *e);
typedef void *(*list_copyfunc)(void const *e);

struct list {
	size_t size;
	void **elements;
};

struct list *list_alloc(void);
int list_init(struct list *l);
void list_deinit(struct list *l);
void list_free(struct list *l);

void list_append(struct list *l, void *e);
void list_clear(struct list *l, void (*f)(void *e));
int list_concatenate(struct list *l1, struct list *l2);
bool list_contains(struct list *l, void const *e,
                   bool (*f)(void const *le, void const *e));
struct list *list_copy(struct list const *l, list_copyfunc cf, list_delfunc df);
void *list_find(struct list const *l, void const *e,
                bool (*f)(void const *le, void const *e));
int unsigned list_find_index(struct list *l, void const *e,
                             bool (*f)(void const *le, void const *e));
void list_insert(struct list *l, void *e, int unsigned index);
void list_insert_sorted(struct list *l, void *e,
                        int (*f)(void const *e1, void const *e2));
void list_prepend(struct list *l, void *e);
void list_push(struct list *l, void *e);
void list_remove(struct list *l, void const *e,
                 bool (*f)(void const *le, void const *e));

#endif /* ndef _KARUI_LIST_H */
