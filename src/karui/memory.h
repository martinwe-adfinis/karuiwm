#ifndef _KARUI_MEMORY_H
#define _KARUI_MEMORY_H

#include <stddef.h>

void *memory_alloc(size_t size, char const *name);
void memory_free(void *mem);

void *memory_dup(void const *mem, size_t len, char const *name);
int memory_resize(void **mem, size_t size, char const *name);

#endif /* ndef _KARUI_MEMORY_H */
