#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fnmatch.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

static int _emit_token(struct list *tokens, char const *start, char const *end);

void
cstr_free(char *str)
{
	if (str)
		memory_free(str);
}

char *
cstr_dup(char const *str)
{
	char *copy;

	copy = memory_dup(str, strlen(str) + 1, "string");
	if (!copy)
		return NULL;
	return copy;
}

bool
cstr_equals(char const *str1, char const *str2)
{
	return strcmp(str1, str2) == 0;
}

bool
cstr_equals_caseless(char const *str1, char const *str2)
{
	return strcasecmp(str1, str2) == 0;
}

char const *
cstr_find_char(char const *str, char c)
{
	char const *cp;

	for (cp = str; *cp != '\0'; ++cp)
		if (*cp == c)
			return cp;
	return NULL;
}

char *
cstr_format(char const *format, ...)
{
	char *cstr;
	va_list ap;
	int len;
	size_t slen;

	/* determine length */
	va_start(ap, format);
	len = vsnprintf(NULL, 0, format, ap);
	va_end(ap);
	if (len < 0) {
		log_set("Could not calculate string length with vsnprintf");
		return NULL;
	}
	slen = (size_t) len;

	/* allocate */
	cstr = memory_alloc(slen + 1, "string");
	if (!cstr)
		return NULL;

	/* copy */
	va_start(ap, format);
	len = vsnprintf(cstr, slen + 1, format, ap);
	va_end(ap);
	if (len < 0 || (size_t) len != slen) {
		log_set("Could not copy string with vsnprintf");
		memory_free(cstr);
		return NULL;
	}
	return cstr;
}

char *
cstr_join(struct list const *strs, char const *delim)
{
	size_t slen_total, slen, dlen, soff;
	int unsigned i;
	char *str;

	if (strs->size == 0)
		return cstr_dup("");

	/* total string size (elements + delimiter) */
	for (i = 0, slen_total = 0; i < strs->size; ++i)
		slen_total += strs->elements[i] ? strlen(strs->elements[i]) : 0;
	dlen = delim ? strlen(delim) : 0;
	slen_total += (strs->size - 1) * dlen;

	/* allocate string */
	str = memory_alloc(slen_total + 1, "joined string");
	if (!str)
		return NULL;

	/* copy */
	for (i = 0, soff = 0; i < strs->size; ++i) {
		assert(soff < slen_total);
		slen = strs->elements[i] ? strlen(strs->elements[i]) : 0;
		(void) memcpy(str + soff, strs->elements[i], slen);
		soff += slen;
		(void) memcpy(str + soff, delim, dlen);
		soff += dlen;
	}
	str[slen_total] = '\0';

	return str;
}

char *
cstr_longest_prefix(struct list const *strs)
{
	char *prefix;
	size_t plen;
	int unsigned i, j;
	char *s;

	if (strs->size == 0) {
		prefix = cstr_dup("");
		return prefix;
	}

	prefix = cstr_dup(strs->elements[0]);
	if (!prefix) {
		log_set("Could not copy first list element as prefix: %s",
		        log_str());
		goto error_init_prefix;
	}
	plen = strlen(prefix) + 1;

	LIST_FOR (strs, i, s)
		for (j = 0; j < plen; ++j)
			if (s[j] != prefix[j])
				plen = j;

	if (memory_resize((void **) &prefix, (plen + 1) * sizeof(char),
	                  "prefix string") < 0)
		log_flush(LOG_WARNING);
	prefix[plen] = '\0';

	return prefix;

 error_init_prefix:
	return NULL;
}

bool
cstr_matches_wildcard(char const *str, char const *wildcard)
{
	int ret;

	ret = fnmatch(wildcard, str, 0x0);
	if (ret != 0 && ret != FNM_NOMATCH)
		ERROR("Could not match string '%s' with wildcard '%s' due to an error with fnmatch",
		      str, wildcard);

	return ret == 0;
}

int
cstr_parse_bool(bool *retval, char const *str)
{
	char const *truestrs[] = {
		"True", "true", "T", "t", "Yes", "yes", "Y", "y", "1",
	};
	char const *falsestrs[] = {
		"False", "false", "F", "f", "No", "no", "N", "n", "0",
	};
	size_t const NSTRS_TRUE = sizeof(truestrs) / sizeof(truestrs[0]);
	size_t const NSTRS_FALSE = sizeof(falsestrs) / sizeof(falsestrs[0]);
	int unsigned i;

	for (i = 0; i < NSTRS_TRUE; ++i) {
		if (strcmp(str, truestrs[i]) == 0) {
			if (retval)
				*retval = true;
			return 0;
		}
	}
	for (i = 0; i < NSTRS_FALSE; ++i) {
		if (strcmp(str, falsestrs[i]) == 0) {
			if (retval)
				*retval = false;
			return 0;
		}
	}
	log_set("Not a boolean value: '%s'", str);
	return -1;
}

int
cstr_parse_colour(uint32_t *retval, char const *str)
{
	size_t slen;
	bool is_12bit;
	char *endptr;
	int long longval;

	/* verify correct colour format */
	slen = strlen(str);
	if (slen == 0) {
		log_set("Empty string");
		return -1;
	}
	if (str[0] != '#') {
		log_set("Colour value '%s' does not start with '#'", str);
		return -1;
	}
	++str;
	--slen;

	/* check if colour channels are single- or double-digit */
	switch (slen) {
	case 3:
		is_12bit = true;
		break;
	case 6:
		is_12bit = false;
		break;
	default:
		log_set("Invalid number of digits (%zu) for RGB value: '%s'",
		        slen, str);
		return -1;
	}

	/* parse value */
	errno = 0;
	longval = strtol(str, &endptr, 16);
	if (errno != 0) {
		log_set("Could not parse '%s' as hexadecimal value: %s",
		        str, strerror(errno));
		return -1;
	}
	if (*endptr != '\0') {
		log_set("Invalid character for integer number at offset %ld: '%s'",
		        endptr - str, endptr);
		return -1;
	}
	if (longval < 0) {
		log_set("Negative value for colour: %ld", longval);
		return -1;
	}
	assert(longval < UINT32_MAX);

	/* assign and adapt for 12-bit colour value */
	if (retval) {
		*retval = (uint32_t) longval;
		if (is_12bit) {
			assert((*retval & 0xFFF000) == 0x000000);
			*retval = ((*retval & 0x000F00) << 12)
			        | ((*retval & 0x0000F0) << 8)
			        | ((*retval & 0x00000F) << 4);
		}
	}
	return 0;
}

int
cstr_parse_float(float *retval, char const *str)
{
	char *endptr;
	float floatval;

	errno = 0;
	floatval = strtof(str, &endptr);
	if (errno == ERANGE) {
		log_set("Value out of range: %s", str);
		return -1;
	}
	if (*endptr != '\0') {
		log_set("Invalid character for floating point number at offset %ld: %s",
		        endptr - str, endptr);
		return -1;
	}
	if (retval)
		*retval = floatval;
	return 0;
}

int
cstr_parse_int(int *retval, char const *str, char **endptr)
{
	bool have_endptr = (bool) endptr;
	char *lendptr;
	int intval;

	errno = 0;
	intval = (int) strtol(str, &lendptr, 0);
	if (errno != 0) {
		log_set("Could not parse '%s' as numeric value: %s",
		        str, strerror(errno));
		return -1;
	}
	if (have_endptr) {
		*endptr = lendptr;
	} else if (*lendptr != '\0') {
		log_set("Trailing character after integer value at offset %ld: '%s'",
		        lendptr - str, lendptr);
		return -1;
	}
	if (retval)
		*retval = intval;
	return 0;
}

int
cstr_parse_int_signed(int *retval, char const *str, char **endptr)
{
	/* ensure there's a sign */
	switch (str[0]) {
	case '+':
	case '-':
		break;
	default:
		log_set("Invalid sign (must be +/-): '%c'", str[0]);
		return -1;
	}

	return cstr_parse_int(retval, str, endptr);
}

int
cstr_parse_int_unsigned(int unsigned *retval, char const *str, char **endptr)
{
	int i;

	/* ensure there's no sign */
	switch (str[0]) {
	case '+':
	case '-':
		log_set("Value may not be signed (found '%c' in '%s')",
		        str[0], str);
		return -1;
	}

	if (cstr_parse_int(&i, str, endptr) < 0)
		return -1;
	assert(i >= 0);
	*retval = (int unsigned) i;
	return 0;
}

struct list *
cstr_split(char const *str, char const *delims)
{
	char *tt, *t, *tokens;
	struct list *list;

	list = list_alloc();
	if (!list) {
		log_set("Could not create list: %s", log_str());
		goto error_list;
	}

	tokens = cstr_dup(str);
	if (!tokens) {
		log_set("Could not copy string: %s", log_str());
		goto error_dup;
	}

	tt = strtok(tokens, delims);
	while (tt) {
		t = cstr_dup(tt);
		if (!t) {
			log_set("Could not copy token string: %s", log_str());
			goto error_append_dup;
		}
		list_append(list, t);
		tt = strtok(NULL, delims);
		continue;

	error_append_dup:
		goto error_append;
	}
	memory_free(tokens);
	return list;

 error_append:
	log_push();
	while (list->size > 0) {
		t = list->elements[0];
		list_remove(list, t, NULL);
		memory_free(t);
	}
	log_pop();
	memory_free(tokens);
 error_dup:
	log_push();
	list_free(list);
	log_pop();
 error_list:
	return NULL;
}

int
cstr_split_once(char const *str, char const *delims,
                char **first, char **second)
{
	size_t slen, dlen;
	int unsigned is, id;
	bool dfound;

	slen = strlen(str);
	dlen = strlen(delims);

	for (is = 0, dfound = false; is < slen && !dfound; ++is)
		for (id = 0; id < dlen && !dfound; ++id)
			dfound = str[is] == delims[id];
	--is;

	if (!dfound) {
		log_set("Delimiter not found in '%s' (among [%s])",
		        str, delims);
		return -1;
	}

	if (first) {
		*first = memory_dup(str, (size_t) is + 1, "first string");
		if (!*first)
			goto error_first;
		(*first)[is] = '\0';
	}

	if (second) {
		*second = memory_dup(str + is + 1, slen - is, "second string");
		if (!*second)
			goto error_second;
	}

	return 0;

 error_second:
	if (first) {
		log_push();
		cstr_free(*first);
		log_pop();
	}
 error_first:
	return -1;
}

struct list *
cstr_split_smart(char const *str)
{
	struct list *tokens;
	char const *src_ptr;
	char *dst_start, *dst_ptr;
	bool quoted, single_quoted, double_quoted, escaped, inword, space;

	assert(str);

	tokens = list_alloc();
	if (!tokens) {
		log_set("Could not create list for split tokens: %s",
		        log_str());
		goto error_list;
	}

	dst_start = memory_alloc(strlen(str) + 1, "Token buffer");
	if (!dst_start)
		goto error_buffer;
	dst_ptr = dst_start;

	quoted = single_quoted = double_quoted = escaped = inword = false;
	for (src_ptr = str; *src_ptr != '\0'; ++src_ptr) {
		space = isspace(*src_ptr);
		if (!space)
			inword = true;

		switch (*src_ptr) {
		case '\'':
			if (escaped || double_quoted)
				goto verbatim;
			quoted = single_quoted = !single_quoted;
			break;
		case '"':
			if (escaped || single_quoted)
				goto verbatim;
			quoted = double_quoted = !double_quoted;
			break;
		case '\\':
			if (single_quoted || escaped)
				goto verbatim;
			escaped = true;
			break;
		case 'f':
		case 'n':
		case 'r':
		case 't':
		case 'v':
			if (!escaped)
				goto verbatim;
			switch (*src_ptr) {
				case 'f': *(dst_ptr++) = '\f'; break;
				case 'n': *(dst_ptr++) = '\n'; break;
				case 'r': *(dst_ptr++) = '\r'; break;
				case 't': *(dst_ptr++) = '\t'; break;
				case 'v': *(dst_ptr++) = '\v'; break;
			}
			break;
		default:
			if (space) {
				if (!inword)
					break;
				if (escaped || quoted)
					goto verbatim;
				inword = false;
				if (_emit_token(tokens, dst_start, dst_ptr) < 0)
					goto error_emit;
				dst_ptr = dst_start;
				break;
			}
			if (escaped) {
				log_set("Invalid escape sequence: '\\%c'",
				        *src_ptr);
				goto error_escape;
			}
		verbatim:
			*(dst_ptr++) = *src_ptr;
			escaped = false;
			break;
		}
	}

	if (escaped || quoted) {
		log_set("Unterminated %s at end of string",
		        single_quoted ? "single quote" :
		        double_quoted ? "double quote" :
		                        "escape sequence");
		goto error_eos;
	}

	if (inword)
		if (_emit_token(tokens, dst_start, dst_ptr) < 0)
			goto error_emit;

	memory_free(dst_start);
	return tokens;

 error_eos:
 error_escape:
 error_emit:
	log_push();
	memory_free(dst_start);
	log_pop();
 error_buffer:
	log_push();
	list_clear(tokens, (list_delfunc) cstr_free);
	list_free(tokens);
	log_pop();
 error_list:
	return NULL;
}

static int
_emit_token(struct list *tokens, char const *start, char const *end)
{
	size_t slen;
	char *token;

	assert(end >= start);
	slen = (int long unsigned) (end - start);

	token = memory_alloc(slen + 1, "token");
	if (!token)
		return -1;

	(void) strncpy(token, start, slen);
	token[slen] = '\0';

	list_append(tokens, token);
	return 0;
}

bool
cstr_starts_with(char const *str, char const *substr)
{
	size_t substr_len;

	substr_len = strlen(substr);

	return strncmp(str, substr, substr_len) == 0;
}
