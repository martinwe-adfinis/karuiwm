#include <karuirpc/rpc_client.h>
#include <karui/cstr.h>
#include <karui/envvars.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <karui/userdirs.h>
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

int _socket_read(struct rpc_client *c, char **str);

static size_t const BUFSIZE = 1024;

struct rpc_client *
rpc_client_alloc(void)
{
	struct rpc_client *c;
	struct sockaddr_un addr;
	char const *XDG_RUNTIME_DIR, *DISPLAY;
	int ret;

	c = memory_alloc(sizeof(struct rpc_client), "RPC client structure");
	if (!c)
		goto error_alloc;

	XDG_RUNTIME_DIR = userdirs_get_runtimedir();

	DISPLAY = envvars_get("DISPLAY", NULL);
	if (!DISPLAY) {
		log_set("DISPLAY is not set");
		goto error_display;
	}

	DEBUG("Initialising RPC connection with XDG_RUNTIME_DIR=%s and DISPLAY=%s",
	      XDG_RUNTIME_DIR, DISPLAY);
	c->fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (c->fd < 0) {
		log_set("Could not create socket: %s", strerror(errno));
		goto error_socket;
	}

	(void) memset(&addr, 0x0, sizeof(struct sockaddr_un));
	addr.sun_family = AF_UNIX;
	(void) snprintf(addr.sun_path, sizeof(addr.sun_path),
	                "%s/karuiwm/%s/rpc.sock", XDG_RUNTIME_DIR, DISPLAY);

	ret = connect(c->fd, (struct sockaddr *) &addr,
	              sizeof(struct sockaddr_un));
	if (ret < 0) {
		log_set("Could not connect to Unix socket at %s: %s",
		        addr.sun_path, strerror(errno));
		goto error_connect;
	}

	return c;

 error_connect:
	close(c->fd);
 error_socket:
 error_display:
	log_push();
	memory_free(c);
	log_pop();
 error_alloc:
	return NULL;
}

void
rpc_client_free(struct rpc_client *c)
{
	if (c->fd >= 0)
		close(c->fd);
	memory_free(c);
}

int
rpc_client_recv(struct rpc_client *c, struct rpc_response **response)
{
	char *str;
	struct rpc_response *r;

	if (c->fd < 0) {
		log_set("Socket is closed");
		return -1;
	}

	if (_socket_read(c, &str) < 0) {
		log_set("Could not read from socket: %s", log_str());
		goto error_socket;
	} else if (!str) {
		assert(c->fd < 0);
		goto closed;
	}

	r = rpc_response_parse(str);
	if (!r) {
		log_set("Could not parse RPC response string: %s", log_str());
		goto error_parse;
	}

	cstr_free(str);
	*response = r;
	return 0;

 closed:
	*response = NULL;
	return 0;

 error_parse:
	log_push();
	cstr_free(str);
	log_pop();
 error_socket:
	return -1;
}

int
rpc_client_send(struct rpc_client *c, char const *cmd)
{
	size_t msglen, txlen;
	ssize_t seglen;

	if (c->fd < 0) {
		log_set("Socket is closed");
		return -1;
	}

	msglen = strlen(cmd) + 1;

	for (txlen = 0, seglen = 0; seglen >= 0 && (size_t) txlen < msglen;) {
		seglen = write(c->fd, cmd + txlen, msglen - txlen);
		if (seglen > 0)
			txlen += (size_t) seglen;
		DEBUG("wrote %zd bytes (%zu bytes written in total, %zu bytes left)",
		      seglen, txlen, msglen - txlen);
	}

	if (seglen == 0) {
		close(c->fd);
		c->fd = -1;
	}
	if (seglen < 0) {
		log_set("Could not write command to socket: %s",
		        strerror(errno));
		goto error_send;
	}

	return 0;

 error_send:
	return -1;
}

int
_socket_read(struct rpc_client *c, char **str)
{
	char *response, *response_extended, buf[BUFSIZE];
	ssize_t snb;
	size_t nb;
	bool reached_end;
	int unsigned i;

	assert(c->fd >= 0);

	response = cstr_dup("");
	if (!response) {
		log_set("Could not create initially empty response string: %s",
		        log_str());
		goto error_response;
	}

	for (i = 0, reached_end = false; !reached_end; ++i) {
		/* read from socket */
		snb = read(c->fd, buf, BUFSIZE);
		if (snb <= 0) {
			if (snb == 0) {
				close(c->fd);
				c->fd = -1;
			} else {
				log_set("%s", strerror(errno));
			}
			goto error_read;
		}
		nb = (size_t) snb;
		if (nb < BUFSIZE || buf[nb - 1] == '\0')
			reached_end = true;

		/* append to response */
		response_extended = cstr_format("%s%s", response, buf);
		if (!response_extended) {
			log_set("Could not append received data segment %u/? to response string: %s",
			        i + 1, log_str());
			goto error_append;
		}
		cstr_free(response);
		response = response_extended;
	}

	*str = response;
	return 0;

 error_append:
 error_read:
	log_push();
	cstr_free(response);
	log_pop();
 error_response:
	*str = NULL;
	return c->fd < 0 ? 0 : -1;
}
