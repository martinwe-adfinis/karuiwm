#include <karuirpc/input.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <karui/userdirs.h>
#include <assert.h>
#include <errno.h>
#include <stdio.h> /* required before readline */
#include <readline/history.h>
#include <readline/readline.h>
#include <string.h>
#include <unistd.h>

static int _handle_command(void);
static int _handle_generic(void);
static int _handle_readline(void);
static int _init_command(char const *command);
static int _init_file(char const *path);
static int _init_fs(void);
static int _init_readline(void);
static int _init_stdin(void);
static void _rl_callback(char *text);
static char **_rl_complete(char const *text, int start, int end);

static size_t const BUFSIZE = 1024;

struct {
	enum input_type type;
	FILE *fs;
	int fd;
	union {
		char *arg;
		char *command;
		char *path;
	};
	int (*cb_input)(char const *);
	struct list *(*cb_completion)(char const *);
	char *history_file;
} _input;

int
input_init(enum input_type type, char const *arg, int (*cb)(char const *))
{
	_input.type = type;
	_input.fs = NULL;
	_input.fd = -1;
	_input.arg = NULL;
	_input.cb_input = cb;
	_input.cb_completion = NULL;
	_input.history_file = NULL;

	switch (type) {
	case INPUT_COMMAND:
		return _init_command(arg);
	case INPUT_FILE:
		return _init_file(arg);
	case INPUT_STDIN:
		return _init_stdin();
	default:
		log_set("Invalid input type: %s", input_type_as_string(type));
		return -1;
	}

	FATAL_BUG("input_init() reached unreachable code");
}

void
input_term(void)
{
	if (_input.type == INPUT_READLINE) {
		if (write_history(_input.history_file) < 0)
			ERROR("Could not write history to %s: %s",
			      _input.history_file, strerror(errno));
		cstr_free(_input.history_file);
		_input.history_file = NULL;
		rl_callback_handler_remove();
		_input.cb_input = NULL;
		_input.cb_completion = NULL;
	}
	if (_input.fs) {
		fclose(_input.fs);
		_input.fs = NULL;
		_input.fd = -1;
	}
	if (_input.arg) {
		cstr_free(_input.arg);
		_input.arg = NULL;
	}
}

int
input_add_history(char const *line)
{
	if (!input_is_interactive()) {
		log_set("Cannot add line to history for non-interactive input");
		return -1;
	}

	add_history(line);
	return 0;
}

int
input_get_fd(void)
{
	return _input.fd;
}

int
input_handle(void)
{
	switch (_input.type) {
	case INPUT_COMMAND:
		return _handle_command();
	case INPUT_FILE:
	case INPUT_STDIN:
		return _handle_generic();
	case INPUT_READLINE:
		return _handle_readline();
	}

	FATAL_BUG("input_handle() reached unreachable code");
}

bool
input_is_interactive(void)
{
	return _input.type == INPUT_READLINE;
}

int
input_set_completion(struct list *(*cb)(char const *))
{
	if (!input_is_interactive()) {
		log_set("Cannot set completion for non-interactive input");
		return -1;
	}

	_input.cb_completion = cb;

	return 0;
}

char const *
input_type_as_string(enum input_type type)
{
	switch (type) {
	case INPUT_COMMAND: return "INPUT_COMMAND";
	case INPUT_FILE: return "INPUT_FILE";
	case INPUT_READLINE: return "INPUT_READLINE";
	case INPUT_STDIN: return "INPUT_STDIN";
	}

	FATAL_BUG("input_type_as_string() reached unreachable code");
}

static int
_handle_command(void)
{
	if (_input.command) {
		/* command has not run yet */
		if (_input.cb_input(_input.command) < 0) {
			log_set("Could not handle command \"%s\": %s",
			        _input.command, log_str());
			goto error_command;
		}
		cstr_free(_input.command);
		_input.command = NULL;
		return 0;
	} else {
		/* command has already run */
		assert(!_input.fs && _input.fd < 0); /* indicates EOF */
		return -1;
	}

 error_command:
	return -1;
}

static int
_handle_generic(void)
{
	char *line, *line_extended, buf[BUFSIZE];
	bool reached_end;
	int unsigned i;
	size_t nb;
	int retval;

	line = cstr_dup("");
	if (!line) {
		log_set("Could not create initially empty line string: %s",
		        log_str());
		goto error_alloc;
	}

	for (i = 0, reached_end = false; !reached_end; ++i) {
		/* read from input/file */
		errno = 0;
		if (!fgets(buf, (int signed) BUFSIZE, _input.fs)) {
			if (errno == 0) {
				if (fclose(_input.fs) != 0)
					WARN("Could not close input file descriptor %d: %s",
					     _input.fd, strerror(errno));
				_input.fs = NULL;
				_input.fd = -1;
				goto eof;
			} else {
				log_set("Could not read from input file descriptor %d: %s",
				        _input.fd, strerror(errno));
				goto error_read;
			}
		}

		/* split trailing EOL */
		nb = strlen(buf);
		if (nb == 0) {
			log_set("Received 0-length string from input file descriptor %d",
			        _input.fd);
			goto error_read;
		}
		if (nb < BUFSIZE - 1 || buf[nb - 1] == '\n') {
			reached_end = true;
			if (buf[nb - 1] == '\n')
				buf[nb - 1] = '\0';
		}

		/* append to line */
		line_extended = cstr_format("%s%s", line, buf);
		if (!line_extended) {
			log_set("Could not append read input segment %u/? to line string: %s",
			        i + 1, log_str());
			goto error_append;
		}
		cstr_free(line);
		line = line_extended;
	}

	retval = _input.cb_input(line);

	cstr_free(line);
	return retval;

 error_append:
 error_read:
 eof:
	log_push();
	cstr_free(line);
	log_pop();
 error_alloc:
	return -1;
}

static int
_handle_readline(void)
{
	rl_callback_read_char();
	return 0;
}

static int
_init_command(char const *command)
{
	_input.command = cstr_dup(command);
	if (!_input.command) {
		log_set("Could not copy command string: %s", log_str());
		goto error_command;
	}

	return 0;

 error_command:
	return -1;
}

static int
_init_file(char const *path)
{
	_input.fs = fopen(path, "r");
	if (!_input.fs) {
		log_set("Could not open file %s for reading: %s",
		        path, strerror(errno));
		goto error_fopen;
	}

	if (_init_fs() < 0) {
		log_set("Could not initialise file stream: %s", log_str());
		goto error_fs;
	}

	_input.path = cstr_dup(path);
	if (!_input.path) {
		log_set("Could not copy file path %s: %s", path, log_str());
		goto error_path;
	}

	return 0;

 error_path:
 error_fs:
	fclose(_input.fs);
	_input.fs = NULL;
 error_fopen:
	return -1;
}

static int
_init_fs(void)
{
	_input.fd = fileno(_input.fs);
	if (_input.fd < 0) {
		log_set("Could not obtain file descriptor for file stream: %s",
		        strerror(errno));
		goto error_fileno;
	}

	return 0;

 error_fileno:
	return -1;
}

static int
_init_readline(void)
{
	char const *XDG_STATE_HOME;

	XDG_STATE_HOME = userdirs_get_statedir();

	_input.type = INPUT_READLINE;
	_input.history_file = cstr_format("%s/karuiwm/karuirpc_history",
	                                  XDG_STATE_HOME);
	if (!_input.history_file) {
		log_set("Could not assemble history file path: %s", log_str());
		goto error_histfilepath;
	}

	DEBUG("Reading history from %s", _input.history_file);
	if (read_history(_input.history_file) < 0)
		WARN("Could not read history from %s: %s",
		     _input.history_file, strerror(errno));

	rl_callback_handler_install("> ", _rl_callback);
	rl_attempted_completion_function = _rl_complete;

	return 0;

 error_histfilepath:
	return -1;
}

static int
_init_stdin(void)
{
	_input.fs = stdin;

	if (_init_fs() < 0) {
		log_set("Could not initialise file stream: %s", log_str());
		goto error_fs;
	}

	if (isatty(_input.fd)) {
		if (_init_readline() < 0)
			WARN("Falling back to non-interactive mode, could not initialise readline: %s",
			     log_str());
	} else if (errno != ENOTTY) {
		log_set("Could not determine whether file descriptor %d is connected to a TTY: %s",
		        _input.fd, strerror(errno));
		goto error_tty;
	}

	return 0;

 error_fs:
 error_tty:
	return -1;
}

static void
_rl_callback(char *text)
{
	if (_input.cb_input(text) < 0)
		log_flush(LOG_ERROR);
	if (text)
		cstr_free(text);
}

static char **
_rl_complete(char const *text, int start, int end)
{
	struct list *suggestions;
	char **rlsug, *sug;
	int unsigned i;

	(void) end;

	/* if we return NULL, prevent readline from completing filenames */
	rl_attempted_completion_over = 1;

	/* check if application provided a completion callback */
	if (!_input.cb_completion)
		return NULL;

	/* only complete first word as an action command for now */
	if (start != 0) {
		DEBUG("Not completing non-first word");
		goto out_not_first;
	}

	/* get suggestions from application */
	suggestions = _input.cb_completion(text);
	if (!suggestions) {
		ERROR("Could not determine suggestions for \"%s\": %s",
		      text, log_str());
		goto error_suggestions;
	}
	if (suggestions->size == 0)
		goto out_no_match;

	/* create suggestions array to be returned to readline */
	rlsug = memory_alloc((suggestions->size + 2) * sizeof(char *),
	                     "Suggestions list for readline");
	if (!rlsug)
		goto error_rlsug;
	for (i = 0; i < suggestions->size + 2; ++i)
		rlsug[i] = NULL;

	/* set longest prefix among matching suggestions */
	rlsug[0] = cstr_longest_prefix(suggestions);
	if (!rlsug[0]) {
		log_set("Could not determine longest prefix for matched suggestions: %s",
		        log_str());
		goto error_prefix;
	}
	DEBUG("Longest prefix for \"%s\" › \"%s\" + %zu suggestions", \
	      text, rlsug[0], suggestions->size);

	/* add matching suggestions */
	LIST_FOR (suggestions, i, sug) {
		rlsug[i + 1] = cstr_dup(sug);
		if (!rlsug[i + 1]) {
			log_set("Could not duplicate suggestion %u/%zu: %s",
			        i + 1, suggestions->size, log_str());
			goto error_rlcopy;
		}
	}

	/* clean up and return suggestions */
	list_clear(suggestions, (list_delfunc) cstr_free);
	list_free(suggestions);
	return rlsug;

 out_no_match:
	list_free(suggestions);
 out_not_first:
	return NULL;

 error_rlcopy:
	log_push();
	while (i--)
		cstr_free(rlsug[i + 1]);
	cstr_free(rlsug[0]);
	log_pop();
 error_prefix:
 error_rlsug:
	log_push();
	list_free(suggestions);
	log_pop();
 error_suggestions:
	return NULL;
}
