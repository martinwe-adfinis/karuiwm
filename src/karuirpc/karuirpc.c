#include <karuirpc/input.h>
#include <karuirpc/rpc_client.h>
#include <karui/cstr.h>
#include <karui/envvars.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <karui/userdirs.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h> /* required before readline/readline.h */
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

enum _input_status {
	INPUT_OK,
	INPUT_END,
	INPUT_ERROR,
};

enum _rpc_status {
	RPC_OK,
	RPC_END,
	RPC_ERROR,
};

enum _run_mode {
	MODE_COMMAND,
	MODE_FILE,
	MODE_HELP,
	MODE_STDIN,
	MODE_STDIN_INTERACTIVE,
	MODE_USER_ERROR,
};

static void _handle_input(void);
static void _handle_rpc(void);
static int _init(void);
static int _init_args(int argc, char **argv);
static int _init_input(void);
static int _init_rpc(void);
static int _input_callback(char const *text);
struct list *_input_complete(char const *text);
static char const *_mode_as_string(enum _run_mode mode);
static void _print_help_note(void);
static void _print_response(struct rpc_response const *response);
static void _print_usage(void);
static int _run_interactive(void);
static int _run_noninteractive(void);
static struct rpc_response *_run_command(char const *command);
static void _term(void);
static void _term_args(void);
static void _term_input(void);
static void _term_rpc(void);

static int const E_USER = 1;
static int const E_INPUT = 2;
static int const E_RPC = 3;
static int const E_COMMAND = 4;
static int const E_SYSTEM = 5;
static int const E_LOG = 255;

static struct {
	struct rpc_client *rpc_client;
	enum _run_mode mode;
	enum _input_status input_status;
	enum _rpc_status rpc_status;
	struct {
		char *command;
		bool errexit;
		bool showstatus;
		char *file;
	} args;
	int last_exit;
} _karuirpc = {
	.rpc_client = NULL,
	.mode = MODE_STDIN,
	.input_status = INPUT_OK,
	.rpc_status = RPC_OK,
	.args.command = NULL,
	.args.errexit = false,
	.args.showstatus = false,
	.args.file = NULL,
	.last_exit = 0,
};

static void
_handle_input(void)
{
	if (input_handle() < 0) {
		if (input_get_fd() < 0) {
			_karuirpc.input_status = INPUT_END;
		} else {
			ERROR("Could not handle input: %s", log_str());
			_karuirpc.input_status = INPUT_ERROR;
		}
	}
}

static void
_handle_rpc(void)
{
	struct rpc_response *response;

	if (rpc_client_recv(_karuirpc.rpc_client, &response) < 0) {
		ERROR("Could not read data karuiwm: %s", log_str());
		_karuirpc.rpc_status = RPC_ERROR;
		return;
	} else if (!response) {
		INFO("Karuiwm socket closed");
		_karuirpc.rpc_status = RPC_END;
		return;
	}

	_print_response(response);
	rpc_response_free(response);
}

static int
_init(void)
{
	int retval;

	if (envvars_init() < 0) {
		log_set("Could not initialise environment variables: %s",
		        log_str());
		retval = -E_SYSTEM;
		goto error_env;
	}
	if (userdirs_init() < 0) {
		log_set("Could not initialise user directories: %s", log_str());
		retval = -E_SYSTEM;
		goto error_xdg;
	}
	if (_init_rpc() < 0) {
		log_set("Could not initialise RPC: %s", log_str());
		retval = -E_RPC;
		goto error_rpc;
	}
	if (_init_input() < 0) {
		log_set("Could not initialise input: %s", log_str());
		retval = -E_INPUT;
		goto error_input;
	}
	return 0;

 error_input:
	log_push();
	_term_rpc();
	log_pop();
 error_rpc:
	log_push();
	_term_input();
	log_pop();
 error_xdg:
	log_push();
	userdirs_deinit();
	log_pop();
 error_env:
	return retval;
}

static int
_init_args(int argc, char **argv)
{
	int opt;

	/* optional arguments */
	optind = 1;
	while ((opt = getopt(argc, argv, ":c:dehs")) >= 0) {
		switch (opt) {
		case 'c':
			_karuirpc.mode = MODE_COMMAND;
			_karuirpc.args.command = cstr_dup(optarg);
			if (!_karuirpc.args.command)
				FATAL("Could not duplicate -c argument string: %s",
				      log_str());
			break;
		case 'd':
			log_set_level(LOG_DEBUG);
			DEBUG("Setting karuirpc log level to debug");
			break;
		case 'e':
			_karuirpc.args.errexit = true;
			break;
		case 'h':
			_karuirpc.mode = MODE_HELP;
			/* no need to process further */
			return 0;
		case 's':
			_karuirpc.args.showstatus = true;
			break;
		case ':':
			_karuirpc.mode = MODE_USER_ERROR;
			(void) fprintf(stderr,
			               "Missing argument to option -%c\n",
			               opt);
			_print_help_note();
			return -1;
		case '?':
			_karuirpc.mode = MODE_USER_ERROR;
			(void) fprintf(stderr, "Unknown option -%c\n", opt);
			_print_help_note();
			return -1;
		default:
			FATAL_BUG("Unhandled getopt case: %c [0x%02X]",
			          opt, opt);
		}
	}

	/* positional arguments */
	if (optind < argc) {
		if (_karuirpc.mode == MODE_COMMAND) {
			_karuirpc.mode = MODE_USER_ERROR;
			(void) fprintf(stderr,
			               "Trailing argument for command mode: %s\n",
			               argv[optind]);
			_print_help_note();
		} else {
			_karuirpc.mode = MODE_FILE;
			_karuirpc.args.file = cstr_dup(argv[optind]);
			if (!_karuirpc.args.file)
				FATAL("Could not duplicate positional argument string: %s",
				      log_str());
			++optind;
		}
	}

	/* trailing arguments */
	if (optind < argc) {
		_karuirpc.mode = MODE_USER_ERROR;
		(void) fprintf(stderr, "Trailing argument: %s\n", argv[optind]);
		_print_help_note();
		return -1;
	}

	return 0;
}

static int
_init_input(void)
{
	int retval = 0;

	switch (_karuirpc.mode) {
	case MODE_COMMAND:
		retval = input_init(INPUT_COMMAND, _karuirpc.args.command,
		                    _input_callback);
		break;
	case MODE_FILE:
		retval = input_init(INPUT_FILE, _karuirpc.args.file,
		                    _input_callback);
		break;
	case MODE_STDIN:
		retval = input_init(INPUT_STDIN, NULL, _input_callback);
		break;
	case MODE_HELP:
	case MODE_STDIN_INTERACTIVE:
	case MODE_USER_ERROR:
		FATAL_BUG("Attempt to initialise input with invalid mode %d",
		          _karuirpc.mode);
	}
	if (retval < 0) {
		log_set("Could not initialise input: %s", log_str());
		goto error_input;
	}

	if (input_is_interactive()) {
		_karuirpc.mode = MODE_STDIN_INTERACTIVE;
		if (input_set_completion(_input_complete) < 0) {
			log_set("Could not set input completion function: %s",
			        log_str());
			goto error_completion;
		}
	}

	return 0;

 error_completion:
	log_push();
	input_term();
	log_pop();
 error_input:
	return -1;
}

static int
_init_rpc(void)
{
	_karuirpc.rpc_client = rpc_client_alloc();
	if (!_karuirpc.rpc_client) {
		log_set("Could not create RPC client: %s", log_str());
		goto error_rpc_client;
	}

	return 0;

 error_rpc_client:
	return -1;
}

static int
_input_callback(char const *input)
{
	struct rpc_response *response;

	if (!input) {
		/* input closed */
		_karuirpc.input_status = INPUT_END;
		return 0;
	}

	/* TODO strip whitespace before and after line */

	if (input[0] == '\0') {
		DEBUG("Ignoring empty line");
		return 0;
	}
	if (input[0] == '#') {
		DEBUG("Ignoring commented line");
		return 0;
	}
	
	if (input_is_interactive())
		if (input_add_history(input) < 0)
			WARN("Could not add line to history: %s", log_str());

	DEBUG("Running input as command: %s", input);
	response = _run_command(input);
	if (!response) {
		log_set("Could not run command: %s", log_str());
		goto error_runcmd;
	}
	_print_response(response);
	_karuirpc.last_exit = response->status;
	rpc_response_free(response);

	return 0;

 error_runcmd:
	return -1;
}

struct list *
_input_complete(char const *text)
{
	struct rpc_response *response;
	struct list *suggestions;
	int unsigned i;
	char *rline, *sug;

	/* get all available actions */
	response = _run_command("registry_list_all_actions");
	if (!response) {
		ERROR("Could not get list of available actions from karuiwm: %s",
		      log_str());
		goto error_list_actions;
	}
	if (response->status < 0) {
		ERROR("Could not get list of available actions from karuiwm: %s",
		      response->message);
		goto error_no_actions;
	}
	
	/* find matches */
	suggestions = list_alloc();
	if (!suggestions) {
		log_set("Could not create suggestions list: %s", log_str());
		goto error_suggestions;
	}
	LIST_FOR (response->lines, i, rline) {
		if (!cstr_starts_with(rline, text))
			continue;
		sug = cstr_dup(rline);
		if (!sug) {
			log_set("Could not duplicate matching suggestion: %s",
			        log_str());
			goto error_dup;
		}
		list_append(suggestions, sug);
	}

	/* clean up and return matches */
	rpc_response_free(response);
	return suggestions;

 error_dup:
	log_push();
	list_clear(suggestions, (list_delfunc) cstr_free);
	list_free(suggestions);
	log_pop();
 error_suggestions:
	_karuirpc.input_status = INPUT_ERROR;
 error_no_actions:
	log_push();
	rpc_response_free(response);
	log_pop();
 error_list_actions:
	return NULL;
}

static char const *
_mode_as_string(enum _run_mode mode)
{
	switch (mode) {
	case MODE_COMMAND: return "MODE_COMMAND";
	case MODE_FILE: return "MODE_FILE";
	case MODE_HELP: return "MODE_HELP";
	case MODE_STDIN_INTERACTIVE: return "MODE_STDIN_INTERACTIVE";
	case MODE_STDIN: return "MODE_STDIN";
	case MODE_USER_ERROR: return "MODE_USER_ERROR";
	}

	FATAL_BUG("Unknown run mode: %d", mode);
}

static void
_print_usage(void)
{
	(void) printf(
		"karuirpc - interact with the karuiwm RPC interface\n"
		"\n"
		"Usage: karuirpc -h\n"
		"       karuirpc [OPTIONS...] -c COMMAND\n"
		"       karuirpc [OPTIONS...] [FILE]\n"
		"\n"
		"Options:\n"
		"  -c COMMAND  Run command COMMAND and then exit.\n"
		"  -d          Display debug messages for karuirpc.\n"
		"  -e          Stop processing if a command fails (\"errexit\").\n"
		"  -h          Display this help message and then abort.\n"
		"  -s          Show status output also for successful action calls.\n"
		"\n"
		"If FILE is given, karuirpc reads commands from that file; otherwise it reads commands from stdin.\n"
	);
}

static void
_print_help_note(void)
{
	(void) fprintf(stderr, "Run with -h for help.\n");
}

static void
_print_response(struct rpc_response const *response)
{
	char *line;
	int unsigned i;

	if (response->status < 0)
		(void) printf("\033[31m%d %s\033[0m\n",
		              response->status, response->message);
	if (response->status >= 0 && _karuirpc.args.showstatus)
		(void) printf("\033[32m%d %s\033[0m\n",
		              response->status, response->message);
	LIST_FOR (response->lines, i, line)
		(void) printf("%s\n", line);
}

static struct rpc_response *
_run_command(char const *command)
{
	struct rpc_response *response;

	if (rpc_client_send(_karuirpc.rpc_client, command) < 0) {
		log_set("Could not send command to karuiwm: %s", log_str());
		goto error_send;
	}

	if (rpc_client_recv(_karuirpc.rpc_client, &response) < 0) {
		log_set("Could not receive response from karuiwm: %s",
		        log_str());
		_karuirpc.rpc_status = RPC_ERROR;
		goto error_recv;
	} else if (!response) {
		log_set("Could not receive response from karuiwm: Socket closed");
		_karuirpc.rpc_status = RPC_END;
		goto error_recv;
	}

	return response;

 error_recv:
 error_send:
	return NULL;
}

static int
_run_interactive(void)
{
	int retval;
	fd_set fds;
	int input_fd;

	input_fd = input_get_fd();
	assert(input_fd >= 0);

	for (retval = 0;
	     _karuirpc.input_status == INPUT_OK
	     && _karuirpc.rpc_status == RPC_OK
	     && retval == 0;)
	{
		FD_ZERO(&fds);
		FD_SET(input_fd, &fds);
		FD_SET(_karuirpc.rpc_client->fd, &fds);

		if (select(FD_SETSIZE, &fds, NULL, NULL, NULL) < 0) {
			log_set("select(): %s", strerror(errno));
			retval = -E_SYSTEM;
		} else {
			if (FD_ISSET(input_fd, &fds))
				_handle_input();
			if (FD_ISSET(_karuirpc.rpc_client->fd, &fds))
				_handle_rpc();
		}
	}

	return retval < 0 ? retval
	     : _karuirpc.input_status == INPUT_ERROR ? -E_INPUT
	     : _karuirpc.rpc_status == RPC_ERROR ? -E_RPC
	     : _karuirpc.last_exit < 0 ? -E_COMMAND
	     : 0;
}

static int
_run_noninteractive(void)
{
	while (_karuirpc.input_status == INPUT_OK
	&& _karuirpc.rpc_status == RPC_OK)
	{
		_handle_input();
	}

	return _karuirpc.input_status == INPUT_ERROR ? -E_INPUT
	     : _karuirpc.rpc_status == RPC_ERROR ? -E_RPC
	     : _karuirpc.last_exit < 0 ? -E_COMMAND
	     : 0;
}

static void
_term(void)
{
	_term_input();
	_term_rpc();
	userdirs_deinit();
	envvars_deinit();
}

static void
_term_args(void)
{
	if (_karuirpc.args.command) {
		cstr_free(_karuirpc.args.command);
		_karuirpc.args.command = NULL;
	}
	if (_karuirpc.args.file) {
		cstr_free(_karuirpc.args.file);
		_karuirpc.args.file = NULL;
	}
}

static void
_term_input(void)
{
	input_term();
}

static void
_term_rpc(void)
{
	rpc_client_free(_karuirpc.rpc_client);
}

int
main(int argc, char **argv)
{
	int retval = 0;

	/* global initialisation */
	if (log_init(LOG_INFO, "karuirpc") < 0) {
		retval = -E_LOG;
		goto error_log;
	}
	if (_init_args(argc, argv) < 0) {
		retval = -E_USER;
		goto error_cmdline;
	}
	DEBUG("Run mode: %s", _mode_as_string(_karuirpc.mode));

	/* run */
	switch (_karuirpc.mode) {
	case MODE_HELP:
		_print_usage();
		break;
	case MODE_USER_ERROR:
		FATAL_BUG("Attempt to handle MODE_USER_ERROR in run part");
	case MODE_COMMAND:
	case MODE_FILE:
	case MODE_STDIN_INTERACTIVE:
	case MODE_STDIN:
		retval = _init();
		if (retval < 0) {
			log_flush(LOG_ERROR);
			break;
		}
		if (_karuirpc.mode == MODE_STDIN_INTERACTIVE)
			retval = _run_interactive();
		else
			retval = _run_noninteractive();
		_term();
		break;
	}

	/* global termination and return */
	_term_args();
	log_deinit();
	return -retval;

	/* exception handling */
 error_cmdline:
	log_deinit();
 error_log:
	return E_LOG;
}
