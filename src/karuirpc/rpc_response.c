#include <karuirpc/rpc_response.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>

struct rpc_response *
rpc_response_parse(char const *str)
{
	struct rpc_response *r;
	char *status_line, *status;

	r = memory_alloc(sizeof(struct rpc_response), "RPC response struct");
	if (!r)
		goto error_alloc;

	/* Split into response lines */
	r->lines = cstr_split(str, "\n");
	if (!r->lines) {
		log_set("Could not split response into lines: %s", log_str());
		goto error_split;
	}
	if (r->lines->size == 0) {
		log_set("Received 0 response lines from karuiwm");
		goto error_resplines;
	}
	DEBUG("Received %zu response line%s from karuiwm",
	      r->lines->size, r->lines->size == 1 ? "" : "s");

	/* Extract status code */
	status_line = r->lines->elements[0];
	list_remove(r->lines, status_line, NULL);
	if (cstr_split_once(status_line, " ", &status, &r->message) < 0)
	{
		log_set("Could not split status line \"%s\" into code and message: %s",
		        status_line, log_str());
		goto error_status;
	}
	if (cstr_parse_int(&r->status, status, NULL) < 0) {
		log_set("Could not parse status code: %s", log_str());
		goto error_status_code;
	}

	/* Check status code integrity */
	if (r->status >= 0 && (size_t) r->status != r->lines->size) {
		log_set("Invalid response: status (%d) != number of response lines (%zu)",
		        r->status, r->lines->size);
		goto error_status_integrity;
	}
	
	cstr_free(status);
	cstr_free(status_line);

	return r;

 error_status_integrity:
 error_status_code:
	log_push();
	cstr_free(status);
	cstr_free(r->message);
	log_pop();
 error_status:
	log_push();
	cstr_free(status_line);
	log_pop();
 error_resplines:
	log_push();
	list_clear(r->lines, (list_delfunc) cstr_free);
	list_free(r->lines);
	log_pop();
 error_split:
	log_push();
	memory_free(r);
	log_pop();
 error_alloc:
	return NULL;
}

void
rpc_response_free(struct rpc_response *r)
{
	cstr_free(r->message);
	list_clear(r->lines, (list_delfunc) cstr_free);
	list_free(r->lines);
	memory_free(r);
}
