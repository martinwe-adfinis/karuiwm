#ifndef _KRPC_INPUT_H
#define _KRPC_INPUT_H

enum input_type {
	INPUT_COMMAND,
	INPUT_FILE,
	INPUT_READLINE,
	INPUT_STDIN,
};

#include <karui/list.h>

int input_init(enum input_type type, char const *arg, int (*cb)(char const *));
void input_term(void);

int input_add_history(char const *line);
int input_get_fd(void);
int input_handle(void);
bool input_is_interactive(void);
int input_set_completion(struct list *(*cb)(char const *));
char const *input_type_as_string(enum input_type type);

#endif /* ndef _KRPC_INPUT_H */
