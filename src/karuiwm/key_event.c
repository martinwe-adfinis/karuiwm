#include <karuiwm/key_event.h>
#include <karui/log.h>

char const *
key_event_type_str(enum key_event_type kevtype)
{
	switch (kevtype) {
	case KEY_EVENT_CURRENT: return "KEY_EVENT_CURRENT";
	case KEY_PRESS: return "KEY_PRESS";
	case KEY_RELEASE: return "KEY_RELEASE";
	}
	FATAL_BUG("Unknown key event type %d", kevtype);
}
