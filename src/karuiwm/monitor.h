#ifndef _KWM_MONITOR_H
#define _KWM_MONITOR_H

#include <karuiwm/rectangle.h>
#include <stdbool.h>

struct monitor {
	struct workspace *workspace, *scratchpad, *selws;
	struct xscreen *xscreen;
	struct rectangle dim;
	struct {
		int unsigned top, right, bottom, left;
	} margins;
	bool focused;
	struct bus_session *bus_session;
	struct {
		struct bus_subscription *workspace_layout;
		struct bus_subscription *scratchpad_layout;
	} bus_subscriptions;
};

#include <karuiwm/state.h>
#include <karuiwm/xscreen.h>
#include <karuiwm/workspace.h>

struct monitor *monitor_alloc(struct xscreen *xscr);
void monitor_free(struct monitor *mon);

int monitor_arrange(struct monitor *mon);
void monitor_focus(struct monitor *mon, bool focus);
bool monitor_has_xscreen(struct monitor const *mon, struct xscreen const *xscr);
void monitor_restack(struct monitor *mon);
int monitor_show_scratchpad(struct monitor *mon, struct workspace *pad);
void monitor_show_workspace(struct monitor *mon, struct workspace *ws);

#endif /* ndef _KWM_MONITOR_H */
