#include <karuiwm/commandline_argument.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>

struct commandline_argument *
commandline_argument_alloc(char const *key, char const *value)
{
	struct commandline_argument *cla;

	cla = memory_alloc(sizeof(struct commandline_argument),
	                   "commandline argument");
	if (!cla)
		goto error_alloc;

	cla->key = cstr_dup(key);
	if (!cla->key) {
		log_set("Could not duplicate key string: %s", log_str());
		goto error_key;
	}

	cla->value = cstr_dup(value);
	if (!cla->value) {
		log_set("Could not duplicate value string: %s", log_str());
		goto error_value;
	}

	return cla;

 error_value:
	log_push();
	cstr_free(cla->key);
	log_pop();
 error_key:
	log_push();
	memory_free(cla);
	log_pop();
 error_alloc:
	return NULL;
}

void
commandline_argument_free(struct commandline_argument *cla)
{
	cstr_free(cla->key);
	cstr_free(cla->value);
	memory_free(cla);
}

bool
commandline_argument_has_key(struct commandline_argument const *cla,
                             char const *key)
{
	return cstr_equals(cla->key, key);
}

struct commandline_argument *
commandline_argument_parse(char const *str)
{
	struct commandline_argument *cla;

	cla = memory_alloc(sizeof(struct commandline_argument),
	                   "commandline argument");
	if (!cla)
		goto error_alloc;

	if (cstr_split_once(str, "=", &cla->key, &cla->value) < 0) {
		log_set("Could not split argument into key and value: %s",
		        log_str());
		goto error_split;
	}

	return cla;

 error_split:
	log_push();
	memory_free(cla);
	log_pop();
 error_alloc:
	return NULL;
}
