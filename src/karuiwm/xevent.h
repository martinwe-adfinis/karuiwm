#ifndef _KWM_XEVENT_H
#define _KWM_XEVENT_H

#include <karuiwm/button.h>
#include <karuiwm/button_event.h>
#include <karuiwm/key.h>
#include <karuiwm/key_event.h>
#include <karuiwm/pointer.h>
#include <karuiwm/pointer_event.h>
#include <karuiwm/position.h>

struct xevent_button {
	enum button_event_type type;
	struct button button;
	struct position position;
	struct xwindow *xwindow;
};

struct xevent_key {
	enum key_event_type type;
	struct key key;
};

struct xevent_pointer {
	enum pointer_event_type type;
	struct pointer pointer;
	struct xwindow *xwindow;
};

#endif /* ndef _KWM_XEVENT_H */
