#include <karuiwm/xoutput.h>
#include <karuiwm/bus.h>
#include <karuiwm/xscreen.h>
#include <karuiwm/xserver.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <X11/extensions/Xinerama.h>

static int _insert_screen(int unsigned pos, struct rectangle dim);
static int _scan_screens_non_xinerama(void);
static int _scan_screens_xinerama(void);
static int _scan_screens_xinerama_deduplicate(XineramaScreenInfo **infos,
                                              size_t *ninfos);

struct xoutput xoutput;

int
xoutput_init(void)
{
	xoutput.screen = DefaultScreen(xserver.display);
	if (xoutput.screen < 0) {
		log_set("Could not obtain default screen from X server (%d)",
		        xoutput.screen);
		goto error_screen;
	}

	xoutput.screen_depth = (int unsigned) DefaultDepth(xserver.display,
	                                                   xoutput.screen);
	if (!xoutput.screen_depth) {
		log_set("Could not obtain default screen depth from X server");
		goto error_screen_depth;
	}

	xoutput.screens = list_alloc();
	if (!xoutput.screens) {
		log_set("Could not create list of screens: %s", log_str());
		goto error_screens;
	}

	if (xoutput_scan() < 0) {
		log_set("Could not scan screens: %s", log_str());
		goto error_scan;
	}

	return 0;

 error_scan:
	log_push();
	list_clear(xoutput.screens, (list_delfunc) xscreen_free);
	list_free(xoutput.screens);
	log_pop();
 error_screens:
 error_screen_depth:
 error_screen:
	return -1;
}

void
xoutput_deinit(void)
{
	list_clear(xoutput.screens, (list_delfunc) xscreen_free);
	list_free(xoutput.screens);
}

int
xoutput_scan(void)
{
	if (XineramaIsActive(xserver.display))
		return _scan_screens_xinerama();
	else
		return _scan_screens_non_xinerama();
}

static int
_insert_screen(int unsigned pos, struct rectangle dim)
{
	struct xscreen *xscr;

	if (pos == xoutput.screens->size) {
		/* index next in list, create new screen */
		DEBUG("Setting up new X screen %u at %ux%u%+d%+d",
		      pos, dim.width, dim.height, dim.x, dim.y);
		xscr = xscreen_alloc(dim);
		if (!xscr) {
			log_set("Could not create new X screen: %s", log_str());
			return -1;
		}
		list_append(xoutput.screens, xscr);
		bus_channel_send(bus.x_screen_attach, xscr);
	} else if (pos < xoutput.screens->size) {
		/* index within list, use existing screen */
		DEBUG("Reusing existing X screen %u at %ux%u%+d%+d",
		      pos, dim.width, dim.height, dim.x, dim.y);
		xscr = xoutput.screens->elements[pos];
		if (xscreen_moveresize(xscr, dim) < 0) {
			log_set("Could not move-resize X screen %u to %ux%u%+d%+d: %s",
			        pos, dim.width, dim.height, dim.x, dim.y,
			        log_str());
			return -1;
		}
	} else {
		/* index out of bound */
		FATAL_BUG("Attempt to set up X screen %u in list of size %zu",
		          pos, xoutput.screens->size);
	}

	return 0;
}

static int
_scan_screens_non_xinerama(void)
{
	struct xscreen *xscr;
	struct rectangle dim;
	int signed xdw, xdh;

	/* get and check information from X */
	xdw = DisplayWidth(xserver.display, xoutput.screen);
	if (xdw < 0) {
		log_set("DisplayWidth() returned negative value (%d)", xdw);
		return -1;
	}
	xdh = DisplayHeight(xserver.display, xoutput.screen);
	if (xdh < 0) {
		log_set("DisplayHeight() returned negative value (%d)", xdh);
		return -1;
	}
	dim.x = 0;
	dim.y = 0;
	dim.width = (int unsigned) xdw;
	dim.height = (int unsigned) xdh;

	/* set up first screen */
	DEBUG("Setting up for scanned non-xinerama screen with %ux%u%+d%+d",
	      dim.width, dim.height, dim.x, dim.y);
	if (_insert_screen(0, dim) < 0) {
		log_set("Could not set up non-Xinerama screen 0 with %ux%u%+d%+d: %s",
		        dim.width, dim.height, dim.x, dim.y, log_str());
		goto error_setup;
	}

	/* remove superfluous screens */
	while (xoutput.screens->size > 1) {
		DEBUG("Non-Xinerama scanning with %zu > 1 screens, removing one screen",
		      xoutput.screens->size);
		xscr = xoutput.screens->elements[1];
		list_remove(xoutput.screens, xscr, NULL);
		bus_channel_send(bus.x_screen_detach, xscr);
		xscreen_free(xscr);
	}

	return 0;

 error_setup:
	return -1;
}

static int
_scan_screens_xinerama(void)
{
	XineramaScreenInfo *infos;
	int n_xinerama;
	size_t ninfos;
	struct xscreen *xscr;
	int unsigned i;
	struct rectangle dim;

	/* get and check information from Xinerama */
	infos = XineramaQueryScreens(xserver.display, &n_xinerama);
	if (n_xinerama <= 0) {
		log_set("Received non-positive number (%d) of Xinerama screen info objects",
		        n_xinerama);
		goto error_xinerama_query;
	}
	ninfos = (size_t) n_xinerama;
	for (i = 0; i < ninfos; ++i) {
		if (infos[i].width < 0) {
			log_set("Received negative width (%d) for Xinerama screen %u",
			        infos[i].width, i);
			goto error_xinerama_negative_size;
		}
		if (infos[i].height < 0) {
			log_set("Received negative height (%d) for Xinerama screen %u",
			        infos[i].height, i);
			goto error_xinerama_negative_size;
		}
	}
	if (_scan_screens_xinerama_deduplicate(&infos, &ninfos) < 0) {
		log_set("Could not deduplicate Xinerama screen info list: %s",
		        log_str());
		goto error_xinerama_deduplicate;
	}

	/* set up screens */
	for (i = 0; i < ninfos; ++i) {
		dim.x = infos[i].x_org;
		dim.y = infos[i].y_org;
		dim.width = (int unsigned) infos[i].width;
		dim.height = (int unsigned) infos[i].height;
		DEBUG("Setting up for scanned Xinerama screen[%u]: %ux%u%+d%+d",
		      i, dim.width, dim.height, dim.x, dim.y);
		if (_insert_screen(i, dim) < 0) {
			log_set("Could not set up Xenerama screen[%u] for %ux%u%+d%+d: %s",
			        i, dim.width, dim.height, dim.x, dim.y,
			        log_str());
			goto error_setup;
		}
	}

	/* remove superfluous screens */
	while (ninfos < xoutput.screens->size) {
		DEBUG("Xinerama scanning with %zu > %zu screens, removing screen %zu",
		      xoutput.screens->size, ninfos, xoutput.screens->size - 1);
		xscr = xoutput.screens->elements[xoutput.screens->size - 1];
		list_remove(xoutput.screens, xscr, NULL);
		bus_channel_send(bus.x_screen_detach, xscr);
		xscreen_free(xscr);
	}

	XFree(infos);
	return 0;

 error_setup:
 error_xinerama_deduplicate:
 error_xinerama_negative_size:
	XFree(infos);
 error_xinerama_query:
	return -1;
}

static int
_scan_screens_xinerama_deduplicate(XineramaScreenInfo **infos, size_t *ninfos)
{
	int unsigned i, id;
	XineramaScreenInfo *infos_dedup;
	size_t ninfos_dedup;
	bool is_dup;

	/* create list for deduplicated screen info objects */
	infos_dedup = memory_alloc(*ninfos * sizeof(XineramaScreenInfo),
	                           "deduplicated Xinerama screen info objects");
	if (!infos_dedup)
		goto error_alloc_dedup;

	/* scan info objects */
	for (i = 0, ninfos_dedup = 0; i < *ninfos; ++i) {
		for (is_dup = false, id = 0; !is_dup && id < ninfos_dedup; ++id)
			is_dup = (*infos)[i].x_org == infos_dedup[id].x_org
			      && (*infos)[i].y_org == infos_dedup[id].y_org
			      && (*infos)[i].width == infos_dedup[id].width
			      && (*infos)[i].height == infos_dedup[id].height;
		if (!is_dup) {
			infos_dedup[ninfos_dedup] = (XineramaScreenInfo) {
				.x_org = (*infos)[i].x_org,
				.y_org = (*infos)[i].y_org,
				.width = (*infos)[i].width,
				.height = (*infos)[i].height,
			};
			++ninfos_dedup;
		}
	}

	if (memory_resize((void **) &infos_dedup,
	                  ninfos_dedup * sizeof(XineramaScreenInfo),
	                  "deduplicated Xinerama screen info objects") < 0)
		log_flush(LOG_WARNING);

	XFree(*infos);
	*infos = infos_dedup;
	*ninfos = ninfos_dedup;
	return 0;

 error_alloc_dedup:
	return -1;
}
