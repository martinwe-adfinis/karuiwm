#include <karuiwm/wsmbox.h>
#include <karuiwm/bus.h>
#include <karuiwm/bus_channel.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/seat.h>
#include <karuiwm/wsmap.h>
#include <karuiwm/xfont.h>
#include <karuiwm/xoutput.h>
#include <karuiwm/xserver.h>
#include <karuiwm/xwindow.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <string.h>

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static int _render(struct wsmbox *wb);

struct wsmbox *
wsmbox_alloc(struct workspace *ws)
{
	struct wsmbox *wb;
	struct rectangle dim;
	int long unsigned event_mask;

	wb = memory_alloc(sizeof(struct wsmbox), "workspace map box structure");
	if (!wb)
		goto error_box;

	wb->workspace = ws;
	wb->size = wsmap.size;
	wb->position = (struct position) { 0, 0 };

	/* window */
	dim = (struct rectangle) {
		.x = 0,
		.y = 0,
		.size = wb->size,
	};
	event_mask = XWINDOW_EVENT_MASK_EXPOSURE;
	wb->xwindow = xserver_create_xwindow(dim, wsmap.border_width,
	                                     event_mask, true);
	if (!wb->xwindow) {
		log_set("Could not create X window: %s", log_str());
		goto error_window;
	}
	wb->mapped = false;

	/* content */
	wb->pixmap = XCreatePixmap(xserver.display, xserver.root->id,
	                           wb->size.width, wb->size.height,
	                           xoutput.screen_depth);
	if (!wb->pixmap) {
		log_set("Could not create pixmap with size %ux%u",
		        wb->size.width, wb->size.height);
		goto error_pixmap;
	}
	if (wsmbox_update_content(wb) < 0)
		goto error_content;

	/* bus events */
	wb->bus.session = bus_connect("wsmbox", _bus_event, wb);
	if (!wb->bus.session) {
		log_set("Could not register on event bus: %s", log_str());
		goto error_bus_register;
	}

	wb->bus.sub_ws_rename = bus_subscription_alloc(wb->bus.session,
	                                               bus.workspace_rename,
	                                               wb->workspace);
	if (!wb->bus.sub_ws_rename) {
		log_set("Could not join event bus channel '%s': %s",
		        bus.workspace_rename->name, log_str());
		goto error_bus_subscribe;
	}
	if (!bus_subscription_alloc(wb->bus.session, bus.x_window_notify_expose,
	                            wb->xwindow))
	{
		log_set("Could not join event bus channel '%s': %s",
		        bus.x_window_notify_expose->name, log_str());
		goto error_bus_subscribe;
	}

	return wb;

 error_bus_subscribe:
	log_push();
	bus_disconnect(wb->bus.session);
	log_pop();
 error_bus_register:
	/* nothing to revert for WSM box content update */
 error_content:
	(void) XFreePixmap(xserver.display, wb->pixmap);
 error_pixmap:
	log_push();
	xserver_destroy_xwindow(wb->xwindow);
	log_pop();
 error_window:
	log_push();
	memory_free(wb);
	log_pop();
 error_box:
	return NULL;
}

void
wsmbox_free(struct wsmbox *wb)
{
	wsmbox_show(wb, false);
	bus_disconnect(wb->bus.session);
	XFreePixmap(xserver.display, wb->pixmap);
	xserver_destroy_xwindow(wb->xwindow);
	memory_free(wb);
}

bool
wsmbox_has_position(struct wsmbox const *wb, struct position const *pos)
{
	return position_equals(&wb->position, pos);
}

bool
wsmbox_has_workspace(struct wsmbox const *wb, struct workspace const *ws)
{
	return wb->workspace == ws;
}

int
wsmbox_moveresize(struct wsmbox *wb, struct rectangle coord)
{
	Pixmap newpm;
	bool size_changed;

	size_changed = !size_equals(&wb->size, &coord.size);
	if (size_changed) {
		newpm = XCreatePixmap(xserver.display, xserver.root->id,
		                      coord.width, coord.height,
		                      xoutput.screen_depth);
		if (!newpm) {
			log_set("Could not create new pixmap with size %ux%u",
			        coord.width, coord.height);
			goto error_pixmap;
		}
	}

	if (xwindow_moveresize(wb->xwindow, coord, false) < 0) {
		log_set("Could not move-resize WSM box X window %lu to %ux%u%+d%+d: %s",
		        wb->xwindow->id, coord.width, coord.height,
		        coord.x, coord.y, log_str());
		goto error_resize;
	}
	wb->size = coord.size;

	if (size_changed) {
		XFreePixmap(xserver.display, wb->pixmap);
		wb->pixmap = newpm;

		if (wsmbox_update_content(wb) < 0)
			ERROR("Could not update WSM box '%s' content: %s",
			      wb->workspace ? wb->workspace->name : NULL, log_str());
	}

	return 0;

 error_resize:
	if (size_changed)
		XFreePixmap(xserver.display, newpm);
 error_pixmap:
	return -1;
}

int
wsmbox_set_workspace(struct wsmbox *wb, struct workspace *ws)
{
	if (bus_subscription_set_data_mask(wb->bus.sub_ws_rename, ws) < 0) {
		log_set("Could not update bus subscription data mask: %s",
		        log_str());
		goto error_bus_mask;
	}
	wb->workspace = ws;

	if (wsmbox_update_content(wb) < 0)
		/* not much we can do at this point, just print an error */
		ERROR("Could not update box content after changing workspace: %s",
		      log_str());

	return 0;

 error_bus_mask:
	return -1;
}

int
wsmbox_show(struct wsmbox *wb, bool visible)
{
	if (visible == wb->mapped)
		return 0;

	if (visible) {
		if (xwindow_map(wb->xwindow) < 0) {
			log_set("Could not map X window %lu: %s",
			        wb->xwindow->id, log_str());
			goto error_map;
		}
		wb->mapped = true;
		if (!XRaiseWindow(xserver.display, wb->xwindow->id)) {
			ERROR("WSM box: XRaiseWindow(%lu) failed",
			      wb->xwindow->id);
		}
		if (wsmbox_update_border(wb) < 0)
			WARN("Could not update border: %s", log_str());
		if (wsmbox_update_content(wb) < 0)
			WARN("Could not update content: %s", log_str());
	} else {
		if (xwindow_unmap(wb->xwindow) < 0) {
			log_set("Could not unmap X window %lu: %s",
			        wb->xwindow->id, log_str());
			goto error_map;
		}
		wb->mapped = false;
	}
	return 0;

 error_map:
	return -1;
}

int
wsmbox_update_border(struct wsmbox *wb)
{
	bool focused, targeted;
	struct colourset c;
	int ret = 0;

	if (!wb->mapped)
		return 0;

	focused = wb->workspace && wb->workspace->mon;
	targeted = wb == wsmap.target;
	c = targeted ? wsmap.colour_targeted
	  : focused  ? wsmap.colour_focused
	  :            wsmap.colour_unfocused;

	if (xwindow_set_border_colour(wb->xwindow, c.border) < 0) {
		log_set("Could not set border colour to #%06X: %s",
		        c.border, log_str());
		ret = -1;
	}

	if (xwindow_set_border_width(wb->xwindow, wsmap.border_width) < 0) {
		log_set("Could not set border width to %u: %s",
		        wsmap.border_width, log_str());
		ret = -1;
	}

	return ret;
}

int
wsmbox_update_content(struct wsmbox *wb)
{
	bool focused, targeted;
	struct colourset c;
	char const *name;

	focused = wb->workspace && wb->workspace->mon;
	targeted = wb == wsmap.target;
	c = targeted ? wsmap.colour_targeted
	  : focused  ? wsmap.colour_focused
	  :            wsmap.colour_unfocused;
	name = wb->workspace ? wb->workspace->name : "";

	/* background */
	XSetForeground(xserver.display, xserver.graphics_context, c.bg);
	XFillRectangle(xserver.display, wb->pixmap, xserver.graphics_context,
	               0, 0, wb->size.width, wb->size.height);

	/* foreground */
	XSetForeground(xserver.display, xserver.graphics_context, c.fg);
	Xutf8DrawString(xserver.display, wb->pixmap, wsmap.xfont->fontset,
	                xserver.graphics_context,
	                2, (int signed) wsmap.xfont->ascent + 1,
	                name, (int signed) strlen(name));

	if (wb->mapped)
		if (_render(wb) < 0) {
			log_set("Could not render content: %s", log_str());
			return -1;
		}

	XSync(xserver.display, False);

	return 0;
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct wsmbox *wb = ctx;
	struct xwindow *xwin;
	struct workspace *ws;

	if (ch == bus.workspace_rename) {
		ws = data;
		if (wsmbox_update_content(wb) < 0)
			ERROR("Could not update WSM box content after workspace rename event on '%s': %s",
			      ws->name, log_str());
	} else if (ch == bus.x_window_notify_expose) {
		xwin = data;
		if (xwin != wb->xwindow) {
			BUG("Bus event '%s' for X window %lu not managed by WSM box, ignoring",
			    ch->name, xwin->id);
			return;
		}
		if (wsmbox_update_content(wb) < 0)
			WARN("Could not update WSM box window: %s", log_str());
	} else {
		BUG("WSM box received bus event from unexpected channel '%s'",
		    ch->name);
	}
}

static int
_render(struct wsmbox *wb)
{
	(void) XCopyArea(xserver.display, wb->pixmap, wb->xwindow->id,
	                 xserver.graphics_context,
	                 0, 0, wb->size.width, wb->size.height, 0, 0);
	return 0;
}
