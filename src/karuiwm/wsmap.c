#include <karuiwm/wsmap.h>
#include <karuiwm/action.h>
#include <karuiwm/bus.h>
#include <karuiwm/commandline.h>
#include <karuiwm/input.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/registry.h>
#include <karuiwm/seat.h>
#include <karuiwm/utils.h>
#include <karuiwm/workspace.h>
#include <karuiwm/wsmbox.h>
#include <karuiwm/xfont.h>
#include <karuiwm/xserver.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct wsmap wsmap;

struct _wsmaction {
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;
};

static int _action_set_border_width(struct list **results, struct list const *args);
static int _action_set_colour_focused(struct list **results, struct list const *args);
static int _action_set_colour_targeted(struct list **results, struct list const *args);
static int _action_set_colour_unfocused(struct list **results, struct list const *args);
static int _action_set_font(struct list **results, struct list const *args);
static int _action_set_size(struct list **results, struct list const *args);
static int _action_target_focus(struct list **results, struct list const *args);
static int _action_target_shift(struct list **results, struct list const *args);
static int _action_target_step(struct list **results, struct list const *args);
static int _action_toggle(struct list **results, struct list const *args);
static int _action_window_shift(struct list **results, struct list const *args);
static int _action_workspace_step(struct list **results, struct list const *args);

static int _add_workspace(struct workspace *ws);
static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static bool _find_box_by_position(struct wsmbox **wb, struct position pos);
static bool _find_box_by_workspace(struct wsmbox **wb,
                                   struct workspace const *ws);
static int _focusws(struct monitor *mon, struct position pos);
static int _moveresize(struct wsmbox *wb);
static int _moveresize_all(void);
static FILE *_savefile_create(void);
static int _savefile_scan(void);
static int _savefile_scan_workspace(FILE *fs);
static int _savefile_scan_wsm(FILE *fs);
static int _savefile_write(void);
static int _state_scan(void);
static int _target_step(enum direction dir);
static int _updateborder_all(void);
static int _updatecontent_all(void);
static int _update_commandline(void);
static int _workspace_step(enum direction dir);

static char const *_CLA_SAVEFILE = "wsm.savefile";

static int unsigned const _DEFAULT_WIDTH = 90;
static int unsigned const _DEFAULT_HEIGHT = 60;
static int unsigned const _DEFAULT_BORDER_WIDTH = 2;
static uint32_t const _DEFAULT_TARGETED_FG      = 0xAFD700;
static uint32_t const _DEFAULT_TARGETED_BG      = 0x444444;
static uint32_t const _DEFAULT_TARGETED_BORDER  = 0xAFD700;
static uint32_t const _DEFAULT_FOCUSED_FG       = 0xCCCCCC;
static uint32_t const _DEFAULT_FOCUSED_BG       = 0x444444;
static uint32_t const _DEFAULT_FOCUSED_BORDER   = 0x888888;
static uint32_t const _DEFAULT_UNFOCUSED_FG     = 0x888888;
static uint32_t const _DEFAULT_UNFOCUSED_BG     = 0x222222;
static uint32_t const _DEFAULT_UNFOCUSED_BORDER = 0x888888;
static char const *const _DEFAULT_FONT =
	"-misc-fixed-medium-r-semicondensed--13-100-100-100-c-60-iso8859-1";

#define _(...) (enum value_type const[]) { __VA_ARGS__ }
static struct _wsmaction const _wsmactions[] = {
	{ "wsm_set_border_width", _action_set_border_width,
	  1, _(VALUE_UNSIGNED_INTEGER) },
	{ "wsm_set_colour_targeted", _action_set_colour_targeted,
	  3, _(VALUE_COLOUR, VALUE_COLOUR, VALUE_COLOUR) },
	{ "wsm_set_colour_focused", _action_set_colour_focused,
	  3, _(VALUE_COLOUR, VALUE_COLOUR, VALUE_COLOUR) },
	{ "wsm_set_colour_unfocused", _action_set_colour_unfocused,
	  3, _(VALUE_COLOUR, VALUE_COLOUR, VALUE_COLOUR) },
	{ "wsm_set_size", _action_set_size,
	  2, _(VALUE_UNSIGNED_INTEGER, VALUE_UNSIGNED_INTEGER) },
	{ "wsm_set_font", _action_set_font,
	  1, _(VALUE_STRING) },

	{ "wsm_target_focus",         _action_target_focus,
	  0, NULL },
	{ "wsm_target_shift",         _action_target_shift,
	  1, _(VALUE_DIRECTION) },
	{ "wsm_target_step",          _action_target_step,
	  1, _(VALUE_DIRECTION) },
	{ "wsm_toggle",               _action_toggle,
	  0, NULL },

	{ "wsm_window_shift",         _action_window_shift,
	  2, _(VALUE_DIRECTION, VALUE_BOOLEAN) },

	{ "wsm_workspace_step",       _action_workspace_step,
	  1, _(VALUE_DIRECTION) },
};
#undef _

static struct {
	struct registry_session *regses;
	struct bus_session *busses;
	char *savefile;
} _wsmap;

int
wsmap_init(void)
{
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;
	int unsigned i;

	_wsmap.savefile = NULL;

	/* map state */
	wsmap.active = false;

	/* key map for map interaction */
	if (input_create_keymap("wsm", true) < 0) {
		log_set("Could not create keymap 'wsm': %s", log_str());
		goto error_keymap;
	}

	/* default configuration */
	wsmap.border_width = _DEFAULT_BORDER_WIDTH;
	wsmap.size.width = _DEFAULT_WIDTH;
	wsmap.size.height = _DEFAULT_HEIGHT;
	wsmap.colour_focused.fg = _DEFAULT_FOCUSED_FG;
	wsmap.colour_focused.bg = _DEFAULT_FOCUSED_BG;
	wsmap.colour_focused.border = _DEFAULT_FOCUSED_BORDER;
	wsmap.colour_targeted.fg = _DEFAULT_TARGETED_FG;
	wsmap.colour_targeted.bg = _DEFAULT_TARGETED_BG;
	wsmap.colour_targeted.border = _DEFAULT_TARGETED_BORDER;
	wsmap.colour_unfocused.fg = _DEFAULT_UNFOCUSED_FG;
	wsmap.colour_unfocused.bg = _DEFAULT_UNFOCUSED_BG;
	wsmap.colour_unfocused.border = _DEFAULT_UNFOCUSED_BORDER;
	wsmap.xfont = xfont_alloc(xserver.display, karuiwm.locale,
	                          _DEFAULT_FONT);
	if (!wsmap.xfont) {
		log_set("Could not load X font set '%s': %s",
		        _DEFAULT_FONT, log_str());
		goto error_font;
	}

	/* actions */
	_wsmap.regses = registry_connect("wsmap");
	if (!_wsmap.regses) {
		log_set("Could not open registry: %s", log_str());
		goto error_registry;
	}
	for (i = 0; i < LENGTH(_wsmactions); ++i) {
		name = _wsmactions[i].name;
		func = _wsmactions[i].func;
		nargs = _wsmactions[i].nargs;
		arg_types = _wsmactions[i].arg_types;

		if (registry_create_action(_wsmap.regses,
		                           name, func, nargs, arg_types) < 0)
		{
			log_set("Could not register action '%s': %s",
			        name, log_str());
			goto error_register_action;
		}
	}

	/* WSM boxes */
	wsmap.boxes = list_alloc();
	if (!wsmap.boxes) {
		log_set("Could not create workspace map boxes list: %s",
		        log_str());
		goto error_boxlist;
	}

	/* target box */
	wsmap.target = wsmbox_alloc(NULL);
	if (!wsmap.target) {
		log_set("Could not create target WSM box: %s", log_str());
		goto error_dummy_wb;
	}

	/* initial set of boxes */
	if (_savefile_scan() < 0)
		WARN("Could not restore WSM from savefile: %s", log_str());
	if (_state_scan() < 0) {
		log_set("Could not scan state for existing workspaces: %s",
		        log_str());
		goto error_scan_state;
	}

	/* workspace events */
	_wsmap.busses = bus_connect("wsm", _bus_event, NULL);
	if (!_wsmap.busses) {
		log_set("Could not register on event bus: %s", log_str());
		goto error_bus_register;
	}
	if (!bus_subscription_alloc(_wsmap.busses, bus.workspace_attach,
	                            BUS_SUBSCRIPTION_DATA_MASK_ALL))
	{
		log_set("Could not join event bus channel '%s': %s",
		        bus.workspace_attach->name, log_str());
		goto error_bus_subscribe;
	}
	if (!bus_subscription_alloc(_wsmap.busses, bus.workspace_detach,
	                            BUS_SUBSCRIPTION_DATA_MASK_ALL))
	{
		log_set("Could not join event bus channel '%s': %s",
		        bus.workspace_detach->name, log_str());
		goto error_bus_subscribe;
	}

	return 0;

 error_bus_subscribe:
	log_push();
	bus_disconnect(_wsmap.busses);
	log_pop();
 error_bus_register:
	/* boxes are removed when cleaning up boxes list below */
 error_scan_state:
	log_push();
	wsmbox_free(wsmap.target);
	log_pop();
 error_dummy_wb:
	log_push();
	list_clear(wsmap.boxes, (list_delfunc) wsmbox_free);
	list_free(wsmap.boxes);
	log_pop();
 error_boxlist:
	/* closing registry below will clean up actions */
 error_register_action:
	log_push();
	registry_disconnect(_wsmap.regses);
	log_pop();
 error_registry:
	log_push();
	xfont_free(wsmap.xfont);
	log_pop();
 error_font:
	log_push();
	if (input_destroy_keymap("wsm") < 0)
		CRITICAL("Could not destroy keymap 'wsm' while handling error: %s",
		         log_str());
	log_pop();
 error_keymap:
	return -1;
}

void
wsmap_deinit(bool restart)
{
	if (wsmap.active) {
		if (_action_toggle(NULL, NULL) < 0)
			WARN("Could not toggle active WSM before deinitialising: %s",
			     log_str());
	}

	bus_disconnect(_wsmap.busses);

	if (restart) {
		if (_savefile_write() < 0)
			WARN("Could not save WSM state to savefile: %s",
			     log_str());
		else if (_update_commandline() < 0)
			WARN("Could not set WSM savefile in commandline: %s",
			     log_str());
	}
	registry_disconnect(_wsmap.regses);
	xfont_free(wsmap.xfont);
	input_destroy_keymap("wsm");
	wsmbox_free(wsmap.target);
	list_clear(wsmap.boxes, (list_delfunc) wsmbox_free);
	list_free(wsmap.boxes);
	wsmap.boxes = NULL;
}

static int
_action_set_border_width(struct list **results, struct list const *args)
{
	(void) results;

	wsmap.border_width = ((struct value *) args->elements[0])->data.ui;

	if (_updateborder_all() < 0) {
		log_set("Could not update border of all WSM boxes: %s",
		        log_str());
		return -1;
	}

	if (_moveresize_all() < 0) {
		log_set("Could not rearrange boxes after updating border: %s",
		        log_str());
		return -1;
	}

	return 0;
}

static int
_action_set_colour_focused(struct list **results, struct list const *args)
{
	(void) results;

	wsmap.colour_focused = (struct colourset) {
		.fg = ((struct value *) args->elements[0])->data.col,
		.bg = ((struct value *) args->elements[1])->data.col,
		.border = ((struct value *) args->elements[2])->data.col,
	};

	if (_updateborder_all() < 0) {
		log_set("Could not update border of all WSM boxes: %s",
		        log_str());
		return -1;
	}

	return 0;
}

static int
_action_set_colour_targeted(struct list **results, struct list const *args)
{
	(void) results;

	wsmap.colour_targeted = (struct colourset) {
		.fg = ((struct value *) args->elements[0])->data.col,
		.bg = ((struct value *) args->elements[1])->data.col,
		.border = ((struct value *) args->elements[2])->data.col,
	};

	if (_updateborder_all() < 0) {
		log_set("Could not update border of all WSM boxes: %s",
		        log_str());
		return -1;
	}

	return 0;
}

static int
_action_set_colour_unfocused(struct list **results, struct list const *args)
{
	(void) results;

	wsmap.colour_unfocused = (struct colourset) {
		.fg = ((struct value *) args->elements[0])->data.col,
		.bg = ((struct value *) args->elements[1])->data.col,
		.border = ((struct value *) args->elements[2])->data.col,
	};

	if (_updateborder_all() < 0) {
		log_set("Could not update border of all WSM boxes: %s",
		        log_str());
		return -1;
	}

	return 0;
}

static int
_action_set_font(struct list **results, struct list const *args)
{
	char const *fontname;
	struct xfont *xf_new;
	(void) results;

	/* create new font */
	fontname = ((struct value *) args->elements[0])->data.str;
	xf_new = xfont_alloc(xserver.display, karuiwm.locale, fontname);
	if (!xf_new) {
		log_set("Could not load new X font for '%s': %s",
		        fontname, log_str());
		return -1;
	}

	/* replace old font */
	xfont_free(wsmap.xfont);
	wsmap.xfont = xf_new;

	if (_updatecontent_all() < 0) {
		log_set("Could not update content of all WSM boxes: %s",
		        log_str());
		return -1;
	}

	return 0;
}

static int
_action_set_size(struct list **results, struct list const *args)
{
	(void) results;

	wsmap.size = (struct size) {
		.width = ((struct value *) args->elements[0])->data.ui,
		.height = ((struct value *) args->elements[1])->data.ui,
	};

	if (_moveresize_all() < 0) {
		log_set("Could not rearrange boxes after changing box size: %s",
		        log_str());
		return -1;
	}
	return 0;
}

static int
_action_target_focus(struct list **results, struct list const *args)
{
	struct position pos;

	(void) results;
	(void) args;

	if (!wsmap.active) {
		log_set("Workspace map is not active");
		return -1;
	}

	pos = wsmap.target->position;
	if (_focusws(seat.selmon, pos) < 0) {
		log_set("Could not focus workspace (%d,%d): %s",
		        pos.x, pos.y, log_str());
		return -1;
	}

	if (_action_toggle(NULL, NULL) < 0) {
		log_set("Could not toggle workspace map: %s", log_str());
		return -1;
	}

	return 0;
}

static int
_action_target_shift(struct list **results, struct list const *args)
{
	struct wsmbox *src, *dst;
	struct position srcpos, dstpos;
	enum direction dir;
	(void) results;

	/* source workspace */
	src = NULL;
	if (wsmap.active) {
		srcpos = wsmap.target->position;
		(bool) _find_box_by_position(&src, srcpos);
	} else {
		if (!_find_box_by_workspace(&src, seat.selmon->workspace))
		{
			BUG("Could not find WSM box for currently focused workspace");
			log_set("Could not find WSM box for currently focused workspace");
			return -1;
		}
		srcpos = src->position;
	}

	/* destination workspace */
	dst = NULL;
	dir = ((struct value *) args->elements[0])->data.dir;
	direction_relative(srcpos, &dstpos, dir);
	(void) _find_box_by_position(&dst, dstpos);

	/* swap positions */
	if (dst)
		dst->position = srcpos;
	if (src)
		src->position = dstpos;
	if (wsmap.active) {
		wsmap.target->position = dstpos;
		if (_moveresize_all() < 0) {
			log_set("Could not rearrange boxes after shifting target workspace: %s",
			        log_str());
			return -1;
		}
	}
	return 0;
}

static int
_action_window_shift(struct list **results, struct list const *args)
{
	struct client *c;
	struct workspace *srcws, *dstws;
	enum direction dir;
	bool follow;
	(void) results;

	dir = ((struct value *) args->elements[0])->data.dir;
	follow = ((struct value *) args->elements[1])->data.b;

	c = seat.selmon->workspace->selcli;
	if (!c)
		return 0;

	/* TODO: implement send *without* follow */
	if (!follow) {
		log_set("action_window_send_relative(..., follow=false) not implemented");
		return -1;
	}

	srcws = seat.selmon->workspace;
	if (_workspace_step(dir) < 0) {
		log_set("Could not move focus %swards to workspace: %s",
		        direction_str(dir), log_str());
		goto error_focus_step;
	}
	dstws = seat.selmon->workspace;
	if (workspace_transfer_client(dstws, c) < 0) {
		log_set("Could not transfer client for X window %lu from workspace '%s' to workspace '%s': %s",
		        c->xwindow->id, srcws->name, dstws->name, log_str());
		goto error_transfer;
	}
	monitor_restack(seat.selmon);
	if (monitor_arrange(seat.selmon) < 0)
		ERROR("Could not rearrange clients on monitor: %s", log_str());

	return 0;

 error_transfer:
	log_push();
	if (_workspace_step(direction_opposite(dir)) < 0)
		ERROR("Could not step back in opposite direction after transfer error: %s",
		      log_str());
	log_pop();
 error_focus_step:
	return -1;
}

static int
_action_workspace_step(struct list **results, struct list const *args)
{
	enum direction dir = ((struct value *) args->elements[0])->data.dir;
	(void) results;

	return _workspace_step(dir);
}

static int
_action_target_step(struct list **results, struct list const *args)
{
	enum direction dir = ((struct value *) args->elements[0])->data.dir;
	(void) results;

	return _target_step(dir);
}


static int
_action_toggle(struct list **results, struct list const *args)
{
	struct position currentpos;
	unsigned int i;
	struct wsmbox *wb, *target;
	char const *keymap;

	(void) results;
	(void) args;

	if (!_find_box_by_workspace(&target, seat.selmon->workspace)) {
		BUG("Could not find WSM box for currently focused workspace");
		log_set("Could not find WSM box for currently focused workspace");
		return -1;
	}

	wsmap.active = !wsmap.active;

	if (wsmap.active) {
		currentpos = target->position;
		if (state_set_clientmask(false) < 0)
			ERROR("Could not unset clientmask on state: %s",
			      log_str());
		wsmap.target->position = currentpos;
		if (_target_step(DIR_NONE) < 0) {
			log_set("Could not step target to currently focused workspace at (%d,%d): %s",
			        currentpos.x, currentpos.y, log_str());
			return -1;
		}
		if (_moveresize_all() < 0)
			ERROR("Could not rearrange WSM boxes: %s", log_str());
	}

	LIST_FOR (wsmap.boxes, i, wb) {
		if (wsmbox_show(wb, wsmap.active) < 0)
			ERROR("Could not %s WSM box at (%d,%d): %s",
			      wsmap.active ? "show" : "hide",
			      wb->position.x, wb->position.y, log_str());
	}
	if (wsmbox_show(wsmap.target, wsmap.active) < 0)
		ERROR("Could not %s targeting WSM box: %s",
		      wsmap.active ? "show" : "hide", log_str());

	keymap = wsmap.active ? "wsm" : INPUT_KEYMAP_DEFAULT;
	if (input_set_active_keymap(keymap) < 0)
		ERROR("Could not set active keymap to %s", keymap);

	if (!wsmap.active)
		if (state_set_clientmask(true) < 0)
			ERROR("Could not set clientmask on state: %s",
			      log_str());

	XSync(xserver.display, False);
	return 0;
}

static int
_add_workspace(struct workspace *ws)
{
	struct wsmbox *wb;
	int wsx;

	if (_find_box_by_workspace(&wb, ws)) {
		/* not necessarily an error/bug, because there are cases where
		 * the WSM creates the workspace+box and already knows about it
		 */
		DEBUG("Workspace '%s' already managed in WSM, not adding again",
		      ws->name);
		return 0;
	}

	wb = wsmbox_alloc(ws);
	if (!wb) {
		log_set("Could not create WSM box for workspace '%s': %s",
		        ws->name, log_str());
		goto error_wbox;
	}

	/* select next free slot for workspace */
	for (wsx = 0;
	     _find_box_by_position(NULL, (struct position) { wsx, 0 });
	     ++wsx);
	wb->position = (struct position) { wsx, 0 };

	list_append(wsmap.boxes, wb);

	return 0;

 error_wbox:
	return -1;
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct wsmbox *wb;
	struct workspace *ws;

	(void) ctx;

	if (ch == bus.workspace_attach) {
		ws = data;
		if (_add_workspace(ws) < 0)
			ERROR("Could not add workspace '%s' to WSM: %s",
			      ws->name, log_str());
	} else if (ch == bus.workspace_detach) {
		ws = data;
		if (!_find_box_by_workspace(&wb, ws)) {
			WARN("Bus event '%s' for workspace '%s' not managed by WSM, ignoring",
			     ch->name, ws->name);
			return;
		}
		list_remove(wsmap.boxes, wb, NULL);
		wsmbox_free(wb);
	} else {
		BUG("WSM received enexpected bus event '%s'", ch->name);
	}
}

static bool
_find_box_by_position(struct wsmbox **wb, struct position pos)
{
	struct wsmbox *_wb;

	_wb = list_find(wsmap.boxes, &pos, (list_eqfunc) wsmbox_has_position);
	if (wb)
		*wb = _wb;
	return (bool) _wb;
}

static bool
_find_box_by_workspace(struct wsmbox **wb, struct workspace const *ws)
{
	struct wsmbox *_wb;

	_wb = list_find(wsmap.boxes, ws, (list_eqfunc) wsmbox_has_workspace);
	if (wb)
		*wb = _wb;
	return (bool) _wb;
}

static int
_focusws(struct monitor *mon, struct position pos)
{
	struct wsmbox *src, *dst;
	struct workspace *dstws;

	/* source box */
	if (!_find_box_by_workspace(&src, mon->workspace)) {
		BUG("Could not find WSM box for focused workspace '%s': %s",
		    mon->workspace->name, log_str());
		log_set("Could not find WSM box for focused workspace '%s': %s",
		        mon->workspace->name, log_str());
		return -1;
	}

	/* destination box */
	if (_find_box_by_position(&dst, pos)) {
		if (src == dst)
			return 0;
	} else {
		dstws = workspace_alloc();
		if (!dstws) {
			log_set("Could not create new workspace: %s",
			        log_str());
			goto error_wscreate;
		}
		dst = wsmbox_alloc(dstws);
		if (!dst) {
			log_set("Could not create WSM box for new workspace: %s",
			        log_str());
			goto error_wbcreate;
		}
		dst->position = pos;
		list_append(wsmap.boxes, dst);
		if (state_attach_workspace(dstws) < 0) {
			log_set("Could not attach workspace to state: %s",
			        log_str());
			goto error_wsattach;
		}
	}

	/* focus and return */
	monitor_show_workspace(mon, dst->workspace);
	return 0;

 error_wsattach:
	log_push();
	list_remove(wsmap.boxes, dst, NULL);
	log_pop();
 //error_wbattach:
	log_push();
	wsmbox_free(dst);
	log_pop();
 error_wbcreate:
	log_push();
	workspace_free(dstws);
	log_pop();
 error_wscreate:
	return -1;
}

static int
_moveresize(struct wsmbox *wb)
{
	struct monitor *mon;
	struct position mon_centre, map_pos;
	struct rectangle boxdim;
	int unsigned uw, uh;

	/* determine centre of monitor */
	mon = seat.selmon;
	mon_centre = (struct position) {
		mon->dim.x + (int signed) (mon->dim.width / 2),
		mon->dim.y + (int signed) (mon->dim.height / 2),
	};

	/* determine box dimensions */
	uw = wsmap.size.width + wsmap.border_width;
	uh = wsmap.size.height + wsmap.border_width;
	map_pos = (struct position) {
		wb->position.x - wsmap.target->position.x,
		wb->position.y - wsmap.target->position.y,
	};
	boxdim = (struct rectangle) {
		.x = mon_centre.x
		   + (map_pos.x) * (int signed) uw
		   - (int signed) (wsmap.size.width / 2 + wsmap.border_width),
		.y = mon_centre.y
		   + (map_pos.y) * (int signed) uh
		   - (int signed) (wsmap.size.height / 2 + wsmap.border_width),
		.size = wsmap.size,
	};

	return wsmbox_moveresize(wb, boxdim);
}

static int
_moveresize_all(void)
{
	int unsigned i;
	struct wsmbox *wb;

	LIST_FOR (wsmap.boxes, i, wb)
		if (_moveresize(wb) < 0)
			ERROR("Could not rearrange WSM box at (%d,%d): %s",
			      wb->position.x, wb->position.y, log_str());
	if (_moveresize(wsmap.target) < 0)
		ERROR("Could not rearrange target WSM box: %s", log_str());

	XSync(xserver.display, False);

	return 0;
}

static int
_savefile_scan(void)
{
	char const *savefile;
	FILE *fs;

	savefile = commandline_get(_CLA_SAVEFILE);
	if (!savefile) {
		DEBUG("No WSM savefile given, nothing to restore from file");
		return 0;
	}

	fs = fopen(savefile, "r");
	if (!fs) {
		log_set("Could not open savefile %s for reading: %s",
		        savefile, strerror(errno));
		goto error_fopen;
	}

	if (_savefile_scan_wsm(fs) < 0)
		goto error_restore;

	if (fclose(fs) < 0)
		WARN("Could not close savefile %s after reading: %s",
		     savefile, strerror(errno));

	return 0;

 error_restore:
	if (fclose(fs) < 0) {
		log_push();
		WARN("Could not close savefile %s after reading: %s",
		     savefile, strerror(errno));
	}
 error_fopen:
	return -1;
}

static int
_savefile_scan_workspace(FILE *fs)
{
	int sret;
	int x, y;
	char name[256];
	struct workspace *ws;
	struct wsmbox *wb;

	/* read workspace */
	errno = 0;
	sret = fscanf(fs, "%d:%d:%s\n", &x, &y, name);
	if (sret < 3) {
		if (sret == EOF) {
			if (errno == 0)
				log_set("Reached end of file while matching");
			else
				log_set("Could not read from savefile: %s",
				        strerror(errno));
		} else {
			log_set("Could not match values (%d matches instead of 3)",
			        sret);
		}
		goto error_match;
	} else if (sret > 3) {
		BUG("Matched more values than expected (%d matches instead of 3)",
		    sret);
	}

	/* find workspace in state */
	if (!state_locate_workspace(&ws, name)) {
		WARN("Workspace `%s` not found in state, not tracking in WSM",
		     name);
		return 0;
	}

	/* find pre-existing box in WSM */
	if (_find_box_by_workspace(&wb, ws)) {
		WARN("Workspace `%s` already tracked by WSM box, skipping",
		     ws->name);
		return 0;
	}

	/* create and attach workspace map box */
	wb = wsmbox_alloc(ws);
	if (!wb) {
		log_set("Could not create WSM box: %s", log_str());
		goto error_box;
	}
	list_append(wsmap.boxes, wb);
	wb->position = (struct position) { x, y };

	return 0;

 error_box:
 error_match:
	return -1;
}

static int
_savefile_scan_wsm(FILE *fs)
{
	int sret;
	size_t nwb;
	unsigned int i;

	errno = 0;
	sret = fscanf(fs, "%zu\n", &nwb);
	if (sret < 1) {
		if (sret == EOF) {
			if (errno == 0)
				log_set("Reached end of savefile while reading number of WSM boxes");
			else
				log_set("Could not read from savefile: %s",
				        strerror(errno));
		} else {
			log_set("Could not match number of workspaces (%d matches instead of 1)",
			        sret);
		}
		goto error_read;
	} else if (sret > 1) {
		BUG("Matched more than the number of WSM boxes (%d matches instead of 1)",
		    sret);
	}

	DEBUG("Scanning %zu WSM boxes...", nwb);
	for (i = 0; i < nwb; ++i) {
		if (_savefile_scan_workspace(fs) < 0) {
			log_set("Could not scan WSM box %u: %s", i, log_str());
			goto error_wsmbox;
		}
	}

	return 0;

 error_wsmbox:
 error_read:
	return -1;
}

static FILE *
_savefile_create(void)
{
	char path_template[] = "/tmp/karuiwm.wsm-XXXXXX";
	int fd;
	FILE *fs;

	fd = mkstemp(path_template);
	if (fd < 0) {
		log_set("Could not create temporary file: %s", strerror(errno));
		goto error_mkstemp;
	}
	DEBUG("Created new WSM savefile %s (fd=%d)", path_template, fd);

	_wsmap.savefile = cstr_dup(path_template);
	if (!_wsmap.savefile) {
		log_set("Could not duplicate savefile path: %s", log_str());
		goto error_pathdup;
	}

	fs = fdopen(fd, "w");
	if (!fs) {
		log_set("Could not open file stream from fd=%d: %s",
		        fd, strerror(errno));
		goto error_fdopen;
	}
	return fs;

 error_fdopen:
	log_push();
	cstr_free(_wsmap.savefile);
	_wsmap.savefile = NULL;
	log_pop();
 error_pathdup:
	close(fd);
 error_mkstemp:
	return NULL;
}

static int
_savefile_write(void)
{
	FILE *fs;
	struct wsmbox *wb;
	int unsigned i;

	fs = _savefile_create();
	if (!fs) {
		log_set("Could not create savefile: %s", log_str());
		goto error_savefile_create;
	}

	/* save number of boxes */
	if (fprintf(fs, "%zu\n", wsmap.boxes->size) < 0) {
		log_set("Could not write number of WSM boxes to savefile: %s",
		        log_str());
		goto error_write;
	}

	/* write each WXM box */
	LIST_FOR (wsmap.boxes, i, wb) {
		if (fprintf(fs, "%d:%d:%s\n",
		                wb->position.x, wb->position.y,
		                wb->workspace->name) < 0)
		{
			log_set("Could not write box information for '%s': %s",
			        wb->workspace->name, strerror(errno));
			goto error_write;
		}
	}

	fclose(fs);
	return 0;

 error_write:
	fclose(fs);
 error_savefile_create:
	return -1;
}

static int
_state_scan(void)
{
	struct workspace *ws;
	struct wsmbox *wb;
	int unsigned i;

	LIST_FOR (state.workspaces, i, ws) {
		if (_find_box_by_workspace(&wb, ws)) {
			DEBUG("Workspace '%s' already in WSM (%d,%d), not readding",
			      ws->name, wb->position.x, wb->position.y);
			continue;
		}
		if (_add_workspace(ws) < 0) {
			log_set("Could not add workspace '%s' to WSM: %s",
			        ws->name, log_str());
			goto error_add;
		}
	}

	return 0;

 error_add:
	return -1;
}

static int
_target_step(enum direction dir)
{
	struct wsmbox *src, *dst;
	struct position srcpos, dstpos;
	struct workspace *dstws;

	/* determine source and destination position */
	srcpos = wsmap.target->position;
	direction_relative(srcpos, &dstpos, dir);
	DEBUG("WSM target step (%d,%d) -> (%d,%d)",
	      srcpos.x, srcpos.y, dstpos.x, dstpos.y);

	/* find corresponding boxes */
	(void) _find_box_by_position(&src, srcpos);
	(void) _find_box_by_position(&dst, dstpos);

	/* update target workspace */
	dstws = dst ? dst->workspace : NULL;
	if (wsmbox_set_workspace(wsmap.target, dstws) < 0) {
		log_set("Could not set workspace of target box: %s", log_str());
		goto error_set_workspace;
	}

	/* move map */
	wsmap.target->position = dstpos;
	if (wsmap.active) {
		if (_moveresize_all() < 0)
			/* not much we can do at this point, just print an
			 * error
			 */
			ERROR("Could not rearrange WSM boxes after stepping target: %s",
			      log_str());
	}

	/* redraw source box (previously covered by target box) */
	if (src) {
		if (wsmbox_update_content(src) < 0)
			ERROR("Could not update content of previously covered WSM box '%s': %s",
			      src->workspace->name, log_str());
	}

	return 0;

 error_set_workspace:
	return -1;
}

static int
_updateborder_all(void)
{
	int unsigned i;
	struct wsmbox *wb;

	LIST_FOR (wsmap.boxes, i, wb)
		if (wsmbox_update_border(wb) < 0)
			ERROR("Could not update border of WSM box at (%d, %d): %s",
			      wb->position.x, wb->position.y, log_str());
	if (wsmbox_update_border(wsmap.target) < 0)
		ERROR("Could not update border of targeted WSM box: %s",
		      log_str());

	return 0;
}

static int
_updatecontent_all(void)
{
	int unsigned i;
	struct wsmbox *wb;

	LIST_FOR (wsmap.boxes, i, wb)
		if (wsmbox_update_content(wb) < 0)
			ERROR("Could not update content of WSM box at (%d,%d): %s",
			      wb->position.x, wb->position.y, log_str());
	if (wsmbox_update_content(wsmap.target) < 0)
		ERROR("Could not update content of targeted WSM box: %s",
		      log_str());

	return 0;
}

static int
_update_commandline(void)
{
	if (!_wsmap.savefile) {
		DEBUG("No savefile set");
		/* TODO: Remove wsm.savefile from commandline */
		return 0;
	}

	if (commandline_set(_CLA_SAVEFILE, _wsmap.savefile) < 0) {
		log_set("Could not set savefile argument in commandline: %s",
		        log_str());
		goto error_add;
	}

	return 0;

 error_add:
	return -1;
}

static int
_workspace_step(enum direction dir)
{
	struct wsmbox *src;
	struct position srcpos, dstpos;

	if (!_find_box_by_workspace(&src, seat.selmon->workspace)) {
		BUG("Could not find WSM box for currently focused workspace");
		log_set("Could not find WSM box for currently focused workspace");
		return -1;
	}
	srcpos = src->position;

	direction_relative(srcpos, &dstpos, dir);
	if (_focusws(seat.selmon, dstpos) < 0) {
		log_set("Could not set selected workspace to (%d, %d): %s",
		        dstpos.x, dstpos.y, log_str());
		return -1;
	}

	return 0;
}
