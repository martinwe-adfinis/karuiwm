#include <karuiwm/keymap.h>
#include <karuiwm/action_call.h>
#include <karuiwm/xserver.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

static void _grab_key(struct key const *k);

struct keymap *
keymap_alloc(char const *name, bool grab_keyboard)
{
	struct keymap *km;

	km = memory_alloc(sizeof(struct keymap), "keymap structure");
	if (!km)
		goto error_alloc;

	km->name = cstr_dup(name);
	if (!km->name) {
		log_set("Could not copy name for keymap: %s", log_str());
		goto error_name;
	}

	km->grab_keyboard = grab_keyboard;

	km->keybinds = list_alloc();
	if (!km->keybinds) {
		log_set("Could not create list for key bindings: %s",
		        log_str());
		goto error_keybinds;
	}

	km->active = false;

	return km;

 error_keybinds:
	cstr_free(km->name);
 error_name:
	log_push();
	memory_free(km);
	log_pop();
 error_alloc:
	return NULL;
}

void
keymap_free(struct keymap *km)
{
	struct keybind *kb;
	int unsigned i;

	if (km->active)
		FATAL_BUG("Attempt to destroy active keymap '%s'", km->name);

	/* First remove the keybind's reference to the keymap, as it would
	 * otherwise complain upon deletion:
	 */
	LIST_FOR (km->keybinds, i, kb)
		kb->keymap = NULL;
	list_clear(km->keybinds, (list_delfunc) keybind_free);
	list_free(km->keybinds);
	cstr_free(km->name);
	memory_free(km);
}

int
keymap_activate(struct keymap *km)
{
	int unsigned i;
	struct keybind *kb;

	if (km->active)
		FATAL_BUG("Attempt to activate already active keymap '%s'",
		          km->name);

	if (km->grab_keyboard) {
		XGrabKeyboard(xserver.display, xserver.root->id, True,
		              GrabModeAsync, GrabModeAsync, CurrentTime);
	} else {
		LIST_FOR (km->keybinds, i, kb)
			_grab_key(&kb->key);
	}
	km->active = true;
	return 0;
}

int
keymap_add_keybind(struct keymap *km, struct keybind *kb, bool overwrite)
{
	struct keybind *conflicting_kb;
	struct key const *k = &kb->key;

	conflicting_kb = list_find(km->keybinds, k,
	                           (list_eqfunc) keybind_has_key);
	if (conflicting_kb) {
		if (!overwrite) {
			log_set("Key %u+%s is already bound to '%s'",
			        conflicting_kb->key.mod,
			        XKeysymToString(conflicting_kb->key.sym),
			        conflicting_kb->actioncall->name);
			return -1;
		}
		if (keymap_unbind_key(km, &conflicting_kb->key) < 0) {
			log_set("Could not unbind existing key bind %u+%s -> '%s': %s",
			        conflicting_kb->key.mod,
			        XKeysymToString(conflicting_kb->key.sym),
			        conflicting_kb->actioncall->name,
			        log_str());
			return -1;
		}
	}

	list_append(km->keybinds, kb);
	kb->keymap = km;
	if (km->active)
		_grab_key(&kb->key);
	return 0;
}

int
keymap_deactivate(struct keymap *km)
{
	if (!km->active)
		FATAL_BUG("Attempt to deactivate inactive keymap '%s'",
		          km->name);

	if (km->grab_keyboard)
		XUngrabKeyboard(xserver.display, CurrentTime);
	else
		XUngrabKey(xserver.display, AnyKey, AnyModifier,
		           xserver.root->id);
	km->active = false;
	return 0;
}

bool
keymap_has_action(struct keymap const *km, struct action const *a)
{
	return list_contains(km->keybinds, a, (list_eqfunc) keybind_has_action);
}

bool
keymap_has_name(struct keymap const *km, char const *name)
{
	return cstr_equals(km->name, name);
}

int
keymap_resolve_key(struct keymap const *km, struct key const *k)
{
	struct keybind *kb;

	kb = list_find(km->keybinds, k, (list_eqfunc) keybind_has_key);
	if (!kb) {
		/* Not an error, because we also get events for modifiers being
		 * pressed.
		 */
		return 0;
	}
	return keybind_execute(kb);
}

int
keymap_unbind_key(struct keymap *km, struct key const *k)
{
	struct keybind *kb;

	kb = list_find(km->keybinds, k, (list_eqfunc) keybind_has_key);
	if (!kb) {
		log_set("Key %u+%s is not bound to any action in keymap '%s'",
		        k->mod, XKeysymToString(k->sym), km->name);
		return -1;
	}

	list_remove(kb->keymap->keybinds, kb, NULL);
	kb->keymap = NULL;
	keybind_free(kb);

	return 0;
}

static void
_grab_key(struct key const *k)
{
	XGrabKey(xserver.display, XKeysymToKeycode(xserver.display, k->sym),
	         k->mod, xserver.root->id, True, GrabModeAsync, GrabModeAsync);
}
