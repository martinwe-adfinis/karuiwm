#include <karuiwm/modman.h>
#include <karuiwm/commandline.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/module.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <karui/userdirs.h>
#include <unistd.h>

static int _init_paths(void);

static char const *_CLA_PATHS = "modman.paths";

static struct {
	struct list *modules;
	struct list *paths;
} _modman;

int
modman_init(void)
{
	if (_init_paths() < 0)
		goto error_paths;

	_modman.modules = list_alloc();
	if (!_modman.modules) {
		log_set("Could not create list for modules: %s", log_str());
		goto error_modules;
	}

	return 0;

 error_modules:
	log_push();
	list_free(_modman.paths);
	log_pop();
 error_paths:
	return -1;
}

void
modman_deinit(void)
{
	struct module *mod;

	while (_modman.modules->size > 0) {
		mod = _modman.modules->elements[0];
		if (module_deinit(mod) < 0)
			CRITICAL("Could not deinitialise module '%s': %s",
			         mod->data.name, log_str());
		list_remove(_modman.modules, mod, NULL);
		module_free(mod);
	}
	list_free(_modman.modules);

	list_clear(_modman.paths, (list_delfunc) cstr_free);
	list_free(_modman.paths);
}

int
modman_load(char const *name)
{
	int unsigned i;
	char const *path;
	char *filepath = NULL;
	struct module *mod;

	LIST_FOR (_modman.paths, i, path) {
		filepath = cstr_format("%s/mod_%s.so", path, name);
		if (!filepath) {
			log_set("Could not assemble file path: %s", log_str());
			goto error_filepath;
		}

		if (access(filepath, F_OK) == 0)
			break;

		DEBUG("Module file %s: No such file or directory", filepath);
		cstr_free(filepath);
		filepath = NULL;
	}
	if (!filepath) {
		log_set("Module file not found in any of the paths");
		goto error_notfound;
	}

	mod = module_alloc(filepath, name);
	if (!mod) {
		log_set("Could not create module: %s", log_str());
		goto error_create;
	}

	if (module_init(mod) < 0) {
		log_set("Could not initialise module: %s", log_str());
		goto error_init;
	}

	list_append(_modman.modules, mod);

	cstr_free(filepath);
	return 0;

 error_init:
	log_push();
	module_free(mod);
	log_pop();
 error_create:
	log_push();
	cstr_free(filepath);
	log_pop();
 error_notfound:
 error_filepath:
	return -1;
}

static int
_init_paths(void)
{
	struct list const *userdirs;
	int unsigned i;
	char const *userdir;
	char const *paths;
	char *path;

	paths = commandline_get(_CLA_PATHS);
	if (paths) {
		DEBUG("Commandline argument '%s' set to '%s'; overriding default module paths",
		      _CLA_PATHS, paths);
		_modman.paths = cstr_split(paths, ":");
		if (!_modman.paths) {
			log_set("Could not split '%s' to paths: %s",
			        paths, log_str());
			return -1;
		}
		return 0;
	} else {
		DEBUG("Commandline argument '%s' not set; falling back to default module paths",
		      _CLA_PATHS);
	}

	_modman.paths = list_alloc();
	if (!_modman.paths) {
		log_set("Could not create list for module paths: %s",
		        log_str());
		goto error_paths;
	}

	userdirs = userdirs_get_datadirs();
	LIST_FOR (userdirs, i, userdir) {
		path = cstr_format("%s/%s/modules", userdir, APPNAME);
		if (!path) {
			log_set("Could not duplicate path '%s/%s/modules': %s",
			        userdir, APPNAME, log_str());
			goto error_path;
		}

		DEBUG("› Appending %s to module paths", path);
		list_append(_modman.paths, path);
	}

	return 0;

 error_path:
	log_push();
	list_clear(_modman.paths, (list_delfunc) cstr_free);
	list_free(_modman.paths);
	log_pop();
 error_paths:
	return -1;
}
