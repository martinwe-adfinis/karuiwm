#include <karuiwm/fdio.h>
#include <karui/list.h>
#include <karui/log.h>
#include <errno.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/select.h>
#include <unistd.h>

static struct {
	struct list *clients;
	int epoll_fd;
} _fdio;

int
fdio_init(void)
{
	_fdio.clients = list_alloc();
	if (!_fdio.clients) {
		log_set("Could not create FDIO clients list: %s", log_str());
		goto error_clients;
	}

	_fdio.epoll_fd = epoll_create1(EPOLL_CLOEXEC);
	if (_fdio.epoll_fd < 0) {
		log_set("Could not create epoll file descriptor: %s",
		        strerror(errno));
		goto error_epoll;
	}

	return 0;

 error_epoll:
	log_push();
	list_free(_fdio.clients);
	log_pop();
 error_clients:
	return -1;
}

void
fdio_deinit(void)
{
	close(_fdio.epoll_fd);
	list_clear(_fdio.clients, (list_delfunc) fdio_client_free);
	list_free(_fdio.clients);
}

int
fdio_add_fd(struct fdio_client *c, int *fd)
{
	struct epoll_event epoll_ev;

	if (fdio_client_add_fd(c, fd) < 0) {
		log_set("Could not add file descriptor %d to client '%s': %s",
		        *fd, c->name, log_str());
		goto error_client;
	}

	epoll_ev.events = EPOLLIN;
	epoll_ev.data.ptr = fd;
	if (epoll_ctl(_fdio.epoll_fd, EPOLL_CTL_ADD, *fd, &epoll_ev) < 0) {
		log_set("Could not add file descriptor %d to epoll interest list: %s",
		        *fd, strerror(errno));
		goto error_epoll;
	}

	return 0;

 error_epoll:
	log_push();
	fdio_client_remove_fd(c, fd);
	log_pop();
 error_client:
	return -1;
}

struct fdio_client *
fdio_register(char const *name, int (*callback)(int, void *), void *data)
{
	struct fdio_client *c;

	c = fdio_client_alloc(name, callback, data);
	if (!c) {
		log_set("Could not create FDIO client: %s", log_str());
		goto error_client;
	}

	list_append(_fdio.clients, c);

	return c;

 error_client:
	return NULL;
}

void
fdio_remove_fd(struct fdio_client *c, int *fd)
{
	if (epoll_ctl(_fdio.epoll_fd, EPOLL_CTL_DEL, *fd, NULL) < 0) {
		log_set("Could not remove file descriptor %d from epoll interest list: %s",
		        *fd, strerror(errno));
		goto error_epoll;
	}

	fdio_client_remove_fd(c, fd);
	return;

 error_epoll:
	log_flush(LOG_ERROR);
}

void
fdio_unregister(struct fdio_client *c)
{
	list_remove(_fdio.clients, c, NULL);
	fdio_client_free(c);
}

int
fdio_wait(void)
{
	int ret, *fd;
	struct epoll_event epoll_ev;
	struct fdio_client *c;

	/* FIXME: Stopping after a single epoll_wait (even for EINTR), doing
	 * other things and then returning here results in epoll_wait somehow
	 * not detecting events on the X file descriptor, which is unexpected,
	 * given that it's level-triggered by default.
	 * Immediately reinvoking epoll_wait after an EINTR results in no
	 * blocks, but really I suspect there to be some race condition and this
	 * approach simply reducing the likelihood of that happening rather than
	 * preventing it altogether.
	 * Should investigate what is wrong with the interaction between
	 * epoll_wait and the X file descriptor.
	 */
	do {
		/* We could give epoll_wait an array of epoll_event structs, but
		 * to keep things simple, we let epoll fill it with the next
		 * event on the next call.
		 */
		ret = epoll_wait(_fdio.epoll_fd, &epoll_ev, 1, -1);
		if (ret < 0) {
			if (errno == EINTR) {
				/* FIXME */
				DEBUG("epoll_wait() returned %d < 0 with errno=%d (EINTR); retrying",
				      ret, errno);
				continue;
			}
			log_set("epoll_wait() returned %d < 0 with errno=%d (%s)",
			        ret, errno, strerror(errno));
			return -1;
		}
	} while (ret < 0 && errno == EINTR);
	if (ret > 1) {
		log_set("Obtained %d > 1 event from epoll_wait()", ret);
		return -1;
	}
	fd = epoll_ev.data.ptr;

	c = list_find(_fdio.clients, fd, (list_eqfunc) fdio_client_has_fd);
	if (!c) {
		log_set("epoll_wait() detected event on unhandled file descriptor %d",
		        *fd);
		return -1;
	}
	if (fdio_client_notify(c, fd) < 0)
		ERROR("FD I/O client '%s' failed to handle event on file descriptor %d: %s",
		      c->name, *fd, log_str());

	return 0;
}
