#include <karuiwm/bus.h>
#include <karuiwm/bus_channel.h>
#include <karuiwm/utils.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>

static int _init_channels(void);
static struct bus_channel *_init_channel(char const *name);

struct bus bus;

static struct {
	struct list *sessions;
	struct list *channels;
} _bus;

int
bus_init(void)
{
	_bus.sessions = list_alloc();
	if (!_bus.sessions) {
		log_set("Could not create sessions list: %s", log_str());
		goto error_sessions;
	}

	_bus.channels = list_alloc();
	if (!_bus.channels) {
		log_set("Could not create channel list: %s", log_str());
		goto error_channels;
	}

	if (_init_channels() < 0)
		goto error_init_channels;

	return 0;

 error_init_channels:
	log_push();
	list_clear(_bus.channels, (list_delfunc) bus_channel_free);
	list_free(_bus.channels);
	log_pop();
 error_channels:
	log_push();
	list_free(_bus.sessions);
	log_pop();
 error_sessions:
	return -1;
}

void
bus_deinit(void)
{
	struct bus_session *bs;

	list_clear(_bus.channels, (list_delfunc) bus_channel_free);
	list_free(_bus.channels);

	if (_bus.sessions->size > 0) {
		WARN("Deinitialising bus with %zu sessions",
		     _bus.sessions->size);
		while (_bus.sessions->size > 0) {
			bs = _bus.sessions->elements[0];
			bus_disconnect(bs);
		}
	}
	list_free(_bus.sessions);
}

struct bus_session *
bus_connect(char const *name,
            void (*cb)(struct bus_subscription const *, void *, void *),
            void *ctx)
{
	struct bus_session *bs;

	bs = bus_session_alloc(name, cb, ctx);
	if (!bs) {
		log_set("Could not create bus session: %s", log_str());
		goto error_alloc;
	}

	list_append(_bus.sessions, bs);
	return bs;

 error_alloc:
	return NULL;
}

void
bus_disconnect(struct bus_session *bs)
{
	list_remove(_bus.sessions, bs, NULL);
	bus_session_free(bs);
}

static struct bus_channel *
_init_channel(char const *name)
{
	struct bus_channel *ch;

	DEBUG("Initialising bus channel '%s'", name);

	ch = list_find(_bus.channels, name, (list_eqfunc) bus_channel_has_name);
	if (ch)
		return ch;

	ch = bus_channel_alloc(name);
	if (!ch) {
		log_set("Could not create new channel '%s': %s",
		        name, log_str());
		goto error_alloc;
	}

	list_append(_bus.channels, ch);

	return ch;

 error_alloc:
	return NULL;
}

static int
_init_channels(void)
{
	int unsigned i;
	struct {
		char const *name;
		struct bus_channel **channel;
	} channels[] = {
		{ "client.destroy",                   &bus.client_destroy },
		{ "client.dimension",                 &bus.client_dimension },
		{ "client.state.dialog",              &bus.client_state_dialog },
		{ "client.state.fullscreen",          &bus.client_state_fullscreen },
		{ "client.transience",                &bus.client_transience },
		{ "client.type",                      &bus.client_type },

		{ "monitor.attach",                   &bus.monitor_attach },
		{ "monitor.detach",                   &bus.monitor_detach },
		{ "monitor.resize",                   &bus.monitor_resize },
		{ "monitor.focus",                    &bus.monitor_focus },
		{ "monitor.workspace",                &bus.monitor_workspace },

		{ "workspace.attach",                 &bus.workspace_attach },
		{ "workspace.detach",                 &bus.workspace_detach },
		{ "workspace.rename",                 &bus.workspace_rename },
		{ "workspace.layout",                 &bus.workspace_layout },

		{ "x.key.notify",                     &bus.x_key_notify },
		{ "x.mouse.notify",                   &bus.x_mouse_notify },
		{ "x.pointer.notify",                 &bus.x_pointer_notify },
		{ "x.screen.attach",                  &bus.x_screen_attach },
		{ "x.screen.detach",                  &bus.x_screen_detach },
		{ "x.screen.resize",                  &bus.x_screen_resize },
		{ "x.window.notify.destroy",          &bus.x_window_notify_destroy },
		{ "x.window.notify.expose",           &bus.x_window_notify_expose },
		{ "x.window.notify.focus",            &bus.x_window_notify_focus },
		{ "x.window.notify.state.fullscreen", &bus.x_window_notify_state_fullscreen },
		{ "x.window.notify.state.modal",      &bus.x_window_notify_state_modal },
	        { "x.window.notify.transience",       &bus.x_window_notify_transience },
		{ "x.window.notify.type",             &bus.x_window_notify_type },
		{ "x.window.notify.unmap",            &bus.x_window_notify_unmap },
	};

	for (i = 0; i < LENGTH(channels); ++i) {
		*(channels[i].channel) = _init_channel(channels[i].name);
		if (!*(channels[i].channel)) {
			log_set("Could not initialise bus channel '%s': %s",
			        channels[i].name, log_str());
			goto error_init_events;
		}
	}

	return 0;

 error_init_events:
	return -1;
}
