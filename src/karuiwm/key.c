#include <karuiwm/key.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <X11/XF86keysym.h>

static KeySym _parse_xf86symbol(char const *str);

bool
key_equals(struct key const *k1, struct key const *k2)
{
	return k1->mod == k2->mod && k1->sym == k2->sym;
}

int
key_parse(struct key *k, char const *str)
{
	struct list *tokens;
	char const *modstr;
	char *symstr;
	struct key _k;
	int unsigned i, mod;

	tokens = cstr_split(str, "+");
	if (!tokens) {
		log_set("Could not split '%s' into modifiers and key: %s",
		        str, log_str());
		goto error_split;
	}
	if (tokens->size == 0) {
		log_set("Empty key definition: %s", str);
		goto error_empty;
	}

	/* key symbol */
	symstr = tokens->elements[tokens->size - 1];
	list_remove(tokens, symstr, NULL);
	if (key_parse_symbol(&_k.sym, symstr) < 0)
		goto error_symstr;

	/* key modifiers */
	_k.mod = 0x0;
	LIST_FOR (tokens, i, modstr) {
		if (key_parse_modifier(&mod, modstr) < 0)
			goto error_modstr;
		_k.mod |= mod;
	}

	if (k)
		*k = _k;

	cstr_free(symstr);
	list_clear(tokens, (list_delfunc) cstr_free);
	list_free(tokens);
	return 0;

 error_modstr:
 error_symstr:
	cstr_free(symstr);
 error_empty:
	log_push();
	list_clear(tokens, (list_delfunc) cstr_free);
	list_free(tokens);
	log_pop();
 error_split:
	return -1;
}

int
key_parse_modifier(int unsigned *mod, char const *str)
{
	int unsigned _mod;

	if (cstr_equals(str, "Shift")) {
		_mod = ShiftMask;
	} else if (cstr_equals(str, "Control")) {
		_mod = ControlMask;
	} else if (cstr_equals(str, "Lock")) {
		/* TODO do we handle this? */
		_mod = LockMask;
	} else if (cstr_equals(str, "Mod1")) {
		_mod = Mod1Mask;
	} else if (cstr_equals(str, "Mod2")) {
		_mod = Mod2Mask;
	} else if (cstr_equals(str, "Mod3")) {
		_mod = Mod3Mask;
	} else if (cstr_equals(str, "Mod4")) {
		_mod = Mod4Mask;
	} else if (cstr_equals(str, "Mod5")) {
		_mod = Mod5Mask;
	} else {
		log_set("Unknown modifier: %s", str);
		return -1;
	}
	if (mod)
		*mod = _mod;
	return 0;
}

int
key_parse_symbol(KeySym *sym, char const *str)
{
	KeySym _sym;

	_sym = XStringToKeysym(str);
	if (_sym == NoSymbol)
		_sym = _parse_xf86symbol(str);
	if (_sym == NoSymbol) {
		log_set("Unknown key symbol: %s", str);
		return -1;
	}
	if (sym)
		*sym = _sym;
	return 0;
}

static KeySym
_parse_xf86symbol(char const *str)
{
	return cstr_equals(str, "AudioLowerVolume") ? XF86XK_AudioLowerVolume
	     : cstr_equals(str, "AudioRaiseVolume") ? XF86XK_AudioRaiseVolume
	     : cstr_equals(str, "AudioMute")        ? XF86XK_AudioMute
	     : cstr_equals(str, "AudioMicMute")     ? XF86XK_AudioMicMute
	     : cstr_equals(str, "AudioPlay")        ? XF86XK_AudioPlay
	     : cstr_equals(str, "AudioPause")       ? XF86XK_AudioPause
	     : cstr_equals(str, "AudioStop")        ? XF86XK_AudioStop
	     : cstr_equals(str, "AudioPrev")        ? XF86XK_AudioPrev
	     : cstr_equals(str, "AudioNext")        ? XF86XK_AudioNext
	     : NoSymbol;
}
