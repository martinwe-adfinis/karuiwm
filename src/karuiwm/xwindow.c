#include <karuiwm/xwindow.h>
#include <karuiwm/bus.h>
#include <karuiwm/utils.h>
#include <karuiwm/xserver.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <limits.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

enum _net_wm_state_action {
	_NET_WM_STATE_REMOVE = 0,
	_NET_WM_STATE_ADD = 1,
	_NET_WM_STATE_TOGGLE = 2
};

static int _check_size(struct size const *size);
static int _handle_clientmessage_wmstate(struct xwindow *xwin,
                                         XClientMessageEvent *xcmev);
static int _query_geometry(struct xwindow *xwin);
static int _query_icon_name(struct xwindow *xwin);
static int _query_name(struct xwindow *xwin);
static int _query_property(struct xwindow const *xwin, Atom property,
                           size_t *nvalues, Atom **values);
static int _query_sizehints(struct xwindow *xwin);
static int _query_text_property(struct xwindow const *xwin, char **value,
                                size_t nproperties, Atom const *properties);
static int _set_fullscreen(struct xwindow *xwin, bool fullscreen);
static int _set_modal(struct xwindow *xwin, bool modal);
static int _set_property(struct xwindow const *xwin, Atom property,
                         size_t nvalues, Atom const *values);
static int _set_state(struct xwindow const *xwin);
static enum xwindow_type _type_from_atom(Atom type);

struct xwindow *
xwindow_alloc(Window id, struct rectangle const *dim,
              int long unsigned event_mask, bool override_redirect,
              enum xwindow_class class)
{
	struct xwindow *xwin;

	xwin = memory_alloc(sizeof(struct xwindow), "X window struct");
	if (!xwin)
		goto error_alloc;
	xwin->id = id;
	xwin->override_redirect = override_redirect;

	if (dim) {
		if (_check_size(&dim->size) < 0) {
			BUG("xwindow_alloc(%lu): Invalid size %ux%u: %s",
			    id, dim->width, dim->height, log_str());
			goto error_dimension;
		}
		xwin->dimension = *dim;
	} else {
		if (_query_geometry(xwin) < 0) {
			log_set("Could not query geometry: %s", log_str());
			goto error_dimension;
		}
	}

	xwin->client = NULL;
	xwin->destroyed = false;
	xwin->class = class;
	if (xwindow_change_event_mask(xwin, event_mask,
	                              XWINDOW_EVENT_MASK_ACTION_REPLACE) < 0)
	{
		log_set("Could not change event mask to 0x%16lX: %s",
		        event_mask, log_str());
		goto error_event_mask;
	}
	xwin->event_mask = event_mask;

	xwin->name = NULL;
	xwin->icon_name = NULL;

	xwin->transients = list_alloc();
	if (!xwin->transients) {
		log_set("Could not create list for transient windows: %s",
		        log_str());
		goto error_transients;
	}

	xwin->transient_for = NULL;
	if (xwindow_query_transience(xwin) < 0) {
		log_set("Could not query transience relation: %s",
		        log_str());
		goto error_query_transience;
	}

	xwin->type = XWINDOW_TYPE_NORMAL;
	xwin->state.modal = false;
	xwin->state.fullscreen = false;

	xwin->sizehints.size = (struct size) { 0, 0 };
	xwin->sizehints.has_position = false;
	xwin->sizehints.base_width = xwin->sizehints.base_height = 0;
	xwin->sizehints.inc_width = xwin->sizehints.inc_height = 0;
	xwin->sizehints.min_width = xwin->sizehints.min_height = 0;
	xwin->sizehints.max_width = xwin->sizehints.max_height = 0;

	return xwin;

 error_query_transience:
	log_push();
	list_free(xwin->transients);
	log_pop();
 error_event_mask:
 error_transients:
 error_dimension:
	log_push();
	memory_free(xwin);
	log_pop();
 error_alloc:
	return NULL;
}

void
xwindow_free(struct xwindow *xwin)
{
	list_free(xwin->transients);
	if (xwin->name)
		cstr_free(xwin->name);
	if (xwin->icon_name)
		cstr_free(xwin->icon_name);
	memory_free(xwin);
}

int
xwindow_apply_sizehints(struct xwindow const *xwin, struct size *size)
{
	int unsigned *w = &size->width, *h = &size->height, u;
	int unsigned wmin, hmin;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	wmin = MAX(xwin->sizehints.base_width, xwin->sizehints.min_width);
	hmin = MAX(xwin->sizehints.base_height, xwin->sizehints.min_height);

	/* base+increment size */
	if (xwin->sizehints.inc_width > 0) {
		u = wmin > *w ? 0 : (*w - wmin) / xwin->sizehints.inc_width;
		*w = wmin + MAX(1, u) * xwin->sizehints.inc_width;
	}
	if (xwin->sizehints.inc_height > 0) {
		u = hmin > *h ? 0 : (*h - hmin) / xwin->sizehints.inc_height;
		*h = hmin + MAX(1, u) * xwin->sizehints.inc_height;
	}

	/* minimum size */
	if (wmin > 0)
		*w = MAX(*w, wmin);
	if (hmin > 0)
		*h = MAX(*h, hmin);

	/* maximum size */
	if (xwin->sizehints.max_width > 0)
		*w = MIN(*w, xwin->sizehints.max_width);
	if (xwin->sizehints.max_height > 0)
		*h = MIN(*h, xwin->sizehints.max_height);

	return 0;
}

int
xwindow_change_event_mask(struct xwindow *xwin, int long unsigned mask,
                          enum xwindow_event_mask_action ema)
{
	XSetWindowAttributes xswa;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	switch (ema) {
	case XWINDOW_EVENT_MASK_ACTION_ADD:
		xswa.event_mask = (int long signed) (xwin->event_mask | mask);
		break;
	case XWINDOW_EVENT_MASK_ACTION_REMOVE:
		xswa.event_mask = (int long signed) (xwin->event_mask & ~mask);
		break;
	case XWINDOW_EVENT_MASK_ACTION_REPLACE:
		xswa.event_mask = (int long signed) mask;
		break;
	}

	if (!XChangeWindowAttributes(xserver.display, xwin->id, CWEventMask,
	                             &xswa))
	{
		log_set("XChangeWindowAttributes() failed");
		goto error_setwa;
	}
	xwin->event_mask = (int long unsigned) xswa.event_mask;

	return 0;

 error_setwa:
	return -1;
}

int
xwindow_handle_clientmessage(struct xwindow *xwin, XClientMessageEvent *xcmev)
{
	char *message_name;

	message_name = XGetAtomName(xserver.display, xcmev->message_type);
	if (!message_name) {
		log_set("Could not obtain message type atom name for %lu",
		        xcmev->message_type);
		goto error_message_name;
	}

	/* match message type */
	if (xcmev->message_type == xserver.atoms[XATOM_NET_WM_STATE]) {
		DEBUG("X window %lu received client message %lu (%s)",
		      xwin->id, xcmev->message_type, message_name);
		if (_handle_clientmessage_wmstate(xwin, xcmev) < 0)
			log_set("Could not handle client message %s: %s",
			        message_name, log_str());
	} else {
		WARN("X window %lu received unknown client message %lu (%s)",
		     xwin->id, xcmev->message_type, message_name);
	}

	(void) XFree(message_name);
	return 0;

 error_message_name:
	return -1;
}

int
xwindow_handle_propertynotify(struct xwindow *xwin, XPropertyEvent *xpev)
{
	char *propname;

	propname = XGetAtomName(xserver.display, xpev->atom);
	if (!propname) {
		log_set("Could not get property atom name for %lu", xpev->atom);
		goto error_propname;
	}

	/* obsolete properties */
	if (xpev->atom == xserver.atoms[XATOM_WM_CLIENT_MACHINE]
	|| xpev->atom == xserver.atoms[XATOM_WM_COMMAND])
	{
		DEBUG("X window %lu updated obsolete property %lu (%s); ignoring",
		      xwin->id, xpev->atom, propname);
		goto out;
	}

	/* ignored properties */
	if (xpev->atom == xserver.atoms[XATOM_NET_WM_BYPASS_COMPOSITOR]
	|| xpev->atom == xserver.atoms[XATOM_NET_WM_OPAQUE_REGION])
	{
		DEBUG("X window %lu updated property %lu (%s); ignoring",
		      xwin->id, xpev->atom, propname);
		goto out;
	}
	if (xpev->atom == xserver.atoms[XATOM_WM_LOCALE_NAME]
	|| xpev->atom == xserver.atoms[XATOM_NET_WM_USER_TIME])
	{
		/* ignored silently, to avoid log spam (very frequent) */
		goto out;
	}
	if (xpev->atom == xserver.atoms[XATOM_NET_WM_STATE]) {
		/* no need to react, this is just an "FYI" (otherwise we would
		 * get a client message)
		 */
		goto out;
	}

	/* unimplemented properties */
	if (xpev->atom == xserver.atoms[XATOM_WM_HINTS]
	|| xpev->atom == xserver.atoms[XATOM_WM_CLASS]
	|| xpev->atom == xserver.atoms[XATOM_WM_PROTOCOLS]
	|| xpev->atom == xserver.atoms[XATOM_WM_STATE]
	|| xpev->atom == xserver.atoms[XATOM_WM_ICON_SIZE]
	|| xpev->atom == xserver.atoms[XATOM_NET_WM_STRUT]
	|| xpev->atom == xserver.atoms[XATOM_NET_WM_STRUT_PARTIAL])
	{
		/* TODO: implement these */
		WARN("X window %lu updated unimplemented property %lu (%s); ignoring",
		     xwin->id, xpev->atom, propname);
		goto out;
	}

	/* matched properties */
	if (xpev->atom != xserver.atoms[XATOM_WM_NORMAL_HINTS])
		DEBUG("X window %lu updated property %lu (%s); handling",
		      xwin->id, xpev->atom, propname);
	if (xpev->atom == xserver.atoms[XATOM_WM_ICON_NAME]) {
		if (_query_icon_name(xwin) < 0) {
			log_set("Could not query icon name (ICCCM): %s",
			        log_str());
			goto error_handle;
		}
	} else if (xpev->atom == xserver.atoms[XATOM_WM_NAME]) {
		if (_query_name(xwin) < 0) {
			log_set("Could not query name (ICCCM): %s", log_str());
			goto error_handle;
		}
	} else if (xpev->atom == xserver.atoms[XATOM_WM_NORMAL_HINTS]) {
		if (_query_sizehints(xwin) < 0) {
			log_set("Could not query size hints: %s", log_str());
			goto error_handle;
		}
		/* TODO: apply size hints? */
	} else if (xpev->atom == xserver.atoms[XATOM_WM_TRANSIENT_FOR]) {
		if (xwindow_query_transience(xwin) < 0) {
			log_set("Could not query transience relation: %s",
			        log_str());
			goto error_handle;
		}
	} else if (xpev->atom == xserver.atoms[XATOM_NET_WM_ICON_NAME]) {
		if (_query_icon_name(xwin) < 0) {
			log_set("Could not query icon name (EWMH): %s",
			        log_str());
			goto error_handle;
		}
	} else if (xpev->atom == xserver.atoms[XATOM_NET_WM_NAME]) {
		if (_query_name(xwin) < 0) {
			log_set("Could not query name (EWMH): %s", log_str());
			goto error_handle;
		}
	} else if (xpev->atom == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE]) {
		if (xwindow_query_type(xwin) < 0) {
			log_set("Could not query type: %s", log_str());
			goto error_handle;
		}
	} else {
		WARN("X window %lu updated unexpected property %lu (%s); ignoring",
		     xwin->id, xpev->atom, propname);
		goto out;
	}

 out:
	(void) XFree(propname);
	return 0;

 error_handle:
	(void) XFree(propname);
 error_propname:
	return -1;
}

bool
xwindow_has_id(struct xwindow const *xwin, Window const *id)
{
	return xwin->id == *id;
}

bool
xwindow_is_manageable(struct xwindow const *xwin)
{
	if (xwin->class == XWINDOW_CLASS_ROOT) {
		log_set("X window is root");
		return false;
	}

	if (xwin->override_redirect) {
		log_set("X window has attribute override_redirect");
		return false;
	}

	return true;
}

bool
xwindow_is_viewable(struct xwindow const *xwin)
{
	XWindowAttributes xwa;

	if (!XGetWindowAttributes(xserver.display, xwin->id, &xwa)) {
		log_set("Could not get X window attributes");
		return false;
	}

	return xwa.map_state == IsViewable;
}

int
xwindow_map(struct xwindow *xwin)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (!XMapWindow(xserver.display, xwin->id)) {
		log_set("XMapWindow() failed");
		return -1;
	}
	return 0;
}

int
xwindow_move(struct xwindow *xwin, struct position pos)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (!XMoveWindow(xserver.display, xwin->id, pos.x, pos.y)) {
		log_set("XMoveWindow() failed");
		return -1;
	}
	xwin->dimension.pos = pos;

	return 0;
}

int
xwindow_moveresize(struct xwindow *xwin, struct rectangle dim, bool apply_hints)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (_check_size(&dim.size) < 0) {
		BUG("xwindow_moveresize(%lu): Invalid size %ux%u: %s",
		    xwin->id, dim.width, dim.height, log_str());
		return -1;
	}

	if (apply_hints && xwindow_apply_sizehints(xwin, &dim.size) < 0) {
		log_set("Could not apply size hints: %s", log_str());
		return -1;
	}

	if (!XMoveResizeWindow(xserver.display, xwin->id,
	                       dim.x, dim.y, dim.width, dim.height))
	{
	        log_set("XMoveResizeWindow() failed");
	        return -1;
	}
	xwin->dimension = dim;

	return 0;
}

int
xwindow_query_state(struct xwindow *xwin)
{
	Atom property, value, *values;
	size_t i, nvalues;
	char *valuename;
	bool ignoring, unexpected;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	property = xserver.atoms[XATOM_NET_WM_STATE];
	if (_query_property(xwin, property, &nvalues, &values) < 0) {
		log_set("Could not query _NET_WM_STATE property for X window %lu: %s",
		        xwin->id, log_str());
		return -1;
	}

	xwin->state.fullscreen = false;
	xwin->state.modal = false;
	for (i = 0; i < nvalues; ++i) {
		value = values[i];
		if (value == 0)
			continue;

		ignoring = unexpected = false;

		/* react to properties */
		if (value == xserver.atoms[XATOM_NET_WM_STATE_FULLSCREEN]) {
			xwin->state.fullscreen = true;
		} else if (value == xserver.atoms[XATOM_NET_WM_STATE_MODAL]) {
			xwin->state.modal = true;
		} else if (value == xserver.atoms[XATOM_NET_WM_STATE_ABOVE]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_BELOW]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_DEMANDS_ATTENTION]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_HIDDEN]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_MAXIMIZED_HORZ]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_MAXIMIZED_VERT]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_SHADED]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_SKIP_PAGER]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_SKIP_TASKBAR]
		        || value == xserver.atoms[XATOM_NET_WM_STATE_STICKY]
		) {
			ignoring = true;
		} else {
			unexpected = true;
		}

		/* print informational message */
		valuename = XGetAtomName(xserver.display, value);
		if (unexpected)
			BUG("X window %lu has unexpected _NET_WM_STATE[%zu/%zu] %s (%lu), ignoring",
			    xwin->id, i + 1, nvalues, valuename, value);
		else
			DEBUG("X window %lu has _NET_WM_STATE[%zu/%zu] %s (%lu)%s",
			      xwin->id, i + 1, nvalues, valuename, value,
			      ignoring ? ", ignoring" : "");
		XFree(valuename);
	}

	XFree(values);
	return 0;
}

int
xwindow_query_transience(struct xwindow *xwin)
{
	Window tr4id;
	struct xwindow *tr4xwin;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	/* query transience relation from X server */
	if (!XGetTransientForHint(xserver.display, xwin->id, &tr4id)) {
		if (xwin->transient_for) {
			WARN("Removing defunct transience relation of X window %lu for %lu",
			     xwin->id, xwin->transient_for->id);
			list_remove(xwin->transient_for->transients, xwin,
			            NULL);
			xwin->transient_for = NULL;
		}
		return 0;
	}

	/* locate transient-for window on our side */
	tr4xwin = xserver_find_xwindow(tr4id);
	if (!tr4xwin) {
		WARN("X window %lu not known", tr4id);
		return 0;
	}

	/* clean up any pre-existing transience relations */
	if (xwin->transient_for) {
		if (tr4xwin != xwin->transient_for) {
			WARN("Changing defunct transience relation of X window %lu for %lu (new: %lu)",
			     xwin->id, xwin->transient_for->id, tr4xwin->id);
			list_remove(xwin->transient_for->transients, xwin,
			            NULL);
			xwin->transient_for = NULL;
		}
	}

	/* set up transience relation, then fire event */
	if (!xwin->transient_for) {
		DEBUG("Adding new transience relation of X window %lu for %lu",
		      xwin->id, tr4xwin->id);
		list_append(tr4xwin->transients, xwin);
		xwin->transient_for = tr4xwin;
	}
	bus_channel_send(bus.x_window_notify_transience, xwin);

	return 0;
}

int
xwindow_query_type(struct xwindow *xwin)
{
	Atom prop, *values;
	size_t i, nvalues;
	char *valuename;
	enum xwindow_type newtype;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	/* query property */
	prop = xserver.atoms[XATOM_NET_WM_WINDOW_TYPE];
	if (_query_property(xwin, prop, &nvalues, &values) < 0) {
		DEBUG("Could not query _NET_WM_WINDOW_TYPE atom for X window %lu; falling back to XWINDOW_TYPE_NORMAL: %s",
		      xwin->id, log_str());
		xwin->type = XWINDOW_TYPE_NORMAL;
		return 0;
	}


	/* find first known property atom */
	for (i = 0, newtype = XWINDOW_TYPE_UNKNOWN;
	     i < nvalues && newtype == XWINDOW_TYPE_UNKNOWN;
	     ++i)
	{
		valuename = XGetAtomName(xserver.display, values[i]);
		if (!valuename) {
			ERROR("X window %lu: Could not get string representation for _NET_WM_WINDOW_TYPE[%zu/%zu] %lu; ignoring",
			      xwin->id, i + 1, nvalues, values[i]);
			continue;
		}
		newtype = _type_from_atom(values[i]);
		DEBUG("X window %lu has _NET_WM_WINDOW_TYPE[%zu/%zu] %s (%lu) › %s%s",
		      xwin->id, i + 1, nvalues, valuename, values[i],
		      xwindow_type_str(newtype),
		      newtype == XWINDOW_TYPE_UNKNOWN ? " (ignoring)" : "");
		XFree(valuename);
	}
	XFree(values);
	if (newtype == XWINDOW_TYPE_UNKNOWN) {
		DEBUG("X window %lu has %zu window types, all unknown; falling back to XWINDOW_TYPE_NORMAL",
		      xwin->id, nvalues);
		newtype = XWINDOW_TYPE_NORMAL;
	}

	/* assign to window, then send event */
	if (newtype != xwin->type) {
		DEBUG("X window %lu type changed from %s to %s", xwin->id,
		      xwindow_type_str(xwin->type), xwindow_type_str(newtype));
		xwin->type = newtype;
		bus_channel_send(bus.x_window_notify_type, xwin);
	}

	return 0;
}

int
xwindow_resize(struct xwindow *xwin, struct size size, bool apply_hints)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (_check_size(&size) < 0) {
		BUG("xwindow_resize(%lu): Invalid size %ux%u: %s",
		    xwin->id, size.width, size.height, log_str());
		return -1;
	}

	if (apply_hints && xwindow_apply_sizehints(xwin, &size) < 0) {
		log_set("Could not apply size hints: %s", log_str());
		return -1;
	}

	if (!XResizeWindow(xserver.display, xwin->id, size.width, size.height))
	{
		log_set("XResizeWindow() failed");
		return -1;
	}
	xwin->dimension.size = size;

	return 0;
}

int
xwindow_set_border_colour(struct xwindow *xwin, uint32_t col)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	xwin->border.colour = col;
	if (!XSetWindowBorder(xserver.display, xwin->id, xwin->border.colour)) {
		log_set("XSetWindowBorder() failed");
		goto error_set;
	}

	return 0;

 error_set:
	return -1;
}

int
xwindow_set_border_width(struct xwindow *xwin, int unsigned width)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	xwin->border.width = width;
	if (!XSetWindowBorderWidth(xserver.display, xwin->id,
	                           xwin->border.width))
	{
		log_set("XSetWindowBorderWidth() failed");
		goto error_set;
	}

	return 0;

 error_set:
	return -1;
}

int
xwindow_set_focus(struct xwindow *xwin, bool focus)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (focus)
		XSetInputFocus(xserver.display, xwin->id,
		               RevertToNone, CurrentTime);

	return 0;
}

int
xwindow_set_fullscreen(struct xwindow *xwin, bool fullscreen)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (fullscreen) {
		if (!XChangeProperty(xserver.display, xwin->id,
		                     xserver.atoms[XATOM_NET_WM_STATE],
		                     XA_ATOM, 32,
		                     PropModeReplace,
		                     (char unsigned *) &xserver.atoms[XATOM_NET_WM_STATE_FULLSCREEN],
		                     1))
		{
			log_set("Could not set _NET_WM_STATE property to [_NET_WM_STATE_FULLSCREEN]: %s",
			        log_str());
			return -1;
		}
	} else {
		/* FIXME: should retain other states */
		if (!XChangeProperty(xserver.display, xwin->id,
		                     xserver.atoms[XATOM_NET_WM_STATE],
		                     XA_ATOM, 32,
		                     PropModeReplace,
		                     (char unsigned *) NULL,
		                     0))
		{
			log_set("Could not set _NET_WM_STATE property to []: %s",
			        log_str());
			return -1;
		}
	}

	return 0;
}

char const *
xwindow_type_str(enum xwindow_type type)
{
	switch (type) {
	case XWINDOW_TYPE_DESKTOP: return "XWINDOW_TYPE_DESKTOP";
	case XWINDOW_TYPE_DOCK: return "XWINDOW_TYPE_DOCK";
	case XWINDOW_TYPE_TOOLBAR: return "XWINDOW_TYPE_TOOLBAR";
	case XWINDOW_TYPE_MENU: return "XWINDOW_TYPE_MENU";
	case XWINDOW_TYPE_UTILITY: return "XWINDOW_TYPE_UTILITY";
	case XWINDOW_TYPE_SPLASH: return "XWINDOW_TYPE_SPLASH";
	case XWINDOW_TYPE_DIALOG: return "XWINDOW_TYPE_DIALOG";
	case XWINDOW_TYPE_DROPDOWN_MENU: return "XWINDOW_TYPE_DROPDOWN_MENU";
	case XWINDOW_TYPE_POPUP_MENU: return "XWINDOW_TYPE_POPUP_MENU";
	case XWINDOW_TYPE_TOOLTIP: return "XWINDOW_TYPE_TOOLTIP";
	case XWINDOW_TYPE_NOTIFICATION: return "XWINDOW_TYPE_NOTIFICATION";
	case XWINDOW_TYPE_COMBO: return "XWINDOW_TYPE_COMBO";
	case XWINDOW_TYPE_DND: return "XWINDOW_TYPE_DND";
	case XWINDOW_TYPE_NORMAL: return "XWINDOW_TYPE_NORMAL";
	case XWINDOW_TYPE_UNKNOWN: return "XWINDOW_TYPE_UNKNOWN";
	}
	FATAL_BUG("Unknown X window type %d", type);
}

int
xwindow_unmap(struct xwindow *xwin)
{
	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (!XUnmapWindow(xserver.display, xwin->id)) {
		log_set("XUnmapWindow(%lu) failed", xwin->id);
		return -1;
	}

	return 0;
}

static int
_check_size(struct size const *size)
{
	int unsigned const WINDOW_SIZE_MAX = 8000;

	/* TODO: Replace with asserts as soon as we've tracked down the issue */
	if (size->width > WINDOW_SIZE_MAX) {
		BUG("Width = %u > %u", size->width, WINDOW_SIZE_MAX);
		return -1;
	}
	if (size->height > WINDOW_SIZE_MAX) {
		BUG("Height = %u > %u", size->height, WINDOW_SIZE_MAX);
		return -1;
	}
	if (size->width < 1) {
		BUG("Width = %u < 1", size->width);
		return -1;
	}
	if (size->height < 1) {
		BUG("Height = %u < 1", size->width);
		return -1;
	}

	return 0;
}

static int
_handle_clientmessage_wmstate(struct xwindow *xwin, XClientMessageEvent *xcmev)
{
	/* https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html#idm45805407959456 */

	enum _net_wm_state_action action;
	Atom property1, property2;
	char *atomname;
	char const *actionverb;
	bool isset;

	if (xcmev->format != 32) {
		log_set("_NET_WM_STATE message has invalid format %d != 32",
		        xcmev->format);
		return -1;
	}

	action = xcmev->data.l[0];
	switch (action) {
	case _NET_WM_STATE_ADD:
		actionverb = "Adding";
		break;
	case _NET_WM_STATE_REMOVE:
		actionverb = "Removing";
		break;
	case _NET_WM_STATE_TOGGLE:
		actionverb = "Toggling";
		break;
	default:
		log_set("_NET_WM_STATE message has unknown action %d", action);
		return -1;
	}

	property1 = (Atom) xcmev->data.l[1];
	atomname = XGetAtomName(xserver.display, property1);
	DEBUG("%s _NET_WM_STATE property %s", actionverb, atomname);
	XFree(atomname);

	property2 = (Atom) xcmev->data.l[2];
	if (property2 != (Atom) 0) {
		atomname = XGetAtomName(xserver.display, property2);
		DEBUG("%s _NET_WM_STATE property %s", actionverb, atomname);
		XFree(atomname);
	}

	/* fullscreen */
	if (property1 == xserver.atoms[XATOM_NET_WM_STATE_FULLSCREEN]
	|| property2 == xserver.atoms[XATOM_NET_WM_STATE_FULLSCREEN])
	{
		isset = action == _NET_WM_STATE_ADD
		               || (action == _NET_WM_STATE_TOGGLE
		                   && !xwin->state.fullscreen);
		if (_set_fullscreen(xwin, isset) < 0) {
			log_set("Could not update fullscreen property to %s: %s",
			        isset ? "true" : "false", log_str());
			goto error_set_prop;
		}
	}

	/* modal */
	if (property1 == xserver.atoms[XATOM_NET_WM_STATE_MODAL]
	|| property2 == xserver.atoms[XATOM_NET_WM_STATE_MODAL])
	{
		isset = action == _NET_WM_STATE_ADD
		               || (action == _NET_WM_STATE_TOGGLE
		                   && !xwin->state.modal);
		if (_set_modal(xwin, isset) < 0) {
			log_set("Could not update modal property to %s: %s",
			        isset ? "true" : "false", log_str());
			goto error_set_prop;
		}
	}

	return 0;

 error_set_prop:
	return -1;
}

static int
_query_geometry(struct xwindow *xwin)
{
	struct rectangle dim;
	Window wdummy;
	int unsigned udummy;

	if (!XGetGeometry(xserver.display, xwin->id, &wdummy,
	                  &dim.x, &dim.y, &dim.width, &dim.height,
	                  &udummy, &udummy))
	{
		log_set("XGetGeometry() failed");
		return -1;
	}

	if (_check_size(&dim.size) < 0) {
		log_set("XGetGeometry() returned invalid size %ux%u: %s",
		        dim.width, dim.height, log_str());
		return -1;
	}

	xwin->dimension = dim;
	return 0;
}

static int
_query_icon_name(struct xwindow *xwin)
{
	Atom properties[] = {
		xserver.atoms[XATOM_NET_WM_ICON_NAME],
		xserver.atoms[XATOM_WM_ICON_NAME],
	};

	return _query_text_property(xwin, &xwin->name,
	                            LENGTH(properties), properties);
}

static int
_query_name(struct xwindow *xwin)
{
	Atom properties[] = {
		xserver.atoms[XATOM_NET_WM_NAME],
		xserver.atoms[XATOM_WM_NAME],
	};

	return _query_text_property(xwin, &xwin->name,
	                            LENGTH(properties), properties);
}

static int
_query_property(struct xwindow const *xwin, Atom property,
                size_t *nvalues, Atom **values)
{
	int ret;
	int long off32, len32;
	Bool deleted;
	Atom type, type_ret;
	int format_ret;
	int long unsigned nitems_ret, nbytes_ret;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	off32 = 0l;
	len32 = LONG_MAX;
	deleted = False;
	type = XA_ATOM;

	ret = XGetWindowProperty(xserver.display, xwin->id, property,
	                         off32, len32, deleted, type,
	                         &type_ret, &format_ret,
	                         &nitems_ret, &nbytes_ret,
	                         (char unsigned **) values);
	if (ret != Success) {
		log_set("XGetWindowProperty() failed");
		return -1;
	}

	if (!*values) {
		*nvalues = 0;
		return 0;
	}

	if (type_ret != type) {
		ERROR("Requested XA_ATOM (%lu), but obtained %lu",
		      type, type_ret);
		XFree(*values);
		return -1;
	}

	*nvalues = (size_t) nitems_ret;
	return 0;
}

static int
_query_sizehints(struct xwindow *xwin)
{
	long size;
	XSizeHints hints;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	if (!XGetWMNormalHints(xserver.display, xwin->id, &hints, &size)) {
		DEBUG("XGetWMNormalHints(%lu) failed, unsetting all size hints",
		      xwin->id);
		hints.flags = 0x0;
	}

	/* client-decided dimensions */
	if (hints.flags & PPosition) {
		xwin->sizehints.has_position = true;
		xwin->sizehints.position = (struct position) { hints.x, hints.y };
	} else {
		xwin->sizehints.has_position = false;
	}

	if (hints.flags & PSize) {
		xwin->sizehints.size.width = (int unsigned) hints.width;
		xwin->sizehints.size.height = (int unsigned) hints.height;
	} else {
		xwin->sizehints.size = (struct size) { 0, 0 };
	}

	/* base size */
	if (hints.flags & PBaseSize) {
		xwin->sizehints.base_width = (int unsigned) hints.base_width;
		xwin->sizehints.base_height = (int unsigned) hints.base_height;
	} else if (hints.flags & PMinSize) {
		xwin->sizehints.base_width = (int unsigned) hints.min_width;
		xwin->sizehints.base_height = (int unsigned) hints.min_height;
	} else {
		xwin->sizehints.base_width = xwin->sizehints.base_height = 0;
	}

	/* resize steps */
	if (hints.flags & PResizeInc) {
		xwin->sizehints.inc_width = (int unsigned) hints.width_inc;
		xwin->sizehints.inc_height = (int unsigned) hints.height_inc;
	} else {
		xwin->sizehints.inc_width = xwin->sizehints.inc_height = 0;
	}

	/* minimum size */
	if (hints.flags & PMinSize) {
		xwin->sizehints.min_width = (int unsigned) hints.min_width;
		xwin->sizehints.min_height = (int unsigned) hints.min_height;
	} else if (hints.flags & PBaseSize) {
		xwin->sizehints.min_width = (int unsigned) hints.base_width;
		xwin->sizehints.min_height = (int unsigned) hints.base_height;
	} else {
		xwin->sizehints.min_width = xwin->sizehints.min_height = 0;
	}

	/* maximum size */
	if (hints.flags & PMaxSize) {
		xwin->sizehints.max_width = (int unsigned) hints.max_width;
		xwin->sizehints.max_height = (int unsigned) hints.max_height;
	} else {
		xwin->sizehints.max_width = xwin->sizehints.max_height = 0;
	}

	/* aspect ratio */
	if (hints.flags & PAspect) {
		xwin->sizehints.min_aspect.width = (int unsigned) hints.min_aspect.x;
		xwin->sizehints.min_aspect.height = (int unsigned) hints.min_aspect.y;
		xwin->sizehints.max_aspect.width = (int unsigned) hints.max_aspect.x;
		xwin->sizehints.max_aspect.height = (int unsigned) hints.max_aspect.y;
	} else {
		xwin->sizehints.min_aspect = (struct size) { 0, 0 };
		xwin->sizehints.max_aspect = (struct size) { 0, 0 };
	}

	return 0;
}

static int
_query_text_property(struct xwindow const *xwin, char **value,
                     size_t nproperties, Atom const *properties)
{
	XTextProperty xtp;
	int unsigned i;
	Atom prop;
	char *propname, *newval, **list;
	bool found;
	int xtpres, nxtp;

	if (xwin->destroyed) {
		log_set("X window is destroyed");
		return -1;
	}

	for (i = 0, found = false; !found && i < nproperties; ++i) {
		prop = properties[i];
		propname = XGetAtomName(xserver.display, prop);
		if (XGetTextProperty(xserver.display, xwin->id, &xtp, prop)) {
			DEBUG("X window %lu: Property %s found; using",
			      xwin->id, propname);
			found = true;
		} else {
			DEBUG("X window %lu: Property %s not found; continuing",
			      xwin->id, propname);
			XFree(propname);
		}
	}

	if (!found) {
		DEBUG("X window %lu: None of the properties found; clearing value",
		      xwin->id);
		if (*value) {
			cstr_free(*value);
			*value = NULL;
		}
		goto out_empty;
	}

	if (xtp.encoding == XA_STRING) {
		/* single string value; copy that */
		newval = cstr_dup((char const *) xtp.value);
		if (!newval) {
			log_set("Could not duplicate %s XTextProperty value: %s",
			        propname, log_str());
			goto error_xtp_copy;
		}
		if (*value)
			cstr_free(*value);
		*value = newval;
	} else {
		/* list of values, extract to array and get first element */
		xtpres = XmbTextPropertyToTextList(xserver.display,
		                                   &xtp, &list, &nxtp);
		if (xtpres < 0) {
			log_set("XmbTextPropertyToTextList() failed: %s",
			        xtpres == XNoMemory ? "XNoMemory" :
			        xtpres == XLocaleNotSupported ? "XLocaleNotSupported" :
			        xtpres == XConverterNotFound ? "XConverterNotFound" :
			        "(unknown reason)");
			goto error_xmb_tp2tl;
		}
		if (nxtp <= 0 || !list) {
			DEBUG("X window %lu: Property %s has 0 XmbTextProperty elements; clearing value",
			      xwin->id, propname);
			if (*value) {
				cstr_free(*value);
				*value = NULL;
			}
			goto out;
		}
		newval = cstr_dup(list[0]);
		if (!newval) {
			log_set("Could not duplicate text list element 0: %s",
			        log_str());
			goto error_xmb_copy;
		}
		XFreeStringList(list);
		if (*value)
			cstr_free(*value);
		*value = newval;
	}

 out:
	XFree(xtp.value);
 out_empty:
	XFree(propname);
	return 0;

 error_xmb_copy:
	XFreeStringList(list);
 error_xmb_tp2tl:
 error_xtp_copy:
	XFree(xtp.value);
	XFree(propname);
	return -1;
}

static int
_set_fullscreen(struct xwindow *xwin, bool fullscreen)
{
	if (fullscreen == xwin->state.fullscreen)
		return 0;

	xwin->state.fullscreen = fullscreen;
	if (_set_state(xwin) < 0) {
		log_set("Could not update _NET_WM_STATE: %s", log_str());
		goto error_state;
	}

	bus_channel_send(bus.x_window_notify_state_fullscreen, xwin);
	return 0;

 error_state:
	return -1;
}

static int
_set_modal(struct xwindow *xwin, bool modal)
{
	if (modal == xwin->state.modal)
		return 0;

	xwin->state.modal = modal;
	if (_set_state(xwin) < 0) {
		log_set("Could not update _NET_WM_STATE: %s", log_str());
		goto error_state;
	}

	bus_channel_send(bus.x_window_notify_state_modal, xwin);
	return 0;

 error_state:
	return -1;
}

static int
_set_property(struct xwindow const *xwin, Atom property,
              size_t nvalues, Atom const *values)
{
	if (!XChangeProperty(xserver.display, xwin->id, property, XA_ATOM, 32,
	                     PropModeReplace, (char unsigned const *) values,
	                     (int signed) nvalues))
	{
		log_set("XChangeProperty() failed");
		goto error_changeprop;
	}

	return 0;

 error_changeprop:
	return -1;
}

static int
_set_state(struct xwindow const *xwin)
{
	Atom property = xserver.atoms[XATOM_NET_WM_STATE];
	Atom states[2];
	size_t nstates = 0;

	if (xwin->state.fullscreen)
		states[nstates++] = xserver.atoms[XATOM_NET_WM_STATE_FULLSCREEN];
	if (xwin->state.modal)
		states[nstates++] = xserver.atoms[XATOM_NET_WM_STATE_MODAL];

	return _set_property(xwin, property, nstates, states);
}

static enum xwindow_type
_type_from_atom(Atom type)
{
	if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DESKTOP])
		return XWINDOW_TYPE_DESKTOP;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DOCK])
		return XWINDOW_TYPE_DOCK;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_TOOLBAR])
		return XWINDOW_TYPE_TOOLBAR;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_MENU])
		return XWINDOW_TYPE_MENU;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_UTILITY])
		return XWINDOW_TYPE_UTILITY;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_SPLASH])
		return XWINDOW_TYPE_SPLASH;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DIALOG])
		return XWINDOW_TYPE_DIALOG;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DROPDOWN_MENU])
		return XWINDOW_TYPE_DROPDOWN_MENU;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_POPUP_MENU])
		return XWINDOW_TYPE_POPUP_MENU;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_TOOLTIP])
		return XWINDOW_TYPE_TOOLTIP;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_NOTIFICATION])
		return XWINDOW_TYPE_NOTIFICATION;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_COMBO])
		return XWINDOW_TYPE_COMBO;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DND])
		return XWINDOW_TYPE_DND;
	else if (type == xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_NORMAL])
		return XWINDOW_TYPE_NORMAL;
	else
		return XWINDOW_TYPE_UNKNOWN;
}
