#include <karuiwm/monitor.h>
#include <karuiwm/bus.h>
#include <karuiwm/client.h>
#include <karuiwm/layout.h>
#include <karuiwm/seat.h>
#include <karuiwm/utils.h>
#include <karuiwm/xserver.h>
#include <karuiwm/xwindow.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static int _init_bus(struct monitor *mon);
static int _moveresize(struct monitor *mon);

struct monitor *
monitor_alloc(struct xscreen *xscr)
{
	struct monitor *mon;

	mon = memory_alloc(sizeof(struct monitor), "monitor structure");
	if (!mon)
		goto error_alloc;

	/* (back)reference to seat */
	mon->focused = false;

	/* dimensions and margins */
	mon->xscreen = xscr;
	mon->dim = xscr->dimension;
	mon->margins.top = 0;
	mon->margins.right = 0;
	mon->margins.bottom = 0;
	mon->margins.left = 0;

	/* workspace */
	mon->workspace = NULL;
	mon->scratchpad = NULL;
	mon->selws = mon->workspace;
	if (_init_bus(mon) < 0)
		goto error_bus;

	return mon;

 error_bus:
	log_push();
	memory_free(mon);
	log_pop();
 error_alloc:
	return NULL;
}

void
monitor_free(struct monitor *mon)
{
	(void) monitor_show_scratchpad(mon, NULL);
	monitor_show_workspace(mon, NULL);
	bus_disconnect(mon->bus_session);
	memory_free(mon);
}

int
monitor_arrange(struct monitor *mon)
{
	struct client *c;
	int unsigned i, pad_margin;
	struct rectangle dim_work, dim_pad, dim_cli;

	if (state_set_clientmask(false) < 0) {
		log_set("Could not unset clientmask on state: %s", log_str());
		return -1;
	}

	/* work area and scratchpad area (TODO: use _NET_WM_STRUTS) */
	dim_work = (struct rectangle) {
		.x = mon->dim.x + (int signed) mon->margins.left,
		.y = mon->dim.y + (int signed) mon->margins.top,
		.width = mon->dim.width
		       - mon->margins.left - mon->margins.right,
		.height = mon->dim.height
		        - mon->margins.top - mon->margins.bottom,
	};

	if (mon->scratchpad) {
		pad_margin = seat.config.scratchpad_margin;
		dim_pad = (struct rectangle) {
			.x = mon->dim.x + (int signed) pad_margin,
			.y = mon->dim.y + (int signed) pad_margin,
			.width = mon->dim.width
			         - MIN(dim_work.width - 1, 2 * pad_margin),
			.height = mon->dim.height
			          - MIN(dim_work.height - 1, 2 * pad_margin),
		};
		layout_apply(mon->scratchpad->layout, &dim_pad,
		             seat.config.client_border_width);
	}

	if (mon->workspace) {
		/* fullscreen */
		LIST_FOR (mon->workspace->tiled, i, c)
			if (c->fullscreen)
				client_moveresize_fullscreen(c);
		LIST_FOR (mon->workspace->floating, i, c)
			if (c->fullscreen)
				client_moveresize_fullscreen(c);

		/* floating */
		LIST_FOR (mon->workspace->floating, i, c) {
			dim_cli = c->dim_floating;
			if (c->splash) {
				(void) rectangle_centre(dim_work, &dim_cli);
			} else if (rectangle_intersection(dim_work, dim_cli) == 0) {
				(void) rectangle_fit(dim_work, &dim_cli);
			}
			if (client_moveresize(c, dim_cli) < 0)
				ERROR("Could not redimension floating client %lu to %ux%u%+d%+d: %s",
				      c->xwindow->id,
				      dim_cli.width, dim_cli.height,
				      dim_cli.x, dim_cli.y, log_str());
		}

		/* tiled */
		layout_apply(mon->workspace->layout, &dim_work,
		             seat.config.client_border_width);
	}

	if (state_set_clientmask(true) < 0)
		ERROR("Could not set clientmask on state: %s", log_str());

	return 0;
}

void
monitor_focus(struct monitor *mon, bool focus)
{
	if (mon->focused == focus)
		return;

	mon->focused = focus;
	if (mon->selws)
		workspace_focus(mon->selws, focus);
	else if (focus)
		XSetInputFocus(xserver.display, xserver.root->id,
		               RevertToPointerRoot, CurrentTime);
}

bool
monitor_has_xscreen(struct monitor const *mon, struct xscreen const *xscr)
{
	return mon->xscreen == xscr;
}

void
monitor_restack(struct monitor *mon)
{
	struct client *c;
	Window *windows;
	size_t nwindows;
	int unsigned i, si;

	if (!mon->selws)
		return;

	nwindows = mon->workspace->tiled->size + mon->workspace->floating->size;
	if (mon->scratchpad)
		/* scratchpad should not have any floating windows */
		nwindows += mon->scratchpad->tiled->size;
	if (nwindows == 0)
		return;

	windows = memory_alloc(nwindows * sizeof(Window), "X windows array");
	if (!windows)
		FATAL("Could not create list of %zu X windows: %s",
		      nwindows, log_str());

	si = 0;

	/* scratchpad */
	if (mon->scratchpad) {
		LIST_FOR (mon->scratchpad->tiled, i, c)
			if (c == mon->scratchpad->selcli)
				windows[si++] = c->xwindow->id;
		LIST_FOR (mon->scratchpad->tiled, i, c)
			if (c != mon->scratchpad->selcli)
				windows[si++] = c->xwindow->id;
	}

	/* fullscreen */
	LIST_FOR (mon->workspace->tiled, i, c)
		if (c->fullscreen && c == mon->workspace->selcli)
			windows[si++] = c->xwindow->id;
	LIST_FOR (mon->workspace->floating, i, c)
		if (c->fullscreen && c == mon->workspace->selcli)
			windows[si++] = c->xwindow->id;
	LIST_FOR (mon->workspace->tiled, i, c)
		if (c->fullscreen && c != mon->workspace->selcli)
			windows[si++] = c->xwindow->id;
	LIST_FOR (mon->workspace->floating, i, c)
		if (c->fullscreen && c != mon->workspace->selcli)
			windows[si++] = c->xwindow->id;

	/* floating */
	LIST_FOR (mon->workspace->floating, i, c)
		if (!c->fullscreen)
			windows[si++] = c->xwindow->id;

	/* tiled */
	LIST_FOR (mon->workspace->tiled, i, c)
		if (!c->fullscreen && c == mon->workspace->selcli)
			windows[si++] = c->xwindow->id;
	LIST_FOR (mon->workspace->tiled, i, c)
		if (!c->fullscreen && c != mon->workspace->selcli)
			windows[si++] = c->xwindow->id;

	XRaiseWindow(xserver.display, windows[0]);
	XRestackWindows(xserver.display, windows, (int signed) nwindows);
	memory_free(windows);
}

int
monitor_show_scratchpad(struct monitor *mon, struct workspace *pad)
{
	struct monitor *oldmon;
	struct workspace *oldpad;

	/* get other monitor */
	oldmon = pad ? pad->mon : NULL;
	oldpad = mon->scratchpad;

	/* if nothing changes, don't do anything */
	if (pad == oldpad || mon == oldmon)
		return 0;

	if (pad) {
		/* show and focus scratchpad */
		mon->scratchpad = pad;
		mon->scratchpad->mon = mon;
		pad->mon = mon;
		if (!oldmon)
			workspace_show(pad, true);
		workspace_focus(pad, mon == seat.selmon);
		monitor_restack(mon);
		if (monitor_arrange(mon) < 0)
			ERROR("Could not rearrange clients: %s", log_str());
		mon->selws = mon->scratchpad;

		/* unfocus regular workspace */
		if (mon->workspace)
			workspace_focus(mon->workspace, false);
	} else {
		/* focus regular workspace */
		if (mon->workspace)
			workspace_focus(mon->workspace,
			                mon == seat.selmon);
		mon->selws = mon->workspace;

		/* unfocus and hide scratchpad */
		workspace_focus(mon->scratchpad, false);
		workspace_show(mon->scratchpad, false);
		mon->scratchpad->mon = NULL;
		mon->scratchpad = NULL;
	}

	if (oldmon) {
		/* scratchpad was visible on other monitor */
		oldmon->scratchpad = NULL;
		if (oldmon->workspace && oldmon == seat.selmon)
			workspace_focus(oldmon->workspace, true);
		oldmon->selws = oldmon->workspace;
	}

	if (bus_subscription_set_data_mask(
	                               mon->bus_subscriptions.scratchpad_layout,
	                               pad) < 0)
		ERROR("Could not update data mask for bus event subscription on scratchpad: %s",
		      log_str());

	return 0;
}

void
monitor_show_workspace(struct monitor *mon, struct workspace *ws)
{
	struct workspace *oldws = mon->workspace;
	struct monitor *oldmon = ws ? ws->mon : NULL;

	/* if nothing changes, don't do anything */
	if (ws == oldws || mon == oldmon)
		return;

	/* update on this monitor */
	mon->workspace = ws;
	if (!mon->scratchpad)
		mon->selws = ws;
	if (ws) {
		ws->mon = mon;
		if (!oldmon)
			workspace_show(ws, true);
		if (monitor_arrange(mon) < 0)
			ERROR("Could not rearrange clients: %s", log_str());
		if (!mon->scratchpad)
			workspace_focus(ws, mon->focused);
	}

	if (oldmon) {
		/* update on other monitor */
		oldmon->workspace = oldws;
		if (oldws)
			oldws->mon = oldmon;
		if (monitor_arrange(oldmon) < 0)
			ERROR("Could not rearrange clients: %s", log_str());
		if (!oldmon->scratchpad) {
			oldmon->selws = oldws;
			if (oldws)
				workspace_focus(oldws, oldmon->focused);
		}
		bus_channel_send(bus.monitor_workspace, oldmon);
	} else if (oldws) {
		/* hide old workspace */
		workspace_focus(oldws, false);
		workspace_show(oldws, false);
		oldws->mon = NULL;
	}

	if (bus_subscription_set_data_mask(
	                                mon->bus_subscriptions.workspace_layout,
	                                ws) < 0)
		ERROR("Could not %sset data mask for bus event subscription on workspace '%s': %s",
		      ws ? "" : "un", ws ? ws->name : oldws->name, log_str());

	bus_channel_send(bus.monitor_workspace, mon);
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct monitor *mon = ctx;
	struct xscreen *xscr;
	struct workspace *ws;

	if (ch == bus.workspace_layout) {
		ws = data;
		if (ws != mon->workspace && ws != mon->scratchpad) {
			WARN("Monitor received bus event '%s' for unassociated workspace '%s'; ignoring",
			     ch->name, ws ? ws->name : NULL);
			return;
		}
		DEBUG("Monitor received bus event '%s'; rearranging", ch->name);
		if (monitor_arrange(mon) < 0)
			ERROR("Could not rearrange monitor after layout change on workspace '%s': %s",
			      ws->name, log_str());
	} else if (ch == bus.x_screen_resize) {
		xscr = data;
		if (xscr != mon->xscreen) {
			WARN("Monitor received bus event '%s' for unassociated X screen; ignoring",
			     ch->name);
			return;
		}
		if (_moveresize(mon) < 0)
			ERROR("Could not handle X screen resize event: %s",
			      log_str());
	} else {
		BUG("Monitor received unexpected bus event '%s'", ch->name);
	}
}

static int
_init_bus(struct monitor *mon)
{
	int unsigned i;
	struct bus_subscription *sub;
	struct {
		struct bus_subscription **sub;
		struct bus_channel *const channel;
		void *mask;
		char const *desc;
	} subs[] = {
		{ &mon->bus_subscriptions.workspace_layout,
		  bus.workspace_layout, NULL, "regular workspace" },
		{ &mon->bus_subscriptions.scratchpad_layout,
		  bus.workspace_layout, NULL, "scratchpad workspace" },
		{ &sub, bus.x_screen_resize, mon->xscreen, "X screen" },
	};

	mon->bus_session = bus_connect("monitor", _bus_event, mon);
	if (!mon->bus_session) {
		log_set("Could not connect to event bus: %s", log_str());
		goto error_connect;
	}

	for (i = 0; i < LENGTH(subs); ++i) {
		*subs[i].sub = bus_subscription_alloc(mon->bus_session,
		                                      subs[i].channel,
		                                      subs[i].mask);
		if (!*subs[i].sub) {
			log_set("Could not join event bus channel '%s' for %s: %s",
			        subs[i].channel->name, subs[i].desc,
				log_str());
			goto error_subscribe;
		}
	}

	return 0;

 error_subscribe:
	log_push();
	bus_disconnect(mon->bus_session);
	log_pop();
 error_connect:
	return -1;
}

static int
_moveresize(struct monitor *mon)
{
	mon->dim = mon->xscreen->dimension;

	if (monitor_arrange(mon) < 0) {
		log_set("Could not rearrange clients: %s", log_str());
		return -1;
	}
	bus_channel_send(bus.monitor_resize, mon);

	return 0;
}
