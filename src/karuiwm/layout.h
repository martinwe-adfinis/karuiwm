#ifndef _KWM_LAYOUT_H
#define _KWM_LAYOUT_H

#include <karuiwm/rectangle.h>

enum layout_type { LAYOUT_RSTACK, LAYOUT_BSTACK, LAYOUT_LFIXED };

struct layout {
	enum layout_type type;
	struct list const *clients;
	struct icon *icon;
	void *state;
};

#include <sys/types.h>

struct layout *layout_alloc(struct list *clients, enum layout_type type);
void layout_free(struct layout *l);

void layout_apply(struct layout const *l, struct rectangle const *area,
                  int unsigned client_border_width);
void layout_setmfact(struct layout const *l, float mfact);
void layout_setnmaster(struct layout const *l, ssize_t nmaster);

#endif /* ndef _KWM_LAYOUT_H */
