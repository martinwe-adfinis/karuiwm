#ifndef _KWM_POSITION_H
#define _KWM_POSITION_H

struct position {
	int x;
	int y;
};

#include <stdbool.h>

bool position_equals(struct position const *pos1, struct position const *pos2);
int position_parse(struct position *pos, char const *str);

#endif /* ndef _KWM_POSITION_H */
