#ifndef _KWM_XOUTPUT_H
#define _KWM_XOUTPUT_H

#include <X11/Xlib.h>

struct xoutput {
	int screen;
	struct list *screens;
	int unsigned screen_depth;
};

int xoutput_init(void);
void xoutput_deinit(void);

int xoutput_scan(void);

extern struct xoutput xoutput;

#endif /* ndef _KWM_XOUTPUT_H */
