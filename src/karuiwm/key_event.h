#ifndef _KWM_KEY_EVENT_H
#define _KWM_KEY_EVENT_H

enum key_event_type {
	KEY_EVENT_CURRENT,    /* placeholder */
	KEY_PRESS,
	KEY_RELEASE,
};

#include <karuiwm/key.h>

struct key_event {
	enum key_event_type type;
	struct key key;
};


char const *key_event_type_str(enum key_event_type kevtype);

#endif /* ndef _KWM_KEY_EVENT_H */
