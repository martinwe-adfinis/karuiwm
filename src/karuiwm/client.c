#include <karuiwm/client.h>
#include <karuiwm/bus.h>
#include <karuiwm/monitor.h>
#include <karuiwm/utils.h>
#include <karuiwm/xserver.h>
#include <karuiwm/xwindow.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>
#include <limits.h>
#include <string.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static int _init_bus(struct client *c);
static int _map_window(struct client *c, bool mapped);
static int _query_type(struct client *c);
static int _refresh_border(struct client *c);
static int _refresh_border_colour(struct client *c);
static int _refresh_border_width(struct client *c);
static bool _update_type(struct client *c);

static struct bus_channel *const *_XWINDOW_CHANNELS[] = {
	&bus.x_window_notify_destroy,
	&bus.x_window_notify_state_fullscreen,
	&bus.x_window_notify_state_modal,
	&bus.x_window_notify_transience,
	&bus.x_window_notify_type,
	&bus.x_window_notify_unmap,
};

struct client *
client_alloc(struct xwindow *xwin)
{
	struct client *c;

	c = memory_alloc(sizeof(struct client), "client structure");
	if (!c)
		goto error_alloc;

	c->xwindow = xwin;
	c->xwindow->client = c;

	c->ws = NULL;
	c->dialog = false;
	c->floating = false;
	c->fullscreen = false;
	c->visible = xwindow_is_viewable(xwin);

	/* dimensions */
	c->dim_floating = (struct rectangle) {
		.pos = xwin->sizehints.has_position ? xwin->sizehints.position
		                                    : xwin->dimension.pos,
		.width = xwin->sizehints.size.width > 0
		       ? xwin->sizehints.size.width
		       : xwin->dimension.width,
		.height = xwin->sizehints.size.height > 0
		        ? xwin->sizehints.size.height
		        : xwin->dimension.height,
	};
	DEBUG("Managing X window %lu with dimensions %ux%u%+d%+d",
	      c->xwindow->id, c->dim_floating.width, c->dim_floating.height,
	      c->dim_floating.x, c->dim_floating.y);

	/* transience */
	if (xwindow_query_transience(c->xwindow) < 0)
		WARN("Could not query transience relation for X window %lu during client creation: %s",
		     xwin->id, log_str());
	if (c->xwindow->transient_for) {
		DEBUG("X window %lu is transient, marking client as dialog and floating",
		      xwin->id);
		c->dialog = true;
	}

	/* type */
	if (_query_type(c) < 0) {
		log_set("Could not query X window type: %s", log_str());
		goto error_query_type;
	}

	/* state */
	if (xwindow_query_state(c->xwindow) < 0) {
		log_set("Could not query X window state: %s", log_str());
		goto error_query_state;
	}
	c->dialog |= c->xwindow->state.modal;
	c->fullscreen = c->xwindow->state.fullscreen;

	/* input and appearance */
	c->focused = false;
	c->border.width = 0;
	c->border.colour_focused = 0x000000;
	c->border.colour_unfocused = 0x000000;
	if (c->visible)
		if (_refresh_border(c) < 0)
			if (!c->xwindow->destroyed)
				ERROR("Could not refresh border colour: %s",
				      log_str());

	if (_init_bus(c) < 0)
		goto error_bus;

	return c;

 error_bus:
 error_query_state:
 error_query_type:
	xwin->client = NULL;
	log_push();
	memory_free(c);
	log_pop();
 error_alloc:
	return NULL;
}

void
client_free(struct client *c)
{
	bus_disconnect(c->bus_session);
	c->xwindow->client = NULL;
	memory_free(c);
}

void
client_get_sizehints(struct client const *c, struct size *size)
{
	if (xwindow_apply_sizehints(c->xwindow, size) < 0)
		ERROR("Could not apply size hints for X window %lu on size %ux%u: %s",
		      c->xwindow->id, size->width, size->height, log_str());
}

void
client_kill(struct client *c)
{
	/* try to kill the client via the WM_DELETE_WINDOW atom */
	if (client_send_event(c, xserver.atoms[XATOM_WM_DELETE_WINDOW]))
		return;

	/* otherwise massacre the client */
	XGrabServer(xserver.display);
	XSetCloseDownMode(xserver.display, DestroyAll);
	XKillClient(xserver.display, c->xwindow->id);
	XUngrabServer(xserver.display);
}

int
client_move(struct client *c, struct position pos)
{
	if (c->fullscreen)
		return 0;

	if (xwindow_move(c->xwindow, pos) < 0) {
		log_set("Could not move X window %lu to %+d%+d: %s",
		        c->xwindow->id, pos.x, pos.y, log_str());
		goto error_redim;
	}
	if (c->floating)
		c->dim_floating.pos = c->xwindow->dimension.pos;
	bus_channel_send(bus.client_dimension, c);

	return 0;

 error_redim:
	return -1;
}

int
client_move_floating(struct client *c, struct position pos)
{
	if (c->floating && !c->fullscreen) {
		if (xwindow_move(c->xwindow, pos) < 0) {
			log_set("Could not move X window %lu to %+d%+d: %s",
			        c->xwindow->id, pos.x, pos.y, log_str());
			goto error_redim;
		}
		c->dim_floating.pos = c->xwindow->dimension.pos;
		bus_channel_send(bus.client_dimension, c);
	} else {
		c->dim_floating.pos = pos;
	}

	return 0;

 error_redim:
	return -1;
}

int
client_moveresize(struct client *c, struct rectangle dim)
{
	if (c->fullscreen)
		return 0;

	if (xwindow_moveresize(c->xwindow, dim, c->floating) < 0) {
		log_set("Could not move-resize X window %lu to %ux%u%+d%+d: %s",
		        c->xwindow->id,
		        dim.width, dim.height, dim.x, dim.y, log_str());
		goto error_redim;
	}
	if (c->floating)
		c->dim_floating = c->xwindow->dimension;
	bus_channel_send(bus.client_dimension, c);

	return 0;

 error_redim:
	return -1;
}

int
client_moveresize_floating(struct client *c, struct rectangle dim)
{
	if (c->floating && !c->fullscreen) {
		if (xwindow_moveresize(c->xwindow, dim, true) < 0) {
			log_set("Could not move-resize X window %lu to %ux%u%+d%+d: %s",
			        c->xwindow->id,
			        dim.width, dim.height, dim.x, dim.y, log_str());
			goto error_redim;
		}
		c->dim_floating = c->xwindow->dimension;
		bus_channel_send(bus.client_dimension, c);
	} else {
		if (xwindow_apply_sizehints(c->xwindow, &dim.size) < 0) {
			log_set("Could not apply size hints for X window %lu to %ux%u%+d%+d: %s",
			        c->xwindow->id,
			        dim.width, dim.height, dim.x, dim.y, log_str());
			goto error_redim;
		}
		c->dim_floating = dim;
	}

	return 0;

 error_redim:
	return -1;
}

int
client_moveresize_fullscreen(struct client *c)
{
	struct rectangle dim;

	if (!c->fullscreen) {
		BUG("Attempt to fullscreen-move-resize non-fullscreen client %lu",
		    c->xwindow->id);
		return -1;
	}

	if (!c->ws) {
		BUG("Attempt to fullscreen-move-resize unattached client %lu",
		    c->xwindow->id);
		return -1;
	}

	if (!c->ws->mon) {
		BUG("Attempt to fullscreen-move-resize client %lu on hidden workspace '%s'",
		    c->xwindow->id, c->ws->name);
		return -1;
	}

	dim = c->ws->mon->dim;
	if (xwindow_moveresize(c->xwindow, dim, false) < 0) {
		log_set("Could not move-resize X window %lu to monitor dimensions %ux%u%+d%+d: %s",
		        c->xwindow->id, dim.width, dim.height, dim.x, dim.y,
		        log_str());
		goto error_redim;
	}
	bus_channel_send(bus.client_dimension, c);

	return 0;

 error_redim:
	return -1;
}

int
client_resize(struct client *c, struct size size)
{
	if (c->fullscreen)
		return 0;

	if (xwindow_resize(c->xwindow, size, c->floating) < 0) {
		log_set("Could not resize X window %lu to %ux%u: %s",
		        c->xwindow->id, size.width, size.height,
		        log_str());
		goto error_redim;
	}
	if (c->floating)
		c->dim_floating.size = c->xwindow->dimension.size;
	bus_channel_send(bus.client_dimension, c);

	return 0;

 error_redim:
	return -1;
}

int
client_resize_floating(struct client *c, struct size size)
{
	if (c->floating && !c->fullscreen) {
		if (xwindow_resize(c->xwindow, size, true) < 0) {
			log_set("Could not resize X window %lu to %ux%u: %s",
			        c->xwindow->id, size.width, size.height,
			        log_str());
			goto error_redim;
		}
		c->dim_floating.size = c->xwindow->dimension.size;
		bus_channel_send(bus.client_dimension, c);
	} else {
		if (xwindow_apply_sizehints(c->xwindow, &size) < 0) {
			log_set("Could not apply size hints for X window %lu to %ux%u: %s",
			        c->xwindow->id,
			        size.width, size.height, log_str());
			goto error_redim;
		}
		c->dim_floating.size = size;
	}

	return 0;

 error_redim:
	return -1;
}

bool
client_send_event(struct client *c, Atom atom)
{
	int unsigned i;
	int n;
	Atom *supported_atoms;
	bool is_supported;
	XEvent ev;

	if (c->xwindow->destroyed)
		return false;

	if (!XGetWMProtocols(xserver.display, c->xwindow->id,
	                     &supported_atoms, &n))
	{
		WARN("Could not get supported WM protocols for %lu",
		     c->xwindow->id);
		return false;
	}
	if (n <= 0) {
		DEBUG("X window %lu does not support any protocols",
		      c->xwindow->id);
		return false;
	}

	for (i = 0, is_supported = false;
	     i < (int unsigned) n && !is_supported;
	     ++i)
		if (supported_atoms[i] == atom)
			is_supported = true;
	XFree(supported_atoms);
	if (!is_supported) {
		WARN("Atom %lu is not supported by %lu", atom, c->xwindow->id);
		return false;
	}

	ev.type = ClientMessage;
	ev.xclient.window = c->xwindow->id;
	ev.xclient.message_type = xserver.atoms[XATOM_WM_PROTOCOLS];
	ev.xclient.format = 32;
	ev.xclient.data.l[0] = (int long signed) atom;
	ev.xclient.data.l[1] = (int long signed) CurrentTime;
	XSendEvent(xserver.display, c->xwindow->id, False, NoEventMask, &ev);
	return true;
}

int
client_set_border_colour_focused(struct client *c, uint32_t col)
{
	if (c->border.colour_focused == col)
		return 0;

	c->border.colour_focused = col;

	if (c->visible && c->focused)
		if (_refresh_border_colour(c) < 0) {
			log_set("Could not refresh border colour: %s",
			        log_str());
			return -1;
		}

	return 0;
}

int
client_set_border_colour_unfocused(struct client *c, uint32_t col)
{
	if (c->border.colour_unfocused == col)
		return 0;

	c->border.colour_unfocused = col;

	if (c->visible && !c->focused)
		if (_refresh_border_colour(c) < 0) {
			log_set("Could not refresh border colour: %s",
			        log_str());
			return -1;
		}

	return 0;
}

int
client_set_border_width(struct client *c, int unsigned border_width)
{
	int bwdiff;
	struct position newpos;

	if (c->border.width == border_width)
		return 0;

	bwdiff = (int signed) border_width - (int signed) c->border.width;
	c->border.width = border_width;

	if (c->visible) {
		if (_refresh_border_width(c) < 0) {
			log_set("Could not refresh border width: %s",
			        log_str());
			return -1;
		}
		/* Keep window content at same position. */
		if (c->floating) {
			newpos.x = c->dim_floating.x - bwdiff;
			newpos.y = c->dim_floating.y - bwdiff;
			if (client_move(c, newpos) < 0)
				ERROR("Could not move client after changing border width: %s",
				      log_str());
		}
	}
	return 0;
}

void
client_set_focus(struct client *c, bool focus)
{
	if (focus == c->focused)
		return;

	c->focused = focus;
	if (c->visible) {
		if (xwindow_set_focus(c->xwindow, focus) < 0)
			if (!c->xwindow->destroyed)
				ERROR("Could not grab input: %s", log_str());
		if (_refresh_border(c) < 0)
			if (!c->xwindow->destroyed)
				ERROR("Could not refresh border: %s",
				      log_str());
	}
}

int
client_set_fullscreen(struct client *c, bool fullscreen)
{
	c->fullscreen = fullscreen;

	if (xwindow_set_fullscreen(c->xwindow, fullscreen) < 0) {
		log_set("Could not set fullscreen property on X window: %s",
		        log_str());
		goto error_fullscreen;
	}
	if (xwindow_set_border_width(c->xwindow,
	                             fullscreen ? 0 : c->border.width) < 0)
		WARN("Could not set window border width: %s", log_str());

	return 0;

 error_fullscreen:
	return -1;
}

void
client_show(struct client *c, bool visible)
{
	if (c->visible == visible)
		return;

	c->visible = visible;
	if (visible) {
		if (_map_window(c, true) < 0) {
			if (!c->xwindow->destroyed)
				ERROR("Could not map window: %s", log_str());
			return;
		}
		if (_refresh_border(c) < 0)
			if (!c->xwindow->destroyed)
				ERROR("Could not refresh border: %s",
				      log_str());
		if (c->focused)
			if (xwindow_set_focus(c->xwindow, true) < 0) {
				if (!c->xwindow->destroyed)
					ERROR("Could not grab input: %s",
					      log_str());
				return;
			}
	} else {
		if (_map_window(c, false) < 0) {
			if (!c->xwindow->destroyed)
				ERROR("Could not unmap window: %s", log_str());
			return;
		}
	}
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct client *c = ctx;
	struct xwindow *xwin;
	int unsigned i;
	bool matched;

	for (i = 0, matched = false; i < LENGTH(_XWINDOW_CHANNELS); ++i)
		if (ch == *_XWINDOW_CHANNELS[i])
			matched = true;

	if (!matched) {
		BUG("Client received unexpected bus event '%s'", ch->name);
		return;
	}

	xwin = data;
	if (xwin != c->xwindow) {
		BUG("Bus event '%s' for X window %lu not associated with client %lu",
		    ch->name, xwin->id, c->xwindow->id);
		return;
	}

	if (ch == bus.x_window_notify_destroy) {
		DEBUG("Client received bus event '%s' for X window %lu; destroying client",
		      ch->name, c->xwindow->id);
		bus_channel_send(bus.client_destroy, c);
		client_free(c);
	} else if (ch == bus.x_window_notify_state_fullscreen) {
		DEBUG("Client received bus event '%s' for X window %lu; updating state info",
		      ch->name, c->xwindow->id);
		if (client_set_fullscreen(c, c->xwindow->state.fullscreen) < 0)
			ERROR("Could not set fullscreen state for client %lu: %s",
			      c->xwindow->id, log_str());
		bus_channel_send(bus.client_state_fullscreen, c);
	} else if (ch == bus.x_window_notify_state_modal) {
		DEBUG("Client received bus event '%s' for X window %lu; updating state info",
		      ch->name, c->xwindow->id);
		if (c->dialog == c->xwindow->state.modal)
			return;
		c->dialog = c->xwindow->state.modal;
		bus_channel_send(bus.client_state_dialog, c);
	} else if (ch == bus.x_window_notify_transience) {
		log_set("Client received bus event '%s' for X window %lu",
		        ch->name, c->xwindow->id);
		if (c->xwindow->transient_for) {
			DEBUG("%s (transient for %lu); marking client as dialog",
			      log_str(), c->xwindow->transient_for->id);
			c->dialog = true;
		} else {
			DEBUG("%s (not transient)", log_str());
		}
		bus_channel_send(bus.client_transience, c);
	} else if (ch == bus.x_window_notify_type) {
		DEBUG("Client received bus event '%s' for X window %lu; updating type info",
		      ch->name, c->xwindow->id);
		if (!_update_type(c))
			/* nothing changed */
			return;
		bus_channel_send(bus.client_type, c);
	} else if (ch == bus.x_window_notify_unmap) {
		if (!c->visible)
			/* We already know of this being unmapped, so this event
			 * was likely caused by ourselves; ignore it.
			 * This is a workaround until we start reparenting and
			 * we can properly distinguish a client-managed window
			 * from the WM-managed "frame" window.
			 */
			return;
		DEBUG("Client received unmap event '%s' for X window %lu; unmanaging",
		      ch->name, xwin->id);
		bus_channel_send(bus.client_destroy, c);
		client_free(c);
	} else {
		BUG("Client received unexpected bus event '%s'", ch->name);
	}
}

static int
_init_bus(struct client *c)
{
	int unsigned i;
	struct bus_channel *ch;

	c->bus_session = bus_connect("client", _bus_event, c);
	if (!c->bus_session) {
		log_set("Could not open event bus session: %s", log_str());
		goto error_bus_session;
	}

	for (i = 0; i < LENGTH(_XWINDOW_CHANNELS); ++i) {
		ch = *_XWINDOW_CHANNELS[i];
		if (!bus_subscription_alloc(c->bus_session,
		                            ch, c->xwindow))
		{
			log_set("Could not join event bus channel '%s' for X window %lu: %s",
			        ch->name, c->xwindow->id, log_str());
			goto error_bus_subscribe;
		}
	}

	return 0;

 error_bus_subscribe:
	log_push();
	bus_disconnect(c->bus_session);
	log_pop();
 error_bus_session:
	return -1;
}

static int
_map_window(struct client *c, bool mapped)
{
	if (c->xwindow->destroyed) {
		log_set("X window %lu is destroyed", c->xwindow->id);
		return -1;
	}

	return mapped ? xwindow_map(c->xwindow) : xwindow_unmap(c->xwindow);
}

static int
_query_type(struct client *c)
{
	if (xwindow_query_type(c->xwindow) < 0) {
		log_set("Could not query type for X window: %s", log_str());
		return -1;
	}

	(void) _update_type(c);
	return 0;
}

static int
_refresh_border(struct client *c)
{
	if (_refresh_border_colour(c) < 0) {
		log_set("%s", log_str());
		return -1;
	}
	if (_refresh_border_width(c) < 0) {
		log_set("%s", log_str());
		return -1;
	}
	return 0;
}

static int
_refresh_border_colour(struct client *c)
{
	uint32_t col;

	col = c->focused ? c->border.colour_focused
	                 : c->border.colour_unfocused;
	if (xwindow_set_border_colour(c->xwindow, col) < 0) {
		log_set("Could not set border colour for X window %lu to #%06X: %s",
		        c->xwindow->id, col, log_str());
		goto error_xwin;
	}

	return 0;

 error_xwin:
	return -1;
}

static int
_refresh_border_width(struct client *c)
{
	int unsigned width;

	width = c->fullscreen || c->splash ? 0 : c->border.width;
	if (xwindow_set_border_width(c->xwindow, width) < 0) {
		log_set("Could not set border width for X window %lu to %u: %s",
		        c->xwindow->id, width, log_str());
		goto error_xwin;
	}

	return 0;

 error_xwin:
	return -1;
}

static bool
_update_type(struct client *c)
{
	bool oldsplash = c->splash;
	bool olddialog = c->dialog;

	c->splash = c->xwindow->type == XWINDOW_TYPE_SPLASH;
	c->dialog = c->xwindow->type == XWINDOW_TYPE_TOOLBAR
	         || c->xwindow->type == XWINDOW_TYPE_MENU
	         || c->xwindow->type == XWINDOW_TYPE_UTILITY
	         || c->xwindow->type == XWINDOW_TYPE_DIALOG
	         || c->xwindow->type == XWINDOW_TYPE_DROPDOWN_MENU
	         || c->xwindow->type == XWINDOW_TYPE_POPUP_MENU
	         || c->xwindow->type == XWINDOW_TYPE_TOOLTIP
	         || c->xwindow->type == XWINDOW_TYPE_NOTIFICATION
	         || c->xwindow->type == XWINDOW_TYPE_COMBO
	         || c->xwindow->type == XWINDOW_TYPE_DND
	         || c->xwindow->state.modal
	         || c->xwindow->transient_for;

	return oldsplash != c->splash || olddialog != c->dialog;
}
