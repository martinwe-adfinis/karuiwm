#ifndef _KWM_BUTTONMAP_H
#define _KWM_BUTTONMAP_H

#include <stdbool.h>

struct buttonmap {
	char *name;
	struct list *buttonbinds;
	struct buttonbind *active_buttonbind;
	bool active;
};

#include <karuiwm/button.h>
#include <karuiwm/buttonbind.h>

struct buttonmap *buttonmap_alloc(char const *name);
void buttonmap_free(struct buttonmap *bm);

int buttonmap_activate(struct buttonmap *bm);
int buttonmap_add_buttonbind(struct buttonmap *bm, struct buttonbind *bb,
                             bool overwrite);
int buttonmap_deactivate(struct buttonmap *bm);
bool buttonmap_has_action(struct buttonmap const *bm, struct action const *a);
bool buttonmap_has_action_with_name(struct buttonmap const *bm,
                                    char const *name);
bool buttonmap_has_name(struct buttonmap const *bm, char const *name);
int buttonmap_resolve_button(struct buttonmap *bm,
                             struct button_event const *bev);
int buttonmap_unbind_button(struct buttonmap *bm, struct button const *b);

#endif /* ndef _KWM_BUTTONMAP_H */
