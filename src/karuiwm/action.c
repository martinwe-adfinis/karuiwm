#include <karuiwm/action.h>
#include <karuiwm/action_call.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>

struct action *
action_alloc(char const *name,
             int (*function)(struct list **, struct list const *),
             size_t nargs, enum value_type const *arg_types)
{
	struct action *a;
	size_t i;

	a = memory_alloc(sizeof(struct action), "action structure");
	if (!a)
		goto error_alloc;

	/* name */
	a->name = cstr_dup(name);
	if (!a->name) {
		log_set("Could not copy name: %s", log_str());
		goto error_name;
	}

	/* arguments */
	a->nargs = nargs;
	a->arg_types = memory_alloc(nargs * sizeof(enum value_type),
	                            "argument type list");
	if (!a->arg_types)
		goto error_argtypes;
	for (i = 0; i < nargs; ++i)
		a->arg_types[i] = arg_types[i];

	/* function */
	a->function = function;

	a->action_calls = list_alloc();
	if (!a->action_calls) {
		log_set("Could not create list for action calls: %s",
		        log_str());
		goto error_actioncalls;
	}

	return a;

 error_actioncalls:
	log_push();
	memory_free(a->arg_types);
	log_pop();
 error_argtypes:
	log_push();
	cstr_free(a->name);
	log_pop();
 error_name:
	log_push();
	memory_free(a);
	log_pop();
 error_alloc:
	return NULL;
}

void
action_free(struct action *a)
{
	while (a->action_calls->size > 0)
		action_call_unlink(a->action_calls->elements[0]);
	list_free(a->action_calls);
	memory_free(a->arg_types);
	cstr_free(a->name);
	memory_free(a);
}

bool
action_has_name(struct action const *a, char const *name)
{
	return cstr_equals(a->name, name);
}
