#ifndef _KWM_XWINDOW_H
#define _KWM_XWINDOW_H

enum xwindow_class {
	XWINDOW_CLASS_CLIENT,
	XWINDOW_CLASS_OWNED,
	XWINDOW_CLASS_ROOT,
};

enum xwindow_type {
	XWINDOW_TYPE_DESKTOP,
	XWINDOW_TYPE_DOCK,
	XWINDOW_TYPE_TOOLBAR,
	XWINDOW_TYPE_MENU,
	XWINDOW_TYPE_UTILITY,
	XWINDOW_TYPE_SPLASH,
	XWINDOW_TYPE_DIALOG,
	XWINDOW_TYPE_DROPDOWN_MENU,
	XWINDOW_TYPE_POPUP_MENU,
	XWINDOW_TYPE_TOOLTIP,
	XWINDOW_TYPE_NOTIFICATION,
	XWINDOW_TYPE_COMBO,
	XWINDOW_TYPE_DND,
	XWINDOW_TYPE_NORMAL,
	XWINDOW_TYPE_UNKNOWN,
};

enum xwindow_event_mask {
	XWINDOW_EVENT_MASK_KEY_PRESS             = 1<<0,
	XWINDOW_EVENT_MASK_KEY_RELEASE           = 1<<1,
	XWINDOW_EVENT_MASK_BUTTON_PRESS          = 1<<2,
	XWINDOW_EVENT_MASK_BUTTON_RELEASE        = 1<<3,
	XWINDOW_EVENT_MASK_WINDOW_ENTER          = 1<<4,
	XWINDOW_EVENT_MASK_WINDOW_LEAVE          = 1<<5,
	XWINDOW_EVENT_MASK_POINTER_MOTION        = 1<<6,
	XWINDOW_EVENT_MASK_POINTER_MOTION_HINT   = 1<<7,
	XWINDOW_EVENT_MASK_BUTTON_MOTION_1       = 1<<8,
	XWINDOW_EVENT_MASK_BUTTON_MOTION_2       = 1<<9,
	XWINDOW_EVENT_MASK_BUTTON_MOTION_3       = 1<<10,
	XWINDOW_EVENT_MASK_BUTTON_MOTION_4       = 1<<11,
	XWINDOW_EVENT_MASK_BUTTON_MOTION_5       = 1<<12,
	XWINDOW_EVENT_MASK_BUTTON_MOTION         = 1<<13,
	XWINDOW_EVENT_MASK_KEYMAP_STATE          = 1<<14,
	XWINDOW_EVENT_MASK_EXPOSURE              = 1<<15,
	XWINDOW_EVENT_MASK_VISIBILITY_CHANGE     = 1<<16,
	XWINDOW_EVENT_MASK_STRUCTURE_NOTIFY      = 1<<17,
	XWINDOW_EVENT_MASK_RESIZE_REDIRECT       = 1<<18,
	XWINDOW_EVENT_MASK_SUBSTRUCTURE_NOTIFY   = 1<<19,
	XWINDOW_EVENT_MASK_SUBSTRUCTURE_REDIRECT = 1<<20,
	XWINDOW_EVENT_MASK_FOCUS_CHANGE          = 1<<21,
	XWINDOW_EVENT_MASK_PROPERTY_CHANGE       = 1<<22,
	XWINDOW_EVENT_MASK_COLOURMAP_CHANGE      = 1<<23,
	XWINDOW_EVENT_MASK_OWNER_GRAB_BUTTON     = 1<<24,
};

enum xwindow_event_mask_action {
	XWINDOW_EVENT_MASK_ACTION_ADD,
	XWINDOW_EVENT_MASK_ACTION_REMOVE,
	XWINDOW_EVENT_MASK_ACTION_REPLACE,
};

#include <karuiwm/position.h>
#include <karuiwm/rectangle.h>
#include <karuiwm/size.h>
#include <stdbool.h>
#include <stdint.h>
#include <X11/Xlib.h>

struct xwindow {
	Window id;
	char *name, *icon_name;
	struct client *client;
	struct rectangle dimension;
	struct {
		struct size size;
		struct position position;
		bool has_position;
		int unsigned base_width, base_height, inc_width, inc_height;
		int unsigned min_width, min_height, max_width, max_height;
		struct size min_aspect, max_aspect;
	} sizehints;
	struct {
		int unsigned width;
		uint32_t colour;
	} border;
	bool destroyed;
	enum xwindow_class class;
	struct xwindow *transient_for;
	struct list *transients;
	enum xwindow_type type;
	struct {
		/* _NET_WM_STATE values supported by us */
		bool fullscreen;
		bool modal;
	} state;
	bool override_redirect;
	int long unsigned event_mask;
};

struct xwindow *xwindow_alloc(Window id, struct rectangle const *dim,
                              int long unsigned event_mask,
                              bool override_redirect, enum xwindow_class class);
void xwindow_free(struct xwindow *xwin);

int xwindow_apply_sizehints(struct xwindow const *xwin, struct size *size);
int xwindow_change_event_mask(struct xwindow *xwin, int long unsigned mask,
                              enum xwindow_event_mask_action ema);
int xwindow_handle_clientmessage(struct xwindow *xwin,
                                 XClientMessageEvent *xcmev);
int xwindow_handle_propertynotify(struct xwindow *xwin, XPropertyEvent *xpev);
bool xwindow_has_id(struct xwindow const *xwin, Window const *id);
bool xwindow_is_manageable(struct xwindow const *xwin);
bool xwindow_is_viewable(struct xwindow const *xwin);
int xwindow_map(struct xwindow *xwin);
int xwindow_move(struct xwindow *xwin, struct position pos);
int xwindow_moveresize(struct xwindow *xwin, struct rectangle dim,
                       bool apply_hints);
int xwindow_query_state(struct xwindow *xwin);
int xwindow_query_transience(struct xwindow *xwin);
int xwindow_query_type(struct xwindow *xwin);
int xwindow_resize(struct xwindow *xwin, struct size size, bool apply_hints);
int xwindow_set_border_colour(struct xwindow *xwin, uint32_t col);
int xwindow_set_border_width(struct xwindow *xwin, int unsigned width);
int xwindow_set_focus(struct xwindow *xwin, bool focus);
int xwindow_set_fullscreen(struct xwindow *xwin, bool fullscreen);
char const *xwindow_type_str(enum xwindow_type type);
int xwindow_unmap(struct xwindow *xwin);

#endif /* ndef _KWM_XWINDOW_H */
