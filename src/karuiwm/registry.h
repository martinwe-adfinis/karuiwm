#ifndef _KWM_REGISTRY_H
#define _KWM_REGISTRY_H

#include <karuiwm/action.h>
#include <karuiwm/registry_session.h>

int registry_init(void);
void registry_deinit(void);

struct registry_session *registry_connect(char const *name);
void registry_disconnect(struct registry_session *rs);

int registry_create_action(struct registry_session *rs, char const *name,
                           int (*func)(struct list **, struct list const *),
                           size_t nargs, enum value_type const *arg_types);
int registry_destroy_action(struct registry_session *rs, char const *name);
struct action *registry_find_action_by_name(char const *name);

#endif /* ndef _KWM_REGISTRY_H */
