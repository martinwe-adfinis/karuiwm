#include <karuiwm/buttonbind.h>
#include <karuiwm/action_call.h>
#include <karuiwm/buttonmap.h>
#include <karuiwm/value.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>

struct buttonbind *
buttonbind_alloc(struct button const *b, struct action_call *ac)
{
	struct buttonbind *bb;

	bb = memory_alloc(sizeof(struct buttonbind), "buttonbind structure");
	if (!bb)
		return NULL;

	bb->button = *b;
	bb->actioncall = ac;
	bb->actioncall->buttonbind = bb;
	bb->buttonmap = NULL;

	return bb;
}

void
buttonbind_free(struct buttonbind *bb)
{
	if (bb->buttonmap)
		FATAL_BUG("Attempt to free buttonbind %u+%u -> '%s' while still part of buttonmap '%s'",
		          bb->button.mod, bb->button.button,
		          bb->actioncall->name, bb->buttonmap->name);
	assert(bb->actioncall->buttonbind == bb);
	bb->actioncall->buttonbind = NULL;
	action_call_free(bb->actioncall);
	memory_free(bb);
}

int
buttonbind_execute(struct buttonbind const *bb, struct button_event const *bev)
{
	return action_call_invoke(bb->actioncall, bev, NULL);
}

bool
buttonbind_has_action(struct buttonbind const *bb, struct action const *a)
{
	return bb->actioncall->action == a;
}

bool
buttonbind_has_action_with_name(struct buttonbind const *bb, char const *name)
{
	return action_has_name(bb->actioncall->action, name);
}

bool
buttonbind_has_button(struct buttonbind const *bb, struct button const *b)
{
	return button_equals(&bb->button, b);
}
