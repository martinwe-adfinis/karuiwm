#include <karuiwm/bus_subscription.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

void const *const BUS_SUBSCRIPTION_DATA_MASK_ALL = "";

struct bus_subscription *
bus_subscription_alloc(struct bus_session *bs, struct bus_channel *ch,
                       void const *mask)
{
	struct bus_subscription *sub;

	sub = memory_alloc(sizeof(struct bus_subscription),
	                   "bus subscription struct");
	if (!sub)
		goto error_alloc;

	sub->session = bs;
	list_append(bs->subscriptions, sub);

	sub->channel = ch;
	list_append(ch->subscriptions, sub);

	sub->data_mask = mask;

	return sub;

 error_alloc:
	return NULL;
}

void
bus_subscription_free(struct bus_subscription *sub)
{
	list_remove(sub->channel->subscriptions, sub, NULL);
	list_remove(sub->session->subscriptions, sub, NULL);
	memory_free(sub);
}

bool
bus_subscription_has_channel(struct bus_subscription const *sub,
                             struct bus_channel const *ch)
{
	return sub->channel == ch;
}

bool
bus_subscription_matches_event(struct bus_subscription const *sub,
                               void const *data)
{
	return sub->data_mask == BUS_SUBSCRIPTION_DATA_MASK_ALL
	    || sub->data_mask == data;
}

int
bus_subscription_set_data_mask(struct bus_subscription *sub, void const *mask)
{
	sub->data_mask = mask;
	return 0;
}
