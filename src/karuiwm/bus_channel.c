#include <karuiwm/bus_channel.h>
#include <karuiwm/bus_subscription.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

struct bus_channel *
bus_channel_alloc(char const *name)
{
	struct bus_channel *ch;

	ch = memory_alloc(sizeof(struct bus_channel), "bus channel structure");
	if (!ch)
		goto error_alloc;

	ch->name = cstr_dup(name);
	if (!ch->name) {
		log_set("Could not copy name: %s", log_str());
		goto error_name;
	}

	ch->subscriptions = list_alloc();
	if (!ch->subscriptions) {
		log_set("Could not create list for subscriptions: %s",
		        log_str());
		goto error_subs;
	}

	return ch;

 error_subs:
	log_push();
	cstr_free(ch->name);
	log_pop();
 error_name:
	log_push();
	memory_free(ch);
	log_pop();
 error_alloc:
	return NULL;
}

void
bus_channel_free(struct bus_channel *ch)
{
	struct bus_subscription *sub;

	while (ch->subscriptions->size > 0) {
		sub = ch->subscriptions->elements[0];
		bus_subscription_free(sub); /* will remove itself from list */
	}
	list_free(ch->subscriptions);

	cstr_free(ch->name);
	memory_free(ch);
}

bool
bus_channel_has_name(struct bus_channel const *ch, char const *name)
{
	return cstr_equals(ch->name, name);
}

void
bus_channel_send(struct bus_channel const *ch, void *data)
{
	int unsigned i;
	struct bus_subscription *sub;

	LIST_FOR (ch->subscriptions, i, sub)
		if (bus_subscription_matches_event(sub, data))
			sub->session->callback(sub, data, sub->session->context);
}
