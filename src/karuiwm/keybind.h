#ifndef _KWM_KEYBIND_H
#define _KWM_KEYBIND_H

#include <karuiwm/key.h>

struct keybind {
	struct key key;
	struct action_call *actioncall;
	struct keymap *keymap; /* managed by keymap.c */
};

#include <karuiwm/action.h>
#include <stdbool.h>

struct keybind *keybind_alloc(struct key const *k, struct action_call *ac);
void keybind_free(struct keybind *kb);

int keybind_execute(struct keybind const *kb);
bool keybind_has_action(struct keybind const *kb, struct action const *k);
bool keybind_has_action_with_name(struct keybind const *kb, char const *name);
bool keybind_has_key(struct keybind const *kb, struct key const *k);

#endif /* ndef _KWM_KEYBIND_H */
