#ifndef _KVM_VALUE_H
#define _KVM_VALUE_H

enum value_type {
	VALUE_BOOLEAN,
	VALUE_INTEGER,
	VALUE_UNSIGNED_INTEGER,
	VALUE_FLOAT,
	VALUE_COLOUR,
	VALUE_POSITION,
	VALUE_STRING,
	VALUE_DIRECTION,
	VALUE_KEY,
	VALUE_ACTION_CALL,
	VALUE_BUTTON,
	VALUE_BUTTON_EVENT,
};

#include <karuiwm/button.h>
#include <karuiwm/button_event.h>
#include <karuiwm/direction.h>
#include <karuiwm/key.h>
#include <stdbool.h>
#include <stdint.h>

union value_data {
	bool b;
	int i;
	int unsigned ui;
	float f;
	uint32_t col;
	struct position pos;
	char const *str;
	enum direction dir;
	struct action_call *ac;
	struct key key;
	struct button button;
	struct button_event bev;
};

struct value {
	enum value_type type;
	union value_data data;
	void *mem;
};

#include <karuiwm/action.h>
#include <karuiwm/action_call.h>

struct value *value_alloc(enum value_type type, union value_data data);
struct value *value_alloc_action_call(struct action_call *ac);
struct value *value_alloc_action_call_reference(struct action_call *ac);
struct value *value_alloc_boolean(bool b);
struct value *value_alloc_button(struct button button);
struct value *value_alloc_button_event(struct button_event bev);
struct value *value_alloc_colour(uint32_t col);
struct value *value_alloc_direction(enum direction dir);
struct value *value_alloc_float(float f);
struct value *value_alloc_integer(int i);
struct value *value_alloc_key(struct key key);
struct value *value_alloc_position(struct position pos);
struct value *value_alloc_string(char const *str);
struct value *value_alloc_string_reference(char const *str);
struct value *value_alloc_unsigned_integer(int unsigned ui);
void value_free(struct value *val);

struct value *value_copy(struct value const *val);
struct value *value_parse(enum value_type, char const *valstr);
char const *value_type_string(enum value_type type);

#endif /* ndef _KVM_VALUE_H */
