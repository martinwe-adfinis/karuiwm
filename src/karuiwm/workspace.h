#ifndef _KWM_WORKSPACE_H
#define _KWM_WORKSPACE_H

#include <stdbool.h>

struct workspace {
	struct list *tiled;
	struct list *floating;
	struct client *selcli;
	char name[256];
	struct monitor *mon;
	struct layout *layout;
	bool focused;
	bool nofloat;
	struct bus_session *bus_session;
	struct list *bus_subscriptions;
};

#include <karuiwm/client.h>
#include <karuiwm/layout.h>

struct workspace *workspace_alloc(void);
void workspace_free(struct workspace *ws);

int workspace_attach_client(struct workspace *ws, struct client *c);
int workspace_compare_name(struct workspace *ws1, struct workspace *ws2);
void workspace_detach_client(struct workspace *ws, struct client *c);
void workspace_focus(struct workspace *ws, bool focus);
void workspace_focus_client(struct workspace *ws, struct client *c);
bool workspace_isempty(struct workspace *ws);
void workspace_raise_floating(struct workspace *ws, struct client *c);
void workspace_rename(struct workspace *ws, char const *name);
int workspace_set_floating(struct workspace *ws, struct client *c,
                           bool floating);
int workspace_set_fullscreen(struct workspace *ws, struct client *c,
                             bool fullscreen);
int workspace_set_layout(struct workspace *ws, enum layout_type type);
void workspace_show(struct workspace *ws, bool show);
int workspace_transfer_client(struct workspace *ws, struct client *c);

#endif /* ndef _KWM_WORKSPACE_H */
