#ifndef _KWM_BUS_SUBSCRIPTION_H
#define _KWM_BUS_SUBSCRIPTION_H

#include <stdbool.h>

extern void const *const BUS_SUBSCRIPTION_DATA_MASK_ALL;

struct bus_subscription {
	struct bus_session *session;
	struct bus_channel *channel;
	void const *data_mask;
};

#include <karuiwm/bus_channel.h>
#include <karuiwm/bus_session.h>

struct bus_subscription *bus_subscription_alloc(struct bus_session *bs,
                                                struct bus_channel *ch,
                                                void const *mask);
void bus_subscription_free(struct bus_subscription *sub);

bool bus_subscription_has_channel(struct bus_subscription const *sub,
                                  struct bus_channel const *ch);
bool bus_subscription_matches_event(struct bus_subscription const *sub,
                                    void const *data);
int bus_subscription_set_data_mask(struct bus_subscription *sub,
                                   void const *mask);

#endif /* ndef _KWM_BUS_SUBSCRIPTION_H */
