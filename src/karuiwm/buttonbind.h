#ifndef _KWM_BUTTONBIND_H
#define _KWM_BUTTONBIND_H

#include <karuiwm/button.h>

struct buttonbind {
	struct button button;
	struct action_call *actioncall;
	struct buttonmap *buttonmap; /* managed by buttonmap.c */
};

#include <karuiwm/action.h>
#include <karuiwm/button_event.h>
#include <stdbool.h>

struct buttonbind *buttonbind_alloc(struct button const *b,
                                    struct action_call *ac);
void buttonbind_free(struct buttonbind *bb);

int buttonbind_execute(struct buttonbind const *bb,
                       struct button_event const *bev);
bool buttonbind_has_action(struct buttonbind const *bb, struct action const *a);
bool buttonbind_has_action_with_name(struct buttonbind const *bb,
                                     char const *name);
bool buttonbind_has_button(struct buttonbind const *bb, struct button const *b);

#endif /* ndef _KWM_BUTTONBIND_H */
