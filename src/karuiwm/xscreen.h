#ifndef _KWM_XSCREEN_H
#define _KWM_XSCREEN_H

#include <karuiwm/rectangle.h>

struct xscreen {
	struct rectangle dimension;
};

struct xscreen *xscreen_alloc(struct rectangle dim);
void xscreen_free(struct xscreen *xscr);

int xscreen_moveresize(struct xscreen *xscr, struct rectangle dim);

#endif /* ndef _KWM_XSCREEN_H */
