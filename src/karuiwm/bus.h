#ifndef _KWM_BUS_H
#define _KWM_BUS_H

#include <karuiwm/bus_channel.h>
#include <karuiwm/bus_session.h>
#include <karuiwm/bus_subscription.h>

struct bus {
	struct bus_channel *client_destroy;
	struct bus_channel *client_dimension;
	struct bus_channel *client_state_dialog;
	struct bus_channel *client_state_fullscreen;
	struct bus_channel *client_transience;
	struct bus_channel *client_type;

	struct bus_channel *monitor_attach;
	struct bus_channel *monitor_detach;
	struct bus_channel *monitor_resize;
	struct bus_channel *monitor_focus;
	struct bus_channel *monitor_workspace;

	struct bus_channel *workspace_attach;
	struct bus_channel *workspace_detach;
	struct bus_channel *workspace_rename;
	struct bus_channel *workspace_layout;

	struct bus_channel *x_key_notify;
	struct bus_channel *x_mouse_notify;
	struct bus_channel *x_pointer_notify;
	struct bus_channel *x_screen_attach;
	struct bus_channel *x_screen_detach;
	struct bus_channel *x_screen_resize;
	struct bus_channel *x_window_notify_destroy;
	struct bus_channel *x_window_notify_expose;
	struct bus_channel *x_window_notify_focus;
	struct bus_channel *x_window_notify_state_fullscreen;
	struct bus_channel *x_window_notify_state_modal;
	struct bus_channel *x_window_notify_transience;
	struct bus_channel *x_window_notify_type;
	struct bus_channel *x_window_notify_unmap;
};

int bus_init(void);
void bus_deinit(void);

struct bus_session *bus_connect(char const *name,
                    void (*cb)(struct bus_subscription const *, void *, void *),
                    void *ctx);
void bus_disconnect(struct bus_session *bs);

extern struct bus bus;

#endif /* ndef _KWM_BUS_H */
