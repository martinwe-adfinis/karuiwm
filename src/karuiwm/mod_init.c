#include <karuiwm/mod_init.h>
#include <karuiwm/action_call.h>
#include <karuiwm/commandline.h>
#include <karuiwm/karuiwm.h>
#include <karui/cstr.h>
#include <karui/log.h>

static char const *_CLA_PATH = "init";
static char const *_INIT_PATH_DEFAULT = PREFIX "/share/" APPNAME "/init";

int
mod_init_init(void)
{
	struct action_call *ac;
	int retval;
	char const *path;
	char *cmd;

	path = commandline_get(_CLA_PATH);
	if (!path) {
		DEBUG("Commandline argument '%s' not set; falling back to default init path %s",
		      _CLA_PATH, _INIT_PATH_DEFAULT);
		path = _INIT_PATH_DEFAULT;
	}

	cmd = cstr_format("exec %s", path);
	if (!cmd) {
		log_set("Could not create inclusion action string: %s",
		        log_str());
		goto error_cstr_format;
	}

	ac = action_call_parse(cmd);
	if (!ac) {
		log_set("Could not create action call: %s", log_str());
		goto error_action_call;
	}

	retval = action_call_invoke(ac, NULL, NULL);
	if (retval < 0)
		log_set("Could not run the exec action: %s", log_str());

	log_push();
	action_call_free(ac);
	cstr_free(cmd);
	log_pop();

	return retval;

 error_action_call:
	log_push();
	cstr_free(cmd);
	log_pop();
 error_cstr_format:
	return -1;
}

void
mod_init_term(void)
{
	/* none */
}
