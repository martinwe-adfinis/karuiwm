#ifndef _KWM_CURSOR_H
#define _KWM_CURSOR_H

enum cursor_shape {
	CURSOR_SHAPE_NORMAL,
	CURSOR_SHAPE_MOVE,
	CURSOR_SHAPE_RESIZE_BOTTOM,
	CURSOR_SHAPE_RESIZE_BOTTOM_LEFT,
	CURSOR_SHAPE_RESIZE_BOTTOM_RIGHT,
	CURSOR_SHAPE_RESIZE_LEFT,
	CURSOR_SHAPE_RESIZE_RIGHT,
	CURSOR_SHAPE_RESIZE_TOP_LEFT,
	CURSOR_SHAPE_RESIZE_TOP,
	CURSOR_SHAPE_RESIZE_TOP_RIGHT,
	CURSOR_SHAPE_LAST,
};

int cursor_init(void);
void cursor_deinit(void);

int cursor_grab(enum cursor_shape shape);
int cursor_set(enum cursor_shape shape);
void cursor_ungrab(void);

#endif /* ndef _KWM_CURSOR_H */
