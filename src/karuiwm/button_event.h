#ifndef _KWM_BUTTON_EVENT_H
#define _KWM_BUTTON_EVENT_H

enum button_event_type {
	BUTTON_EVENT_CURRENT,    /* placeholder */
	BUTTON_PRESS,
	BUTTON_RELEASE,
	BUTTON_MOVE,
};

#include <karuiwm/button.h>
#include <karuiwm/position.h>

struct button_event {
	enum button_event_type type;
	struct button button;
	struct position position;
	struct client *client;
};

int button_event_parse(struct button_event *bev, char const *str);
int button_event_parse_type(enum button_event_type *evtype, char const *str);
char const *button_event_type_str(enum button_event_type bevtype);

extern struct position const BUTTON_POSITION_CURRENT;

#endif /* ndef _KWM_BUTTON_EVENT_H */
