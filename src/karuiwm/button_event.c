#include <karuiwm/button_event.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <limits.h>

struct position const BUTTON_POSITION_CURRENT = { INT_MIN, INT_MIN };

int
button_event_parse(struct button_event *bev, char const *str)
{
	if (!cstr_equals(str, "CURRENT")) {
		log_set("Cannot parse button event other than CURRENT");
		return -1;
	}

	bev->type = BUTTON_EVENT_CURRENT;
	bev->button.mod = 0;
	bev->button.button = 0;
	bev->position = BUTTON_POSITION_CURRENT;
	bev->client = NULL;
	return 0;
}

int
button_event_parse_type(enum button_event_type *evtype, char const *str)
{
	if (cstr_equals(str, "CURRENT")) {
		*evtype = BUTTON_EVENT_CURRENT;
	} else if (cstr_equals_caseless(str, "press")) {
		*evtype = BUTTON_PRESS;
	} else if (cstr_equals_caseless(str, "release")) {
		*evtype = BUTTON_RELEASE;
	} else if (cstr_equals_caseless(str, "move")) {
		*evtype = BUTTON_MOVE;
	} else {
		log_set("Unknown button event type: '%s'", str);
		return -1;
	}
	return 0;
}

char const *
button_event_type_str(enum button_event_type bevtype)
{
	switch (bevtype) {
	case BUTTON_EVENT_CURRENT: return "BUTTON_EVENT_CURRENT";
	case BUTTON_PRESS: return "BUTTON_PRESS";
	case BUTTON_RELEASE: return "BUTTON_RELEASE";
	case BUTTON_MOVE: return "BUTTON_MOVE";
	}
	FATAL_BUG("Unknown button event type %d", bevtype);
}
