#include <karuiwm/icon.h>
#include <karuiwm/xoutput.h>
#include <karuiwm/xserver.h>
#include <karui/log.h>
#include <karui/memory.h>

struct icon *
icon_alloc(int unsigned w, int unsigned h, uint64_t const *lines)
{
	struct icon *i;

	if (w > 64) {
		log_set("Icon width (%u) is larger than 64 pixels", w);
		return NULL;
	}
	if (w == 0 || h == 0) {
		log_set("Icon width (%u) or height (%u) is 0", w, h);
		return NULL;
	}

	i = memory_alloc(sizeof(struct icon), "icon structure");
	if (!i)
		goto error_alloc;

	i->w = w;
	i->h = h;
	i->lines = memory_dup(lines, h * sizeof(uint64_t), "array of pixel lines");
	if (!i->lines)
		goto error_lines;

	i->pixmap = XCreatePixmap(xserver.display, xserver.root->id, w, h,
	                          xoutput.screen_depth);

	return i;

 error_lines:
	log_push();
	memory_free(i);
	log_pop();
 error_alloc:
	return NULL;
}

void
icon_free(struct icon *i)
{
	XFreePixmap(xserver.display, i->pixmap);
	memory_free(i->lines);
	memory_free(i);
}

int
icon_draw(struct icon *i, uint32_t fg, uint32_t bg)
{
	int unsigned x, y;

	XSetForeground(xserver.display, xserver.graphics_context, bg);
	XFillRectangle(xserver.display, i->pixmap, xserver.graphics_context,
	               0, 0, i->w, i->h);
	XSetForeground(xserver.display, xserver.graphics_context, fg);
	for (y = 0; y < i->h; ++y) {
		i->lines[y] = i->lines[y];
		for (x = 0; x < i->w; ++x)
			if ((i->lines[y] >> (i->w - x - 1)) & 1)
				XDrawPoint(xserver.display, i->pixmap,
				           xserver.graphics_context,
				           (int signed) x, (int signed) y);
	}
	return 0;
}
