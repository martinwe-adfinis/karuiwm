#ifndef _KWM_MODULE_H
#define _KWM_MODULE_H

struct module_data {
	char *name;
	int (*init)(struct module_data *data);
	int (*deinit)(struct module_data *data);
	void *context;
};

struct module {
	char *path;
	void *so_handler;
	int (*announce)(struct module_data *data);
	struct module_data data;
};

struct module *module_alloc(char const *path, char const *name);
int module_init(struct module *mod);
int module_deinit(struct module *mod);
void module_free(struct module *mod);


#endif /* ndef _KWM_MODULE_H */
