#ifndef _KWM_SEAT_H
#define _KWM_SEAT_H

struct seat {
	struct list *monitors;
	struct monitor *selmon;
	struct {
		int unsigned scratchpad_margin;
		int unsigned client_border_width;
	} config;
};

#include <karuiwm/client.h>
#include <karuiwm/monitor.h>
#include <karuiwm/position.h>
#include <karuiwm/rectangle.h>

int seat_init(void);
void seat_deinit(void);

void seat_focus_monitor(struct monitor *mon);
struct monitor *seat_locate_point(struct position pos);
struct monitor *seat_locate_rectangle(struct rectangle r);
int seat_set_client_border_width(int unsigned client_border_width);
int seat_set_scratchpad_margin(int unsigned margin);

extern struct seat seat;

#endif /* ndef _KWM_SEAT_H */
