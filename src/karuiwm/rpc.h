#ifndef _KWM_RPC_H
#define _KWM_RPC_H

#include <karuiwm/rpc_client.h>

int rpc_init(void);
void rpc_deinit(void);

int rpc_clean_up(void);

#endif /* _KWM_RPC_H */
