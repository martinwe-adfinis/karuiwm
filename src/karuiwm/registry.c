#include <karuiwm/registry.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/utils.h>
#include <karuiwm/value.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>

struct _registryaction {
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;
};

static int _action_registry_list_actions(struct list **results, struct list const *args);
static int _action_registry_list_sessions(struct list **results, struct list const *args);

static struct list *_registry_sessions;
static struct registry_session *_introspection_session;

#define _(...) (enum value_type const[]) { __VA_ARGS__ }
static struct _registryaction const _registryactions[] = {
	{ "registry_list_all_actions",     _action_registry_list_actions,
	  0, NULL },
	{ "registry_list_sessions",        _action_registry_list_sessions,
	  0, NULL },
};
#undef _

int
registry_init(void)
{
	int unsigned i;

	_registry_sessions = list_alloc();
	if (!_registry_sessions) {
		log_set("Could not create registry sessions list: %s",
		        log_str());
		goto error_registry_sessions;
	}

	_introspection_session = registry_connect("registry");
	if (!_introspection_session) {
		log_set("Could not create registry introspection session: %s",
		        log_str());
		goto error_introspection;
	}

	for (i = 0; i < LENGTH(_registryactions); ++i) {
		if (registry_create_action(_introspection_session,
		                           _registryactions[i].name,
		                           _registryactions[i].func,
		                           _registryactions[i].nargs,
		                           _registryactions[i].arg_types) < 0)
		{
			log_set("Could not register action '%s': %s",
			        _registryactions[i].name, log_str());
			goto error_register_action;
		}
	}

	return 0;

 error_register_action:
	log_push();
	registry_disconnect(_introspection_session);
	log_pop();
	_introspection_session = NULL;
 error_introspection:
	log_push();
	list_free(_registry_sessions);
	log_pop();
 error_registry_sessions:
	return -1;
}

void
registry_deinit(void)
{
	registry_disconnect(_introspection_session);
	_introspection_session = NULL;
}

struct registry_session *
registry_connect(char const *name)
{
	struct registry_session *rs;

	rs = registry_session_alloc(name);
	if (!rs) {
		log_set("Could not create registry session: %s", log_str());
		goto error_session;
	}

	list_append(_registry_sessions, rs);

	return rs;

 error_session:
	return NULL;
}

void
registry_disconnect(struct registry_session *rs)
{
	list_remove(_registry_sessions, rs, NULL);
	registry_session_free(rs);
}

int
registry_create_action(struct registry_session *rs, char const *name,
                       int (*func)(struct list **, struct list const *),
                       size_t nargs, enum value_type const *arg_types)
{
	struct action *a;

	DEBUG("Creating action %s (%zu argument)", name, nargs);

	a = action_alloc(name, func, nargs, arg_types);
	if (!a) {
		log_set("Could not create action '%s': %s", name, log_str());
		goto error_action;
	}

	if (registry_session_add_action(rs, a) < 0) {
		log_set("Could not add action '%s' to registry session: %s",
		        name, log_str());
		goto error_session;
	}

	return 0;

 error_session:
	log_push();
	action_free(a);
	log_pop();
 error_action:
	return -1;
}

int
registry_destroy_action(struct registry_session *rs, char const *name)
{
	struct action *a;

	DEBUG("Removing action '%s'", name);
	a = registry_session_find_action_by_name(rs, name);
	if (!a) {
		log_set("Action '%s' not registered by this session", name);
		return -1;
	}

	registry_session_remove_action(rs, a);
	action_free(a);

	return 0;
}

struct action *
registry_find_action_by_name(char const *name)
{
	struct registry_session const *rs;
	struct action *a;
	int unsigned i;

	LIST_FOR (_registry_sessions, i, rs) {
		a = registry_session_find_action_by_name(rs, name);
		if (a)
			return a;
	}
	return NULL;
}

static int
_action_registry_list_actions(struct list **results, struct list const *args)
{
	struct list *action_names;
	int unsigned i, j;
	struct registry_session *rs;
	struct action *a;
	struct value *val;
	(void) args;

	if (!results)
		return 0;

	action_names = list_alloc();
	if (!action_names) {
		log_set("Could not create action names list: %s", log_str());
		goto error_list;
	}

	LIST_FOR (_registry_sessions, i, rs) {
		LIST_FOR (rs->actions, j, a) {
			val = value_alloc_string(a->name);
			if (!val) {
				log_set("Could not create string value for '%s': %s",
				        a->name, log_str());
				goto error_val;
			}
			list_append(action_names, val);
		}
	}

	*results = action_names;
	return (int) (*results)->size;

 error_val:
	log_push();
	list_clear(action_names, (list_delfunc) value_free);
	list_free(action_names);
	log_pop();
 error_list:
	return -1;
}

static int
_action_registry_list_sessions(struct list **results, struct list const *args)
{
	struct list *session_names;
	int unsigned i;
	struct registry_session *rs;
	struct value *val;
	(void) args;

	if (!results)
		return 0;

	session_names = list_alloc();
	if (!session_names) {
		log_set("Could not create session names list: %s", log_str());
		goto error_list;
	}

	LIST_FOR (_registry_sessions, i, rs) {
		val = value_alloc_string(rs->name);
		if (!val) {
			log_set("Could not create string value for '%s': %s",
			        rs->name, log_str());
			goto error_val;
		}
		list_append(session_names, val);
	}

	*results = session_names;
	return (int) (*results)->size;

 error_val:
	log_push();
	list_clear(session_names, (list_delfunc) value_free);
	list_free(session_names);
	log_pop();
 error_list:
	return -1;
}
