#ifndef _KARUIWM_WSMAP_H
#define _KARUIWM_WSMAP_H

#include <karuiwm/colourset.h>
#include <karuiwm/size.h>
#include <stdbool.h>

struct wsmap {
	struct wsmbox *target;
	bool active;
	int unsigned border_width;
	struct size size;
	struct colourset colour_focused;
	struct colourset colour_targeted;
	struct colourset colour_unfocused;
	struct xfont *xfont;
	struct list *boxes;
};

int wsmap_init(void);
void wsmap_deinit(bool restart);

extern struct wsmap wsmap;

#endif /* ndef _KARUIWM_WSMAP_H */
