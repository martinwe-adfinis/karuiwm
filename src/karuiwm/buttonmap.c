#include <karuiwm/buttonmap.h>
#include <karuiwm/action_call.h>
#include <karuiwm/input.h>
#include <karuiwm/xserver.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

static void _grab_button(struct button const *b);

struct buttonmap *
buttonmap_alloc(char const *name)
{
	struct buttonmap *bm;

	bm = memory_alloc(sizeof(struct buttonmap), "buttonmap structure");
	if (!bm)
		goto error_alloc;

	bm->name = cstr_dup(name);
	if (!bm->name) {
		log_set("Could not copy name for buttonmap: %s", log_str());
		goto error_name;
	}

	bm->buttonbinds = list_alloc();
	if (!bm->buttonbinds) {
		log_set("Could not create list for button bindings: %s",
		        log_str());
		goto error_buttonbinds;
	}
	bm->active_buttonbind = NULL;
	bm->active = false;

	return bm;

 error_buttonbinds:
	log_push();
	cstr_free(bm->name);
	log_pop();
 error_name:
	log_push();
	memory_free(bm);
	log_pop();
 error_alloc:
	return NULL;
}

void
buttonmap_free(struct buttonmap *bm)
{
	struct buttonbind *bb;
	int unsigned i;

	if (bm->active)
		FATAL_BUG("Attempt to free active buttonmap '%s'", bm->name);

	/* First remove the buttonbind's reference to the buttonmap, as it would
	 * otherwise complain upon deletion:
	 */
	LIST_FOR (bm->buttonbinds, i, bb)
		bb->buttonmap = NULL;
	list_clear(bm->buttonbinds, (list_delfunc) buttonbind_free);
	list_free(bm->buttonbinds);
	cstr_free(bm->name);

	memory_free(bm);
}

int
buttonmap_activate(struct buttonmap *bm)
{
	int unsigned i;
	struct buttonbind *bb;

	LIST_FOR (bm->buttonbinds, i, bb)
		_grab_button(&bb->button);
	bm->active = true;

	return 0;
}

int
buttonmap_add_buttonbind(struct buttonmap *bm, struct buttonbind *bb,
                         bool overwrite)
{
	struct buttonbind *conflicting_bb;
	struct button const *b = &bb->button;

	conflicting_bb = list_find(bm->buttonbinds, b,
	                           (list_eqfunc) buttonbind_has_button);
	if (conflicting_bb) {
		if (!overwrite) {
			log_set("Button %u+%u is already bound to '%s'",
			        conflicting_bb->button.mod,
			        conflicting_bb->button.button,
			        conflicting_bb->actioncall->name);
			return -1;
		}
		if (buttonmap_unbind_button(bm, &conflicting_bb->button) < 0) {
			log_set("Could not unbind existing button bind %u+%u -> '%s': %s",
			        conflicting_bb->button.mod,
			        conflicting_bb->button.button,
			        conflicting_bb->actioncall->name,
			        log_str());
			return -1;
		}
	}

	list_append(bm->buttonbinds, bb);
	bb->buttonmap = bm;
	if (bm->active)
		_grab_button(&bb->button);
	return 0;
}

int
buttonmap_deactivate(struct buttonmap *bm)
{
	if (!bm->active)
		FATAL_BUG("Attempt to deactive inactive buttonmap '%s'",
		          bm->name);

	XUngrabButton(xserver.display, AnyButton, AnyModifier,
	              xserver.root->id);

	bm->active = false;
	return 0;
}

bool
buttonmap_has_action(struct buttonmap const *bm, struct action const *a)
{
	return list_contains(bm->buttonbinds, a,
	                     (list_eqfunc) buttonbind_has_action);
}

bool
buttonmap_has_name(struct buttonmap const *bm, char const *name)
{
	return cstr_equals(bm->name, name);
}

int
buttonmap_resolve_button(struct buttonmap *bm,
                         struct button_event const *bev)
{
	struct buttonbind *bb;
	struct button const b = bev->button;
	int retval;

	/* check if we handle this button event */
	switch (bev->type) {
	case BUTTON_PRESS:
		if (bm->active_buttonbind) {
			/* already executing another buttonbind */
			DEBUG("Already executing buttonbind %u+%u -> '%s'",
			      bm->active_buttonbind->button.mod,
			      bm->active_buttonbind->button.button,
			      bm->active_buttonbind->actioncall->name);
			return 0;
		}
		break;
	case BUTTON_MOVE:
	case BUTTON_RELEASE:
		if (!bm->active_buttonbind)
			return 0;
		if (bev->type == BUTTON_RELEASE
		&& bev->button.button != bm->active_buttonbind->button.button) {
			DEBUG("Released button %d does not match executing buttonbind %u+%u -> '%s'",
			      bev->button.button,
			      bm->active_buttonbind->button.mod,
			      bm->active_buttonbind->button.button,
			      bm->active_buttonbind->actioncall->name);
			return 0;
		}
		break;
	case BUTTON_EVENT_CURRENT:
		FATAL_BUG("Invalid button event: BUTTON_EVENT_CURRENT");
	}

	/* determine buttonbind to execute */
	if (bm->active_buttonbind) {
		bb = bm->active_buttonbind;
	} else {
		bb = list_find(bm->buttonbinds, &b,
		               (list_eqfunc) buttonbind_has_button);
		if (!bb) {
			log_set("Button %u+%u is not bound to any action in keymap '%s'",
			        b.mod, b.button, bm->name);
			return -1;
		}
		DEBUG("Buttonbind %u+%u -> '%s'",
		      bb->button.mod, bb->button.button,
		      bb->actioncall->name);
	}

	/* execute buttonbind */
	retval = buttonbind_execute(bb, bev);

	/* update interactive buttonbind */
	if (bev->type == BUTTON_PRESS) {
		DEBUG("Button press; activating buttonbind");
		bm->active_buttonbind = bb;
	}
	if (bev->type == BUTTON_RELEASE) {
		DEBUG("Button release; deactivating buttonbind");
		bm->active_buttonbind = NULL;
	}

	return retval;
}

int
buttonmap_unbind_button(struct buttonmap *bm, struct button const *b)
{
	struct buttonbind *bb;

	DEBUG("Unbinding button %u+%u in buttonmap '%s'",
	      b->mod, b->button, bm->name);

	bb = list_find(bm->buttonbinds, b, (list_eqfunc) buttonbind_has_button);
	if (!bb) {
		log_set("Button %u+%u is not bound to any action in buttonmap '%s'",
		        b->mod, b->button, bm->name);
		return -1;
	}

	list_remove(bb->buttonmap->buttonbinds, bb, NULL);
	bb->buttonmap = NULL;
	buttonbind_free(bb);

	return 0;
}

static void
_grab_button(struct button const *b)
{
	XGrabButton(xserver.display, b->button, b->mod, xserver.root->id, False,
	            INPUT_MASK_BUTTON, GrabModeAsync, GrabModeAsync, None,
	            None);
}
