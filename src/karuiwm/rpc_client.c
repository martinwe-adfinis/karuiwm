#include <karuiwm/rpc_client.h>
#include <karuiwm/action_call.h>
#include <karuiwm/button_event.h>
#include <karuiwm/fdio.h>
#include <karuiwm/value.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define RPC_BUFSIZE 1024

static int _append_rpcmsg(char **rpcmsg, struct value const *val);
static char *_assemble_rpcmsg(int retval, char const *logstr,
                              struct list const *results);
static void _close_connection(struct rpc_client *c);
static int _fd_callback(int fd, void *data);
static int _invoke_actionline(struct rpc_client *c, char const *actionstr);
static int _process_input(struct rpc_client *c, char const *input);
static int _write_retmsg(struct rpc_client *c, int retval, char const *retmsg,
                         struct list const *results);

struct rpc_client *
rpc_client_alloc(int sockfd)
{
	struct rpc_client *c;

	c = memory_alloc(sizeof(struct rpc_client), "RPC client structure");
	if (!c)
		return NULL;

	c->name = cstr_format("RPC client %d", sockfd);
	if (!c->name) {
		log_set("Could not set name: %s", log_str());
		goto error_name;
	}

	c->fdio = fdio_register(c->name, _fd_callback, c);
	if (!c->fdio) {
		log_set("Could not register to FD I/O: %s", log_str());
		goto error_fdio_register;
	}

	c->fd = sockfd;
	if (fdio_add_fd(c->fdio, &c->fd) < 0) {
		log_set("Could not add socket file descriptor to I/O: %s",
		        log_str());
		goto error_fdio_add;
	}
	return c;

 error_fdio_add:
	log_push();
	fdio_unregister(c->fdio);
	log_pop();
 error_fdio_register:
	log_push();
	cstr_free(c->name);
	log_pop();
 error_name:
	return NULL;
}

void
rpc_client_free(struct rpc_client *c)
{
	if (c->fd >= 0)
		_close_connection(c);
	cstr_free(c->name);
	memory_free(c);
}

bool
rpc_client_has_fd(struct rpc_client const *c, int const *fd)
{
	return c->fd == *fd;
}

static int
_append_rpcmsg(char **rpcmsg, struct value const *val)
{
	char *rpcmsg_new = NULL;
	char const *valstr = NULL;

	switch (val->type) {
	case VALUE_BOOLEAN:
		valstr = val->data.b ? "true" : "false";
		rpcmsg_new = cstr_format("%s%s\n", *rpcmsg, valstr);
		break;
	case VALUE_INTEGER:
		rpcmsg_new = cstr_format("%s%d\n", *rpcmsg, val->data.i);
		break;
	case VALUE_UNSIGNED_INTEGER:
		rpcmsg_new = cstr_format("%s%u\n", *rpcmsg, val->data.ui);
		break;
	case VALUE_FLOAT:
		rpcmsg_new = cstr_format("%s%f\n", *rpcmsg, val->data.f);
		break;
	case VALUE_COLOUR:
		rpcmsg_new = cstr_format("%s%06X\n", *rpcmsg, val->data.col);
		break;
	case VALUE_POSITION:
		rpcmsg_new = cstr_format("%s%+d%+d\n", *rpcmsg,
		                         val->data.pos.x, val->data.pos.y);
		break;
	case VALUE_STRING:
		rpcmsg_new = cstr_format("%s%s\n", *rpcmsg, val->data.str);
		break;
	case VALUE_DIRECTION:
		rpcmsg_new = cstr_format("%s%s\n", *rpcmsg,
		                         direction_str(val->data.dir));
		break;
	case VALUE_KEY:
		rpcmsg_new = cstr_format("%s%u+%s\n", *rpcmsg,
		                         val->data.key.mod,
		                         XKeysymToString(val->data.key.sym));
		break;
	case VALUE_ACTION_CALL:
		/* TODO: print action call arguments along */
		rpcmsg_new = cstr_format("%s%s\n", *rpcmsg, val->data.ac->name);
		break;
	case VALUE_BUTTON:
		rpcmsg_new = cstr_format("%s%u+%u\n", *rpcmsg,
		                         val->data.button.mod,
		                         val->data.button.button);
		break;
	case VALUE_BUTTON_EVENT:
		/* We cannot represent a button event as string, but really it
		 * also shouldn't ever be returned as a result of an action call
		 * anyway.
		 */
		BUG("Attempt to append button event to RPC message");
		return -1;
	}


	if (!rpcmsg_new) {
		log_set("Could not extend RPC return message with %s: %s",
		        value_type_string(val->type), log_str());
		goto error_extend;
	}
	cstr_free(*rpcmsg);
	*rpcmsg = rpcmsg_new;
	return 0;

 error_extend:
	return -1;
}

static char *
_assemble_rpcmsg(int retval, char const *retmsg, struct list const *results)
{
	int unsigned i;
	struct value *val;
	char *rpcmsg;

	rpcmsg = cstr_format("%d %s\n", retval, retmsg);
	if (!rpcmsg) {
		log_set("Could not set RPC return message: %s", log_str());
		goto error_retmsg;
	}
	if (results) {
		LIST_FOR (results, i, val) {
			if (_append_rpcmsg(&rpcmsg, val) < 0) {
				log_set("Could not append result %u to RPC return message: %s",
				        i, log_str());
				goto error_resval;
			}
		}
	}
	return rpcmsg;

 error_resval:
	log_push();
	cstr_free(rpcmsg);
	log_pop();
 error_retmsg:
	return NULL;
}

static void
_close_connection(struct rpc_client *c)
{
	shutdown(c->fd, SHUT_RDWR);
	fdio_remove_fd(c->fdio, &c->fd);
	fdio_unregister(c->fdio);
	close(c->fd);
	c->fd = -1;
}

static int
_fd_callback(int fd, void *data)
{
	struct rpc_client *c;
	ssize_t nb;
	char buf[RPC_BUFSIZE];

	c = data;
	if (c->fd < 0)
		FATAL_BUG("fd == %d != %d", fd, c->fd);

	nb = read(c->fd, buf, RPC_BUFSIZE);
	if (nb <= 0) {
		if (nb == 0)
			DEBUG("RPC client %d closed connection", c->fd);
		else
			log_set("RPC client %d: could not read from connection: %s",
			        c->fd, strerror(errno));
		_close_connection(c);
		return nb == 0 ? 0 : -1;
	}
	buf[nb] = '\0';

	return _process_input(c, buf);
}

static int
_invoke_actionline(struct rpc_client *c, char const *line)
{
	struct list *results = NULL;
	struct action_call *ac;
	int retval;
	char const *retmsg;

	/* parse action string */
	ac = action_call_parse(line);
	if (!ac) {
		retval = -1;
		retmsg = log_str();
		goto error_parse;
	}

	/* run command */
	retval = action_call_invoke(ac, NULL, &results);
	retmsg = retval < 0 ? log_str() : "Success";

	/* clean up action call */
	log_push();
	action_call_free(ac);
	log_pop();

	/* process results */
 error_parse:
	log_push();
	if (_write_retmsg(c, retval, retmsg, results) < 0)
		ERROR("Could not write return message to RPC client: %s",
		      log_str());
	if (results) {
		list_clear(results, (list_delfunc) value_free);
		list_free(results);
	}
	log_pop();
	return retval;
}

static int
_process_input(struct rpc_client *c, char const *input)
{
	struct list *lines;
	char *line;
	int unsigned i;

	lines = cstr_split(input, "\n");
	if (!lines) {
		log_set("Could not split input into lines: %s", log_str());
		return -1;
	}

	LIST_FOR(lines, i, line)
		if (_invoke_actionline(c, line) < 0)
			INFO("Action invocation failed: %s -› %s",
			     line, log_str());

	log_push();
	list_clear(lines, (list_delfunc) cstr_free);
	list_free(lines);
	log_pop();

	return 0;
}

static int
_write_retmsg(struct rpc_client *c, int retval, char const *retmsg,
              struct list const *results)
{
	char *rpcmsg;
	size_t msglen;
	ssize_t wlen;

	/* assemble message */
	rpcmsg = _assemble_rpcmsg(retval, retmsg, results);
	if (!rpcmsg) {
		log_set("Could not assemble RPC return message: %s",
		        log_str());
		goto error_assemble;
	}

	/* write out message */
	msglen = strlen(rpcmsg) + 1;
	for (wlen = 0;
	     wlen >= 0 && (size_t) wlen < msglen;
	     wlen += write(c->fd, rpcmsg + wlen, msglen - (size_t) wlen));
	if (wlen < 0) {
		log_set("Could not write return message (retval=%d, %zu results) to RPC client: %s",
		        retval, results ? results->size : 0, strerror(errno));
		goto error_write;
	}

	cstr_free(rpcmsg);
	return 0;

 error_write:
	log_push();
	cstr_free(rpcmsg);
	log_pop();
 error_assemble:
	return -1;
}
