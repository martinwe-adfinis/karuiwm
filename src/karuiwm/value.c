#include <karuiwm/value.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>

static struct value *_alloc(enum value_type type, union value_data data);
static struct value *_alloc_memcpy(enum value_type type, void const *mem);
static struct value *_alloc_memtake(enum value_type type, void *mem);

struct value *
value_alloc(enum value_type type, union value_data data)
{
	return _alloc(type, data);
}

struct value *
value_alloc_action_call(struct action_call *ac)
{
	return _alloc_memcpy(VALUE_ACTION_CALL, ac);
}

struct value *
value_alloc_action_call_reference(struct action_call *ac)
{
	return _alloc(VALUE_ACTION_CALL,
	              (union value_data) { .ac = ac });
}

struct value *
value_alloc_boolean(bool b)
{
	return _alloc(VALUE_BOOLEAN,
	              (union value_data) { .b = b });
}

struct value *
value_alloc_button(struct button button)
{
	return _alloc(VALUE_BUTTON,
	              (union value_data) { .button = button });
}

struct value *
value_alloc_button_event(struct button_event bev)
{
	return _alloc(VALUE_BUTTON_EVENT,
	              (union value_data) { .bev = bev });
}

struct value *
value_alloc_colour(uint32_t col)
{
	return _alloc(VALUE_COLOUR,
	              (union value_data) { .col = col });
}

struct value *
value_alloc_direction(enum direction dir)
{
	return _alloc(VALUE_DIRECTION,
	              (union value_data) { .dir = dir });
}

struct value *
value_alloc_float(float f)
{
	return _alloc(VALUE_FLOAT,
	              (union value_data) { .f = f });
}

struct value *
value_alloc_integer(int i)
{
	return _alloc(VALUE_INTEGER,
	              (union value_data) { .i = i });
}

struct value *
value_alloc_key(struct key key)
{
	return _alloc(VALUE_KEY,
	              (union value_data) { .key = key });
}

struct value *
value_alloc_position(struct position pos)
{
	return _alloc(VALUE_POSITION,
	              (union value_data) { .pos = pos });
}

struct value *
value_alloc_string(char const *str)
{
	return _alloc_memcpy(VALUE_STRING, str);
}

struct value *
value_alloc_string_reference(char const *str)
{
	return _alloc(VALUE_STRING,
	              (union value_data) { .str = str });
}

struct value *
value_alloc_unsigned_integer(int unsigned ui)
{
	return _alloc(VALUE_UNSIGNED_INTEGER,
	              (union value_data) { .ui = ui });
}

void
value_free(struct value *val)
{
	if (val->mem) {
		switch (val->type) {
		case VALUE_STRING:
			cstr_free(val->mem);
			break;
		case VALUE_ACTION_CALL:
			action_call_free(val->mem);
			break;
		default:
			BUG("Unhandled memory type in value_free: %d (%s)",
			    val->type, value_type_string(val->type));
			memory_free(val->mem);
		}
		val->mem = NULL;
	}
	memory_free(val);
}

struct value *
value_copy(struct value const *val)
{
	if (val->mem)
		return _alloc_memcpy(val->type, val->mem);
	else
		return _alloc(val->type, val->data);
}

struct value *
value_parse(enum value_type type, char const *str)
{
	union value_data data;
	void *mem;

	switch (type) {
	case VALUE_BOOLEAN:
		if (cstr_parse_bool(&data.b, str) < 0)
			return NULL;
		return value_alloc_boolean(data.b);
	case VALUE_INTEGER:
		if (cstr_parse_int(&data.i, str, NULL) < 0)
			return NULL;
		return value_alloc_integer(data.i);
	case VALUE_UNSIGNED_INTEGER:
		if (cstr_parse_int_unsigned(&data.ui, str, NULL) < 0)
			return NULL;
		return value_alloc_unsigned_integer(data.ui);
	case VALUE_FLOAT:
		if (cstr_parse_float(&data.f, str) < 0)
			return NULL;
		return value_alloc_float(data.f);
	case VALUE_COLOUR:
		if (cstr_parse_colour(&data.col, str) < 0)
			return NULL;
		return value_alloc_colour(data.col);
	case VALUE_POSITION:
		if (position_parse(&data.pos, str) < 0)
			return NULL;
		return value_alloc_position(data.pos);
	case VALUE_STRING:
		return _alloc_memcpy(VALUE_STRING, str);
	case VALUE_DIRECTION:
		if (direction_parse(&data.dir, str) < 0)
			return NULL;
		return value_alloc_direction(data.dir);
	case VALUE_ACTION_CALL:
		mem = action_call_parse(str);
		if (!mem)
			return NULL;
		return _alloc_memtake(VALUE_ACTION_CALL, mem);
	case VALUE_KEY:
		if (key_parse(&data.key, str) < 0)
			return NULL;
		return value_alloc_key(data.key);
	case VALUE_BUTTON:
		if (button_parse(&data.button, str) < 0)
			return NULL;
		return value_alloc_button(data.button);
	case VALUE_BUTTON_EVENT:
		if (button_event_parse(&data.bev, str) < 0)
			return NULL;
		return value_alloc_button_event(data.bev);
	}
	assert(false);
}

char const *
value_type_string(enum value_type type)
{
	switch (type) {
	case VALUE_BOOLEAN: return "VALUE_BOOLEAN";
	case VALUE_INTEGER: return "VALUE_INTEGER";
	case VALUE_UNSIGNED_INTEGER: return "VALUE_UNSIGNED_INTEGER";
	case VALUE_FLOAT: return "VALUE_FLOAT";
	case VALUE_COLOUR: return "VALUE_COLOUR";
	case VALUE_POSITION: return "VALUE_POSITION";
	case VALUE_STRING: return "VALUE_STRING";
	case VALUE_DIRECTION: return "VALUE_DIRECTION";
	case VALUE_KEY: return "VALUE_KEY";
	case VALUE_ACTION_CALL: return "VALUE_ACTION_CALL";
	case VALUE_BUTTON: return "VALUE_BUTTON";
	case VALUE_BUTTON_EVENT: return "VALUE_BUTTON_EVENT";
	}

	BUG("Attempt to stringify invalid value type: %d", type);
	return NULL;
}

static struct value *
_alloc(enum value_type type, union value_data data)
{
	struct value *val;

	val = memory_alloc(sizeof(struct value), "value structure");
	if (!val)
		return NULL;

	val->type = type;
	val->data = data;
	val->mem = NULL;
	return val;
}

static struct value *
_alloc_memcpy(enum value_type type, void const *mem)
{
	void *nmem;

	switch (type) {
	case VALUE_STRING:
		nmem = cstr_dup(mem);
		if (!nmem) {
			log_set("Could not duplicate string from input data: %s",
			        log_str());
			goto error_dup;
		}
		break;
	case VALUE_ACTION_CALL:
		nmem = action_call_copy(mem);
		if (!nmem) {
			log_set("Could not duplicate action call from input data: %s",
			        log_str());
			goto error_dup;
		}
		break;
	default:
		BUG("value::_alloc_memcpy() for unsupported value type %d (%s)",
		    type, value_type_string(type));
		goto error_type;
	}

	return _alloc_memtake(type, nmem);

 error_dup:
	return NULL;

 error_type:
	return NULL;
}

static struct value *
_alloc_memtake(enum value_type type, void *mem)
{
	struct value *val;

	val = memory_alloc(sizeof(struct value), "value structure");
	if (!val)
		goto error_mem;

	val->type = type;
	switch (type) {
	case VALUE_STRING:
		val->data.str = mem;
		break;
	case VALUE_ACTION_CALL:
		val->data.ac = mem;
		break;
	default:
		BUG("value::_alloc_memtake() for unsupported value type %d (%s)",
		    type, value_type_string(type));
	}
	val->mem = mem;

	return val;

 error_mem:
	return NULL;
}
