#ifndef _KWM_RECTANGLE_H
#define _KWM_RECTANGLE_H

#include <karuiwm/position.h>
#include <karuiwm/size.h>

enum rectangle_area {
	RECT_BOTTOM,
	RECT_BOTTOM_LEFT,
	RECT_BOTTOM_RIGHT,
	RECT_CENTRE,
	RECT_LEFT,
	RECT_RIGHT,
	RECT_TOP,
	RECT_TOP_LEFT,
	RECT_TOP_RIGHT,
};

struct rectangle {
	union {
		struct position pos;
		struct {
			int x;
			int y;
		};
	};
	union {
		struct size size;
		struct {
			int unsigned width;
			int unsigned height;
		};
	};
};

#include <stdbool.h>

int rectangle_centre(struct rectangle outer, struct rectangle *inner);
bool rectangle_contains_point(struct rectangle r, struct position pos);
bool rectangle_equals(struct rectangle const *r1, struct rectangle const *r2);
int rectangle_fit(struct rectangle outer, struct rectangle *inner);
int rectangle_get_quadrant(struct rectangle r, struct position pos,
                           enum rectangle_area *area);
int rectangle_get_ninth(struct rectangle r, struct position pos,
                        enum rectangle_area *area);
int unsigned rectangle_intersection(struct rectangle r1, struct rectangle r2);

#endif /* ndef _KWM_GEOMETRY_H */
