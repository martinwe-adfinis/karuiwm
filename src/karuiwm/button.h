#ifndef _KWM_BUTTON_H
#define _KWM_BUTTON_H

struct button {
	int unsigned mod;
	int unsigned button;
};

#include <stdbool.h>

bool button_equals(struct button const *b1, struct button const *b2);
int button_parse(struct button *b, char const *str);
int button_parse_button(int unsigned *button, char const *str);

#endif /* ndef _KWM_BUTTON_H */
