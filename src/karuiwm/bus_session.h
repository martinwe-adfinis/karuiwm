#ifndef _KWM_BUS_SESSION_H
#define _KWM_BUS_SESSION_H

struct bus_subscription;

struct bus_session {
	char *name;
	void *context;
	void (*callback)(struct bus_subscription const *, void *, void *);
	struct list *subscriptions;
};

#include <karuiwm/bus_channel.h>
#include <stdbool.h>

struct bus_session *bus_session_alloc(char const *name,
                    void (*cb)(struct bus_subscription const *, void *, void *),
                    void *ctx);
void bus_session_free(struct bus_session *bs);

int bus_session_join_channel(struct bus_session *bs,
                             struct bus_channel *ch, void *mask);
void bus_session_leave_channel(struct bus_session *bs, struct bus_channel *ch);

#endif /* ndef _KWM_BUS_SESSION_H */
