#ifndef _KWM_ICON_H
#define _KWM_ICON_H

#include <stdint.h>
#include <X11/Xlib.h>

struct icon {
	int unsigned w, h;
	uint64_t *lines;
	Pixmap pixmap;
};

struct icon *icon_alloc(int unsigned w, int unsigned h, uint64_t const *lines);
void icon_free(struct icon *i);

int icon_draw(struct icon *i, uint32_t fg, uint32_t bg);

#endif /* ndef _KWM_ICON_H */
