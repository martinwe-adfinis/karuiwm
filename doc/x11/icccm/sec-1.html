<HTML>
<HEAD>
<TITLE>ICCCM - Introduction</TITLE>
</HEAD>

<BODY>
<H1><A NAME="s-1">1.</A>  Introduction</H1>
It was an explicit design goal of X Version 11 to specify mechanism,
not policy.
As a result,  
a client that converses with the server using the protocol defined 
by the <I>X Window System Protocol</I>, <I>Version 11</I> may operate correctly 
in isolation but may not coexist properly with others sharing the same server.
<P>
Being a good citizen in the X Version 11 world involves adhering to
conventions that govern inter-client communications in the following areas:
<UL>
<LI> Selection mechanism
<LI> Cut buffers
<LI> Window manager
<LI> Session manager
<LI> Manipulation of shared resources
<LI> Device color characterization
</UL>
This document proposes suitable conventions without attempting to enforce 
any particular user interface.
To permit clients written in different languages to communicate,
these conventions are expressed solely in terms of protocol operations,
not in terms of their associated Xlib interfaces,
which are probably more familiar.
The binding of these operations to the Xlib interface for C
and to the equivalent interfaces for other languages
is the subject of other documents.

<H2><A NAME="s-1.1">1.1.</A> Evolution of the Conventions</H2>
In the interests of timely acceptance,
the <I>Inter-Client Communication Conventions Manual</I> (ICCCM)
covers only a minimal set of required conventions.
These conventions will be added to and updated as appropriate,
based on the experiences of the X Consortium.
<P>
As far as possible,
these conventions are upwardly compatible with those in the February 25, 1988,
draft that was distributed with the X Version 11, Release 2 of the software.
In some areas,
semantic problems were discovered with those conventions,
and, thus, complete upward compatibility could not be assured.
These areas are noted in the text and are summarized in <A HREF="sec-A.html#s-A">Appendix A</A>.
<P>
In the course of developing these conventions,
a number of minor changes to the protocol were identified as desirable.
They also are identified in the text, are summarized in <A HREF="sec-B.html#s-B">Appendix B</A>,
and are offered as input to a future protocol revision process.
If and when a protocol revision incorporating these changes is undertaken,
it is anticipated that the ICCCM will need to be revised.
Because it is difficult to ensure that clients and servers are upgraded
simultaneously, 
clients using the revised conventions should examine the minor protocol 
revision number and be prepared to use the older conventions 
when communicating with an older server.
<P>
It is expected that these revisions will ensure that clients using 
the conventions appropriate to protocol minor revision <I>n</I> 
will interoperate correctly with those that use the conventions 
appropriate to protocol minor revision <I>n</I>+1 if the server supports both.

<H2><A NAME="s-1.2">1.2.</A>  Atoms</H2>
Many of the conventions use atoms.
To assist the reader,
the following sections attempt to amplify the description of atoms 
that is provided in the protocol specification.

<H3><A NAME="s-1.2.1">1.2.1.</A> What Are Atoms?</H3>
At the conceptual level, 
atoms are unique names that clients can use to communicate information 
to each other.
They can be thought of as a bundle of octets,
like a string but without an encoding being specified.
The elements are not necessarily ASCII characters,
and no case folding happens.<A HREF="footnotes.html#f-1"><SUP>1</SUP></A>

<P>
The protocol designers felt that passing these
sequences of bytes back and forth across the wire would be too costly.
Further, they thought it important that events 
as they appear on the wire have a fixed size (in fact, 32 bytes)
and that because some events contain atoms, a fixed-size representation 
for them was needed.
<P>
To allow a fixed-size representation,
a protocol request 
<B>( InternAtom )</B>
was provided to register a byte sequence with the server,
which returns a 32-bit value (with the top three bits zero) 
that maps to the byte sequence.
The inverse operator is also available 
<B>( GetAtomName ).</B>

<H3><A NAME="s-1.2.2">1.2.2.</A> Predefined Atoms</H3>
The protocol specifies a number of atoms as being predefined:
<BLOCKQUOTE>
Predefined atoms are not strictly necessary
and may not be useful in all environments,
but they will eliminate many 
<B>InternAtom</B>
requests in most applications.
Note that they are predefined only in the sense of having numeric values, 
not in the sense of having required semantics.
</BLOCKQUOTE>
Predefined atoms are an implementation trick to avoid the cost of interning
many of the atoms that are expected to be used during the startup phase 
of all applications.
The results of the 
<B>InternAtom </B>
requests, which require a handshake, can be assumed <I>a priori</I>.
<P>
Language interfaces should probably cache the atom-name mappings 
and get them only when required.
The CLX interface, for instance, makes no distinction between predefined atoms
and other atoms; all atoms are viewed as symbols at the interface.
However, a CLX implementation will typically keep a symbol or atom cache 
and will typically initialize this cache with the predefined atoms.

<H3><A NAME="s-1.2.3">1.2.3.</A> Naming Conventions</H3>
The built-in atoms are composed of uppercase ASCII characters with the
logical words separated by an underscore character (_), for example,  
WM_ICON_NAME.
The protocol specification recommends that atoms used 
for private vendor-specific reasons should begin with an underscore.
To prevent conflicts among organizations, 
additional prefixes should be chosen 
(for example,  _DEC_WM_DECORATION_GEOMETRY).
<P>
The names were chosen in this fashion to make it easy to use them in a
natural way within LISP.
Keyword constructors allow the programmer to specify the atoms as LISP atoms.
If the atoms were not all uppercase,
special quoting conventions would have to be used.

<H3><A NAME="s-1.2.4">1.2.4.</A>  Semantics</H3>
The core protocol imposes no semantics on atoms except as they are used in
FONTPROP structures.
For further information on FONTPROP semantics,
see the <I>X Logical Font Description Conventions</I>.

<H3><A NAME="s-1.2.5">1.2.5.</A> Name Spaces</H3>
The protocol defines six distinct spaces in which atoms are interpreted.
Any particular atom may or may not have some valid interpretation
with respect to each of these name spaces.
<TABLE>
<TR><TH COLSPAN=3><HR>
<TR><TH ALIGN=left>Space	<TH ALIGN=left>Briefly	<TH ALIGN=left>Examples
<TR><TH COLSPAN=3><HR>
<TR><TD>Property name	<TD>Name	<TD>WM_HINTS, WM_NAME, RGB_BEST_MAP, ...
<TR><TD>Property type	<TD>Type	<TD>WM_HINTS, CURSOR, RGB_COLOR_MAP, ...
<TR><TD>Selection name	<TD>Selection	<TD>PRIMARY, SECONDARY, CLIPBOARD
<TR><TD>Selection target	<TD>Target	<TD>FILE_NAME, POSTSCRIPT, PIXMAP, ...
<TR><TD>Font property	<TD>	<TD>QUAD_WIDTH, POINT_SIZE, ...
<TR><TD><B>ClientMessage</B> type	<TD>	<TD>WM_SAVE_YOURSELF, _DEC_SAVE_EDITS, ...
<TR><TH COLSPAN=3><HR>
</TABLE>


<H3><A NAME="s-1.2.6">1.2.6.</A> Discriminated Names</H3>
Sometimes a protocol requires there to be an arbitrary number of similar
objects which need unique names (usually because the objects are created
dynamically, so that names cannot be invented in advance). For example, a
colormap-generating program might use the selection mechanism to offer
colormaps for each screen, and so needs a selection name for each screen.
Such names are called &quot;discriminated names&quot; and are discriminated by
some entity. This entity can be:
<PRE><CODE>
    a screen
    an X resource (a window, a colormap, a visual, etc.)
    a client
</CODE></PRE>
<P>
If it is only necessary to generate a fixed set of names for each value
of the discriminating entity, then the discriminated names are formed by
suffixing an ordinary name according to the value of the entity.
<P>
If <I>name</I> is a descriptive portion for the name, <I>d</I> is a decimal
number with no leading zeroes, and <I>x</I> is a hexadecimal number with
exactly 8 digits, and using uppercase letters, then such discriminated names
shall have the form:
<TABLE>
<TR><TH COLSPAN=3><HR>
<TR><TH ALIGN=left>Name Discriminated By	<TH ALIGN=left>Form	<TH ALIGN=left>Example
<TR><TH COLSPAN=3><HR>
<TR><TD>screen number	<TD><I>name</I>_S<I>d</I>	<TD>WM_COMMS_S2
<TR><TD>X resource	<TD><I>name</I>_R<I>x</I>	<TD>GROUP_LEADER_R1234ABCD
<TR><TH COLSPAN=3><HR>
</TABLE>

<P>
To discriminate a name by client, use an X resource ID created by that
client.  This resource can be of any type.
<P>
Sometimes it is simply necessary to generate a unique set of names (for
example, for the properties on a window used by a MULTIPLE selection).
These names should have the form:
<PRE><CODE>
	U<I>d</I>		(e.g. U0 U1 U2 U3 ...)
</CODE></PRE>
<P>
if the names stand totally alone, and the form:
<PRE><CODE>
	<I>name</I>_U<I>d</I>	(e.g. FOO_U0  BAR_U0  FOO_U1  BAR_U1  ...)
</CODE></PRE>
<P>
if they come in sets (here there are two sets, named &quot;FOO&quot; and
&quot;BAR&quot;).  The stand-alone U<I>d</I> form should only be used if it is
clear that the module using it has complete control over the relevant
namespace, or has the active cooperation of all other entities which might
also use these names. (Naming properties on a window created specifically
for a particular selection is such a use; naming properties on the root
window is almost certainly not.)
<P>
In a particularly difficult case, it might be necessary to combine both
forms of discrimination. If this happens, the U form should come after
the other form, thus:
<PRE><CODE>
	FOO_R12345678_U23
</CODE></PRE>
<P ALIGN=center> Rationale
<BLOCKQUOTE>
<P>Existing protocols will not be changed to use these naming conventions,
because doing so will cause too much disruption.  However, it is expected
that future protocols &emdash; both standard and private &emdash; will use these
conventions.
</BLOCKQUOTE>

<HR>
<A HREF="./"><IMG SRC="/images/up.gif" WIDTH=31 HEIGHT=31 ALT="^"></A><A HREF="sec-2.html"><IMG SRC="/images/right.gif" WIDTH=31 HEIGHT=31 ALT=">"></A>
<P>
<ADDRESS><A HREF="http://tronche.com/">Christophe Tronche</A>, <A HREF="mailto:ch.tronche@computer.org">ch.tronche@computer.org</A></ADDRESS>
</BODY>
</HTML>
