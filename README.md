karuiwm
=======

Karuiwm is a lightweight, dynamically tiling window manager for X.

The original codebase was heavily inspired by [dwm](http://dwm.suckless.org/),
but in the meantime has deviated quite fundamentally.

The workspace map is inspired by a misinterpretation of the
[GridSelect](https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-GridSelect.html)
extension for XMonad.


Building and Installing
-----------------------

### Requirements

To build karuiwm, you need a C compiler with C11 support (recent versions of GCC
and Clang should cover that), [Meson](https://mesonbuild.com/) and
[Ninja](https://ninja-build.org/).

Additionally, you need Xlib (e.g. `libx11`) and Xinerama (e.g. `libxinerama`)
for karuiwm's runtime dependencies, and GNU Readline (e.g. `libreadline`) for
karuirpc's runtime dependencies.

Most commonly used Linux distributions should provide packages for all of them.

### Building

First, generate a build directory with the Ninja files:

	meson setup /path/to/builddir

Change into the build directory:

	cd /path/to/builddir

Check the build configuration with `meson configure`, or adapt some of the
settings (the defaults should be sane enough for quick testing, but you may want
to adapt some of the values e.g. for distro-packaging):

	meson configure -Db_sanitize=address
	meson configure -Doptimization=3
	meson configure -Dprefix=/usr

Once configured, build karuiwm and karuirpc as follows:

	ninja

This should result in two executable binary files, `karuiwm` and `karuirpc`, and
a couple of shared object files (`mod_[…].so`) in the build directory.

### Testing

There is [test/xephyr.sh](test/xephyr.sh) for launching karuiwm inside Xephyr.
It can be invoked by running the following command in the build directory:

    ninja xephyr

### Installing

The following command installs the built karuiwm, modules, karuirpc, karuimenu
and default configuration to the system, into the corresponding directories:

    meson install

For staged installations (e.g. when building a package), pass `--destdir`:

    meson install --destdir=path/to/staging/dir

Runtime dependencies here are:

 * Xlib and Xinerama for `karuiwm`.
 * POSIX-compatible `/bin/sh` and `test`/`[` (usually shell-builtin) for all the
   scripts.
 * POSIX-compatible `cat`, `printf` and `sort`, and `dmenu` for
   `karuimenu`.
 * XDG-compliant runtime (i.e. with `XDG_RUNTIME_DIR` and everything).

For a questionable example/reference, see also the [zuepfe
PKGBUILD](https://gitlab.com/ayekat/PKGBUILDs/tree/karuiwm-git).


Usage
-----

### Launching

Follow the standard procedure for launching a WM: Either configure your
`~/.xinitrc` as needed (for `xinit`/`startx`), or use your display manager to
launch karuiwm. Packagers may want to install something like this as
`/usr/share/xsessions/karuiwm.desktop`:

```dosini
[Desktop Entry]
Name=karuiwm
Comment=lightweight dynamically tiling window manager for X
Exec=/usr/bin/karuiwm
TryExec=/usr/bin/karuiwm
Type=Application
```

### Key and button bindings

By default, the keybindings in `PREFIX/share/karuiwm/init.kwm` are active (`Mod`
is `Mod1`/Alt):

**Applications**

 * `Mod`+`n`
   launch xterm
 * `Mod`+`p`
   launch dmenu\_run
 * `PrtSc`
   launch scrot (screenshot)

**Hardware**

 * `VolUp`|`VolDown`
   increase/decrease default PulseAudio sink volume level by 2%
 * `VolMute`|`MicMute`
   toggle default PulseAudio sink/source volume (mute/unmute)

**Clients**

 * `Mod`+`j`|`k`
   set focus to next/previous client
 * `Mod`+`t`
   toggle floating for selected client
 * `Mod`+`Shift`+`c`
   close selected client

**Layout** (see section [Layouts](#Layouts))

 * `Mod`+`l`|`h`
   increase/decrease master area size
 * `Mod`+`Shift`+`j`|`k`
   swap client with next/previous client in the layout
 * `Mod`+`,`|`.`
   increase/decrease number of clients in the master area
 * `Mod`(+`Shift`)+`Space`
   select next (previous) layout
 * `Mod`+`Return`
   move selected client to master area

**Workspaces** (see concept section [Workspaces](#Workspaces))

 * `Mod`+`Ctrl`+`h`|`j`|`k`|`l`
   set view to left/below/above/right workspace
 * `Mod`+`Ctrl`+`Shift`+`h`|`j`|`k`|`l`
   move and follow client to left/below/above/right workspace
 * `Mod`+`i`
   switch to a workspace by name
 * `Mod`+`Shift`+`i`
   send selected client to a workspace by name
 * `Mod`+`Shift`+`Ctrl`+`i`
   send and follow selected client to a workspace by name
 * `Mod`+`r`
   rename current workspace
 * `Mod`+`o`
   open workspace map

**Scratchpad** (see concept section [Scratchpad](#Scratchpad))

 * `Mod`+`Tab`
   toggle scratchpad
 * `Mod`+`Shift`+`Tab`
   set focused client as scratchpad, or unset scratchpad if it is focused

**Monitors** (see concept section [Xinerama](#Xinerama-aka-multi-monitor))

 * `Mod`+`m`
   set focus to next monitor

**Session**

 * `Mod`+`q`
   restart karuiwm
 * `Mod`+`Shift`+`q`
   quit karuiwm

**Mouse**

 * `Mod`+`Button1`
   grab and move client, make it floating
 * `Mod`+`Button3`
   resize client, make it floating

### RPC

Karuiwm exposes a Unix socket that allows external components to interact with
karuiwm by invoking so-called *actions*. A client tool, `karuirpc` is provided
for convenient usage of the RPC socket.

The list of available actions can be obtained by invoking
`registry_list_all_actions`.

Documentation on the available actions will follow. For now, check
`statusbar.c`, `useraction.c` and `wsmap.c` (i.e. all files where
`registry_create_action` is invoked at some point). Most of the essential
actions are declared in `useraction.c`.


Configuration
-------------

When starting up, karuiwm will simply run all actions in
`PREFIX/share/karuiwm/init.kwm`. These actions define the key and button
bindings listed in the previous section.

It is possible to override this behaviour by creating an executable script
`$XDG_USER_HOME/karuiwm/init`, and then do whatever you want in there. It is
highly recommended to at least define some basic key bindings, though.

For a start, copy `PREFIX/share/karuiwm/init.kwm` to
`$XDG_CONFIG_HOME/karuiwm/init.kwm`. Then create the following
`$XDG_CONFIG_HOME/karuiwm/init` shell script:

```sh
#!/bin/sh -

karuirpc "${XDG_CONFIG_HOME:-~/.config}"/karuiwm/init.kwm
```

Finally, make it executable.

This will not differ from the default setup, but now you can adapt your
`init.kwm` and add/change/remove actions to be run at startup.


Concepts
--------

### Layouts

karuiwm is a *dynamically tiling window manager*, this means that it
automatically arranges windows following certain dynamically changable rules,
called *layouts*. This allows the user to focus on the workflow instead of
having to arrange windows manually.

You can cycle through the layouts with `Mod`+`Space`; the
windows will get arranged automatically.

The layouts are defined in `layout.h`; the array at the end of the
file contains the layouts that will be actually used in your personal karuiwm
setup. Every layout below the `NULL` entry will not be accessible
through the usual layout cycling.

Every layout is assigned a bitfield that describes an icon that will get
displayed in the statusbar to indicate the current layout. Every hexadecimal
`long` entry in the array corresponds to a row in the icon (thus an
icon cannot be wider than `sizeof(long)\*8` pixels, but that
shouldn't be a problem).

### Workspaces

Workspaces in karuiwm are arranged in a two-dimensional grid, and they are
created and destroyed dynamically. A workspace may be considered either
*persistent* (it remains if left) or *temporary* (leaving it destroys it).

Whether a workspaces is temporary or persistent depends on the number of clients
on that workspace; an empty workspace is considered temporary, whereas it
becomes persistent as soon as a client is added.

The workspace map (accessible by `Mod`+`o`, see above) is
a visual representation of the workspaces and allows to perform actions on them
with a separately configured set of keys (the `wsmkeys` array):

* `Mod`+`h`|`j`|`k`|`l`
  set selection to left/below/above/right workspace
* `Mod`+`Shift`+`h`|`j`|`k`|`l`
  swap selected workspace with left/below/above/right workspace
* `Return`
  switch to the selected workspace
* `Esc`
  close workspace map without selecting a workspace

Each workspace is assigned a unique name that can be used to switch workspaces
by name using dmenu (see the dmenu manpage for further information how to use
it). If no or an empty name is assigned to a workspace, it automatically
takes the workspace structure's pointer value, preceded by `*`.

### Scratchpad

A *scratchpad* is a window that can easily be displayed and dismissed, typically
a terminal emulator to spontaneously type a command.

In karuiwm, the scratchpad is a workspace that is displayed on top of the
currently viewed workspace, and most client actions work on the scratchpad
workspace the same way as they would on a regular workspace.

The scratchpad can be toggled by hitting `Mod`+`Tab`.

At startup, no client is in the scratchpad (thus toggling it won't have any
effect); one first needs to add a client to the scratchpad by hitting
`Mod`+`Shift`+`Tab`. The same key combination
can be used to move the client out of the scratchpad again.

### Xinerama (aka multi-monitor)

karuiwm comes with Xinerama support and is thus capable of handling a
multi-monitor setup. Unlike its parent dwm however (which assigns a separate tag
set to each monitor), karuiwm uses a unified workspace set for all monitors.

Once a monitor is added, it will attempt to display the next undisplayed
workspace; if there is none, it will create a new workspace nearby and move its
view there. If a monitor moves its view to a workspace that is already displayed
on another monitor, the workspace view is swapped.


Links
-----

 * [Inter-Client Communication Conventions Manual (ICCCM)](https://tronche.com/gui/x/icccm/)
    - [4. Client to Window Manager Communication](https://tronche.com/gui/x/icccm/sec-4.html#s-4)
 * [Extended Window Manager Hints (EWMH)](https://specifications.freedesktop.org/wm-spec/latest/)
