#!/bin/sh --

set -efu

if [ -z "${MESON_BUILD_ROOT:-}" ]; then
	echo "MESON_BUILD_ROOT not defined." >&2
	echo "Please run \`ninja xephyr' from the build directory instead." >&2
	exit 1
fi

unset XMODIFIERS
PATH=$MESON_SOURCE_ROOT/utils:$PATH
export PATH
xinit \
	"$MESON_BUILD_ROOT"/karuiwm -d \
		modman.paths="$MESON_BUILD_ROOT" \
		init="$MESON_SOURCE_ROOT"/test/init.sh \
	-- \
	"$(command -v Xephyr)" :2 -screen 1024x768
